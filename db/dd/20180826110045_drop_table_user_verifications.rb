class DropTableUserVerifications < ActiveRecord::Migration[5.2]
  def change
    drop_table :user_verifications
  end
end
