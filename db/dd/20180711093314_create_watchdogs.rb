class CreateWatchdogs < ActiveRecord::Migration[5.2]
  def change
    create_table :watchdogs do |t|
      t.integer :admin_id
      t.datetime :logintime
      t.text :ipaddress
      t.datetime :lastvisit

      t.timestamps
    end
  end
end
