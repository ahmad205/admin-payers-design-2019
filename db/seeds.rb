# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Country.create([{
#     short_code: "EG",
#     Full_Name: "Egypt",
#     Phone_code: "002",
#     Currency:  "EGP",
#     language: "Arabic",
#     active: "1",
#             },
# {
#     short_code: "SA",
#     Full_Name: "Saudi Arabia	",
#     Phone_code: "00966",
#     Currency:  "SAR",
#     language: "Arabic",
#     active: "1",
#             }
#         ])  

# Admin.create([{
#             email: "ahmedhassan20593@gmail.com",
#             password: "Kh123456",
#             username: "AHassan",
#             firstname: "ahmad",
#             lastname: "hasan",
#             status: 1,
#             account_number: "PS100000001",
#             remember_token: Clearance::Token.new,
#             country_id: 1
#         },{
#             email: "work@aymanfekri.com",
#             password: "Ayman12345",
#             username: "Ayman",
#             firstname: "ayman",
#             lastname: "fekri",
#             status: 1,
#             account_number: "PS100000002",
#             remember_token: Clearance::Token.new,
#             country_id: 1
#         },{
#             email: "omararansa10@gmail.com",
#             password: "Omar12345",
#             username: "Omar",
#             firstname: "omar",
#             lastname: "aransa",
#             status: 1,
#             account_number: "PS100000003",
#             remember_token: Clearance::Token.new,
#             country_id: 1
#         },{
#             email: "amira.elfayome@servicehigh.com",
#             password: "Amira123456",
#             username: "Amira",
#             firstname: "amira",
#             lastname: "elfayome",
#             status: 1,
#             account_number: "PS100000004",
#             remember_token: Clearance::Token.new,
#             country_id: 1
#         }
# ])
# Admin.create([{
#             email: "Engy1192@gmail.com",
#             password: "Engy12345",
#             username: "Engy",
#             firstname: "Engy",
#             lastname: "Ali",
#             account_number: "PS100000009",
#             language: "en",
#             status: 1,
#             remember_token: Clearance::Token.new,
#             country_id: 1
#         }
#     ])
# UsersGroup.create([{
#     id: 1,
#     country_code: "EGY",
#     classification: "1",
#     name: "EG_class1",
#     description: "first class for Egyptian Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",
#     },{
#     id: 2,
#     country_code: "EGY",
#     classification: "2",
#     name: "EG_class2",
#     description: "second class for Egyptian Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     },{
#     id: 3,
#     country_code: "SAU",
#     classification: "1",
#     name: "SA_class1",
#     description: "first class for Saudian Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",     
#     },{
#     id: 4,
#     country_code: "SAU",
#     classification: "2",
#     name: "SA_class2",
#     description: "second class for Saudian Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     },{
#     id: 5,
#     country_code: "Public",
#     classification: "0",
#     name: "Public_Unverified",
#     description: "Unverified Users - upgrade_limit is set to 0 because this group user's can not be upgraded until verifing their account",
#     upgrade_limit: "0",
#     maximum_daily_send: "100",
#     maximum_monthly_send: "3000",
#     maximum_daily_recieve: "100",
#     maximum_monthly_recieve: "3000",
#     minimum_send_value: "5",
#     maximum_send_value: "100",
#     daily_send_number: "1",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "1",
#     daily_deposite_number: "2",
#     recieve_fees: "4",
#     recieve_ratio: ".04",
#     withdraw_fees: "4",
#     withdraw_ratio: ".04",
#     deposite_fees: "4", 
#     deposite_ratio: ".04",
#     },{
#     id: 6,
#     country_code: "Public",
#     classification: "1",
#     name: "Public_class1",
#     description: "first class for Public Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",
#     },{
#     id: 7,
#     country_code: "Public",
#     classification: "2",
#     name: "Public_class2",
#     description: "second class for Public Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     }
# ])

# GroupsBanksLimit.create([{
#     bank_type: "local_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#    },{
#     bank_type: "electronic_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#    },{
#     bank_type: "digital_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#         bank_type: "local_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     }

# ])

# Setting.create([{
#             key: "website_name",
#             value: "Payers",
#             description: "this is the website name.",
#         },{
#             key: "payers_deposit_expenses",
#             value: "1",
#             description: "Determines when payers deposit expenses will be taken:, 1: added to the amount before deposit, 2: Deducted from the amount after deposit",
#         },{ 
#             key: "language",
#             value: "en",
#             description: "this is the website language.",
#         },{
#             key: "default_country",
#             value: "Egypt",
#             description: "website client default country.",
#         },{
#             key: "time_zone",
#             value: "Cairo",
#             description: "website time zone.",
#         },{
#             key: "date_format",
#             value: "DD-MM-YYYY",
#             description: "website date format.",
#         },{
#             key: "maintenance_mode",
#             value: "on",
#             description: "website main status.",
#         },{
#             key: "logging_level",
#             value: "level1",
#             description: "website logging level.",
#         },{
#             key: "withdraw_status",
#             value: "off",
#             description: "withdraw closing status.",
#         },{
#             key: "withdraw_msg",
#             value: "sorry this page is under work try again later",
#             description: "withdraw closing message.",
#         },{
#             key: "deposit_status",
#             value: "off",
#             description: "deposit closing status.",
#         },{
#             key: "deposit_msg",
#             value: "sorry this page is under work try again later",
#             description: "deposit closing message.",
#         },{
#             key: "transfer_status",
#             value: "off",
#             description: "transfer money closing status.",
#         },{
#             key: "transfer_msg",
#             value: "sorry this page is under work try again later",
#             description: "transfer closing message.",
#         },{
#             key: "cards_status",
#             value: "off",
#             description: "buy cards closing status.",
#         },{
#             key: "charging_card_status",
#             value: "closed",
#             description: "open for all control, closed for transfer only",
#         },{
#             key: "cards_msg",
#             value: "sorry this page is under work try again later",
#             description: "buy cards closing message.",
#         },{
#             key: "support_status",
#             value: "off",
#             description: "support tickets closing status.",
#         },{
#             key: "support_msg",
#             value: "sorry this page is under work try again later",
#             description: "support tickets closing message.",
#         },{
#             key: "usd_to_sar",
#             value: "3.75",
#             description: "change from Dollar to SAR",
#         },{
#             key: "usd_to_egp",
#             value: "17.75",
#             description: "change for Dollar to EGP",
#         }
# ])