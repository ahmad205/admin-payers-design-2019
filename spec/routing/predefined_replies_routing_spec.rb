require "rails_helper"

RSpec.describe PredefinedRepliesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/predefined_replies").to route_to("predefined_replies#index")
    end

    it "routes to #new" do
      expect(:get => "/predefined_replies/new").to route_to("predefined_replies#new")
    end

    it "routes to #show" do
      expect(:get => "/predefined_replies/1").to route_to("predefined_replies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/predefined_replies/1/edit").to route_to("predefined_replies#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/predefined_replies").to route_to("predefined_replies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/predefined_replies/1").to route_to("predefined_replies#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/predefined_replies/1").to route_to("predefined_replies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/predefined_replies/1").to route_to("predefined_replies#destroy", :id => "1")
    end
  end
end
