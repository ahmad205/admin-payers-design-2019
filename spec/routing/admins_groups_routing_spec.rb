require "rails_helper"

RSpec.describe AdminsGroupsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/admins_groups").to route_to("admins_groups#index")
    end

    it "routes to #new" do
      expect(:get => "/admins_groups/new").to route_to("admins_groups#new")
    end

    it "routes to #show" do
      expect(:get => "/admins_groups/1").to route_to("admins_groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admins_groups/1/edit").to route_to("admins_groups#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/admins_groups").to route_to("admins_groups#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admins_groups/1").to route_to("admins_groups#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admins_groups/1").to route_to("admins_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admins_groups/1").to route_to("admins_groups#destroy", :id => "1")
    end
  end
end
