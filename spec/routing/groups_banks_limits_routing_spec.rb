require "rails_helper"

RSpec.describe GroupsBanksLimitsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/groups_banks_limits").to route_to("groups_banks_limits#index")
    end

    it "routes to #new" do
      expect(:get => "/groups_banks_limits/new").to route_to("groups_banks_limits#new")
    end

    it "routes to #show" do
      expect(:get => "/groups_banks_limits/1").to route_to("groups_banks_limits#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/groups_banks_limits/1/edit").to route_to("groups_banks_limits#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/groups_banks_limits").to route_to("groups_banks_limits#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/groups_banks_limits/1").to route_to("groups_banks_limits#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/groups_banks_limits/1").to route_to("groups_banks_limits#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/groups_banks_limits/1").to route_to("groups_banks_limits#destroy", :id => "1")
    end
  end
end
