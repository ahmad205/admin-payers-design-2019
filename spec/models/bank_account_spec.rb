require 'rails_helper'

RSpec.describe BankAccount, type: :model do

  it 'create bank account and update it' do

    # Setup
    @new_bank_account = BankAccount.create(bank_name: 'بنك فيصل الاسلامي المصري', branche_name: 1, swift_code: 22147953861, country: 'جمهورية مصر العربية', account_name: 'omar', account_number: 8965478512)
    # Exercise
    @updated_bank_account = @new_bank_account.update(:bank_name => 'بنك فيصل الاسلامي')
    # Verify
    expect(@new_bank_account.bank_name).to eq 'بنك فيصل الاسلامي'
    expect(@new_bank_account.branche_name)== 1
    expect(@new_bank_account.swift_code)== 22147953861
    expect(@new_bank_account.country).to eq 'جمهورية مصر العربية'
    expect(@new_bank_account.account_name).to eq 'omar'
    expect(@new_bank_account.account_number)== 8965478512
    # teardown is handled for you by RSpec
  end
end

describe BankAccount do

  it 'should delete the bank account' do
    
  # Setup 
    @new_bank_account = BankAccount.create(bank_name: 'بنك فيصل الاسلامي المصري', branche_name: 1, swift_code: 22147953861, country: 'جمهورية مصر العربية', account_name: 'omar', account_number: 8965478512)
  # Exercise
    @new_bank_account.destroy
    @deleted_bank_account = BankAccount.where(:branche_name => 1).first
  # Verify
    expect(@deleted_bank_account).to eq nil
  end
end