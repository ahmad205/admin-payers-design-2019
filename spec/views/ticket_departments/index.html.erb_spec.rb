require 'rails_helper'

RSpec.describe "ticket_departments/index", type: :view do
  before(:each) do
    assign(:ticket_departments, [
      TicketDepartment.create!(
        :name => "Name",
        :code => "Code",
        :description => "Description"
      ),
      TicketDepartment.create!(
        :name => "Name",
        :code => "Code",
        :description => "Description"
      )
    ])
  end

  it "renders a list of ticket_departments" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
