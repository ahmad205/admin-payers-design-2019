require 'rails_helper'

RSpec.describe "ticket_replies/new", type: :view do
  before(:each) do
    assign(:ticket_reply, TicketReply.new(
      :content => "MyString"
    ))
  end

  it "renders new ticket_reply form" do
    render

    assert_select "form[action=?][method=?]", ticket_replies_path, "post" do

      assert_select "input[name=?]", "ticket_reply[content]"
    end
  end
end
