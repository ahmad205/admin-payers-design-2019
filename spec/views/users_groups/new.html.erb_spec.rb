require 'rails_helper'

RSpec.describe "users_groups/new", type: :view do
  before(:each) do
    assign(:users_group, UsersGroup.new(
      :name => "MyString",
      :min_transfer => "",
      :max_transfer => ""
    ))
  end

  it "renders new users_group form" do
    render

    assert_select "form[action=?][method=?]", users_groups_path, "post" do

      assert_select "input[name=?]", "users_group[name]"

      assert_select "input[name=?]", "users_group[min_transfer]"

      assert_select "input[name=?]", "users_group[max_transfer]"
    end
  end
end
