require 'rails_helper'

RSpec.describe "users_groups/index", type: :view do
  before(:each) do
    assign(:users_groups, [
      UsersGroup.create!(
        :name => "Name",
        :min_transfer => "",
        :max_transfer => ""
      ),
      UsersGroup.create!(
        :name => "Name",
        :min_transfer => "",
        :max_transfer => ""
      )
    ])
  end

  it "renders a list of users_groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
