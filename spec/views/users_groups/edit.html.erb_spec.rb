require 'rails_helper'

RSpec.describe "users_groups/edit", type: :view do
  before(:each) do
    @users_group = assign(:users_group, UsersGroup.create!(
      :name => "MyString",
      :min_transfer => "",
      :max_transfer => ""
    ))
  end

  it "renders the edit users_group form" do
    render

    assert_select "form[action=?][method=?]", users_group_path(@users_group), "post" do

      assert_select "input[name=?]", "users_group[name]"

      assert_select "input[name=?]", "users_group[min_transfer]"

      assert_select "input[name=?]", "users_group[max_transfer]"
    end
  end
end
