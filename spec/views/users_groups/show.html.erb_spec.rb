require 'rails_helper'

RSpec.describe "users_groups/show", type: :view do
  before(:each) do
    @users_group = assign(:users_group, UsersGroup.create!(
      :name => "Name",
      :min_transfer => "",
      :max_transfer => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
