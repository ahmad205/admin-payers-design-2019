require 'rails_helper'

RSpec.describe "tickets/index", type: :view do
  before(:each) do
    assign(:tickets, [
      Ticket.create!(
        :number => "Number",
        :title => "Title",
        :content => "MyText",
        :attachment => "MyText"
      ),
      Ticket.create!(
        :number => "Number",
        :title => "Title",
        :content => "MyText",
        :attachment => "MyText"
      )
    ])
  end

  it "renders a list of tickets" do
    render
    assert_select "tr>td", :text => "Number".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
