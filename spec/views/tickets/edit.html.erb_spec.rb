require 'rails_helper'

RSpec.describe "tickets/edit", type: :view do
  before(:each) do
    @ticket = assign(:ticket, Ticket.create!(
      :number => "MyString",
      :title => "MyString",
      :content => "MyText",
      :attachment => "MyText"
    ))
  end

  it "renders the edit ticket form" do
    render

    assert_select "form[action=?][method=?]", ticket_path(@ticket), "post" do

      assert_select "input[name=?]", "ticket[number]"

      assert_select "input[name=?]", "ticket[title]"

      assert_select "textarea[name=?]", "ticket[content]"

      assert_select "textarea[name=?]", "ticket[attachment]"
    end
  end
end
