require 'rails_helper'

RSpec.describe "admin_watchdogs/edit", type: :view do
  before(:each) do
    @admin_watchdog = assign(:admin_watchdog, AdminWatchdog.create!(
      :admin_id => 1,
      :ipaddress => "MyText",
      :operation_type => "MyString"
    ))
  end

  it "renders the edit admin_watchdog form" do
    render

    assert_select "form[action=?][method=?]", admin_watchdog_path(@admin_watchdog), "post" do

      assert_select "input[name=?]", "admin_watchdog[admin_id]"

      assert_select "textarea[name=?]", "admin_watchdog[ipaddress]"

      assert_select "input[name=?]", "admin_watchdog[operation_type]"
    end
  end
end
