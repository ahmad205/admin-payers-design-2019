require 'rails_helper'

RSpec.describe "bank_accounts/edit", type: :view do
  before(:each) do
    @bank_account = assign(:bank_account, BankAccount.create!(
      :bank_name => "MyText",
      :branche_name => "MyText",
      :swift_code => "MyText",
      :country => "MyText",
      :account_name => "MyText",
      :account_number => "MyText"
    ))
  end

  it "renders the edit bank_account form" do
    render

    assert_select "form[action=?][method=?]", bank_account_path(@bank_account), "post" do

      assert_select "textarea[name=?]", "bank_account[bank_name]"

      assert_select "textarea[name=?]", "bank_account[branche_name]"

      assert_select "textarea[name=?]", "bank_account[swift_code]"

      assert_select "textarea[name=?]", "bank_account[country]"

      assert_select "textarea[name=?]", "bank_account[account_name]"

      assert_select "textarea[name=?]", "bank_account[account_number]"
    end
  end
end
