require 'rails_helper'

RSpec.describe "bank_accounts/index", type: :view do
  before(:each) do
    assign(:bank_accounts, [
      BankAccount.create!(
        :bank_name => "MyText",
        :branche_name => "MyText",
        :swift_code => "MyText",
        :country => "MyText",
        :account_name => "MyText",
        :account_number => "MyText"
      ),
      BankAccount.create!(
        :bank_name => "MyText",
        :branche_name => "MyText",
        :swift_code => "MyText",
        :country => "MyText",
        :account_name => "MyText",
        :account_number => "MyText"
      )
    ])
  end

  it "renders a list of bank_accounts" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
