require 'rails_helper'

RSpec.describe "predefined_replies/index", type: :view do
  before(:each) do
    assign(:predefined_replies, [
      PredefinedReply.create!(
        :title => "Title",
        :content => "Content",
        :category_id => 2
      ),
      PredefinedReply.create!(
        :title => "Title",
        :content => "Content",
        :category_id => 2
      )
    ])
  end

  it "renders a list of predefined_replies" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Content".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
