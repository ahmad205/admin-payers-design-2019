require 'rails_helper'

RSpec.describe "predefined_replies/show", type: :view do
  before(:each) do
    @predefined_reply = assign(:predefined_reply, PredefinedReply.create!(
      :title => "Title",
      :content => "Content",
      :category_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Content/)
    expect(rendered).to match(/2/)
  end
end
