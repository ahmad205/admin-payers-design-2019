require 'rails_helper'

RSpec.describe "predefined_replies/edit", type: :view do
  before(:each) do
    @predefined_reply = assign(:predefined_reply, PredefinedReply.create!(
      :title => "MyString",
      :content => "MyString",
      :category_id => 1
    ))
  end

  it "renders the edit predefined_reply form" do
    render

    assert_select "form[action=?][method=?]", predefined_reply_path(@predefined_reply), "post" do

      assert_select "input[name=?]", "predefined_reply[title]"

      assert_select "input[name=?]", "predefined_reply[content]"

      assert_select "input[name=?]", "predefined_reply[category_id]"
    end
  end
end
