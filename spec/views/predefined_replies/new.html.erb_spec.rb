require 'rails_helper'

RSpec.describe "predefined_replies/new", type: :view do
  before(:each) do
    assign(:predefined_reply, PredefinedReply.new(
      :title => "MyString",
      :content => "MyString",
      :category_id => 1
    ))
  end

  it "renders new predefined_reply form" do
    render

    assert_select "form[action=?][method=?]", predefined_replies_path, "post" do

      assert_select "input[name=?]", "predefined_reply[title]"

      assert_select "input[name=?]", "predefined_reply[content]"

      assert_select "input[name=?]", "predefined_reply[category_id]"
    end
  end
end
