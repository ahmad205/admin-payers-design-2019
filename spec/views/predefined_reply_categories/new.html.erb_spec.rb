require 'rails_helper'

RSpec.describe "predefined_reply_categories/new", type: :view do
  before(:each) do
    assign(:predefined_reply_category, PredefinedReplyCategory.new(
      :name => "MyString"
    ))
  end

  it "renders new predefined_reply_category form" do
    render

    assert_select "form[action=?][method=?]", predefined_reply_categories_path, "post" do

      assert_select "input[name=?]", "predefined_reply_category[name]"
    end
  end
end
