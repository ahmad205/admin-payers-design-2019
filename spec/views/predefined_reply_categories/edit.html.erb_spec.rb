require 'rails_helper'

RSpec.describe "predefined_reply_categories/edit", type: :view do
  before(:each) do
    @predefined_reply_category = assign(:predefined_reply_category, PredefinedReplyCategory.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit predefined_reply_category form" do
    render

    assert_select "form[action=?][method=?]", predefined_reply_category_path(@predefined_reply_category), "post" do

      assert_select "input[name=?]", "predefined_reply_category[name]"
    end
  end
end
