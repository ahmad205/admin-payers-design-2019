require 'rails_helper'

RSpec.describe "predefined_reply_categories/show", type: :view do
  before(:each) do
    @predefined_reply_category = assign(:predefined_reply_category, PredefinedReplyCategory.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
