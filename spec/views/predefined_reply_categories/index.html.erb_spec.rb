require 'rails_helper'

RSpec.describe "predefined_reply_categories/index", type: :view do
  before(:each) do
    assign(:predefined_reply_categories, [
      PredefinedReplyCategory.create!(
        :name => "Name"
      ),
      PredefinedReplyCategory.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of predefined_reply_categories" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
