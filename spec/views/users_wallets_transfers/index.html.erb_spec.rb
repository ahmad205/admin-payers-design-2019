require 'rails_helper'

RSpec.describe "users_wallets_transfers/index", type: :view do
  before(:each) do
    assign(:users_wallets_transfers, [
      UsersWalletsTransfer.create!(
        :user_id => 2,
        :user_to_id => 3,
        :transfer_method => 4,
        :transfer_type => 5,
        :hold_period => 6,
        :amount => 7.5,
        :ratio => 8.5,
        :approve => 9,
        :note => "Note"
      ),
      UsersWalletsTransfer.create!(
        :user_id => 2,
        :user_to_id => 3,
        :transfer_method => 4,
        :transfer_type => 5,
        :hold_period => 6,
        :amount => 7.5,
        :ratio => 8.5,
        :approve => 9,
        :note => "Note"
      )
    ])
  end

  it "renders a list of users_wallets_transfers" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.5.to_s, :count => 2
    assert_select "tr>td", :text => 8.5.to_s, :count => 2
    assert_select "tr>td", :text => 9.to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
