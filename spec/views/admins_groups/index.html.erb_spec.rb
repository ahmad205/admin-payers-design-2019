require 'rails_helper'

RSpec.describe "admins_groups/index", type: :view do
  before(:each) do
    assign(:admins_groups, [
      AdminsGroup.create!(
        :name => "Name"
      ),
      AdminsGroup.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of admins_groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
