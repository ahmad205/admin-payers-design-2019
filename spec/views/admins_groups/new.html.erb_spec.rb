require 'rails_helper'

RSpec.describe "admins_groups/new", type: :view do
  before(:each) do
    assign(:admins_group, AdminsGroup.new(
      :name => "MyString"
    ))
  end

  it "renders new admins_group form" do
    render

    assert_select "form[action=?][method=?]", admins_groups_path, "post" do

      assert_select "input[name=?]", "admins_group[name]"
    end
  end
end
