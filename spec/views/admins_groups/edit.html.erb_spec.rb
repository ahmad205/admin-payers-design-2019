require 'rails_helper'

RSpec.describe "admins_groups/edit", type: :view do
  before(:each) do
    @admins_group = assign(:admins_group, AdminsGroup.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit admins_group form" do
    render

    assert_select "form[action=?][method=?]", admins_group_path(@admins_group), "post" do

      assert_select "input[name=?]", "admins_group[name]"
    end
  end
end
