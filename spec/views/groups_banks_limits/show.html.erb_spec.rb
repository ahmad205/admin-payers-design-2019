require 'rails_helper'

RSpec.describe "groups_banks_limits/show", type: :view do
  before(:each) do
    @groups_banks_limit = assign(:groups_banks_limit, GroupsBanksLimit.create!(
      :bank_type => 2,
      :group_id => 3,
      :maximum_daily_withdraw => "",
      :maximum_monthly_withdraw => "",
      :maximum_daily_deposite => "",
      :maximum_monthly_deposite => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
