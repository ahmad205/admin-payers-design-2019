require 'rails_helper'

RSpec.describe "groups_banks_limits/index", type: :view do
  before(:each) do
    assign(:groups_banks_limits, [
      GroupsBanksLimit.create!(
        :bank_type => 2,
        :group_id => 3,
        :maximum_daily_withdraw => "",
        :maximum_monthly_withdraw => "",
        :maximum_daily_deposite => "",
        :maximum_monthly_deposite => ""
      ),
      GroupsBanksLimit.create!(
        :bank_type => 2,
        :group_id => 3,
        :maximum_daily_withdraw => "",
        :maximum_monthly_withdraw => "",
        :maximum_daily_deposite => "",
        :maximum_monthly_deposite => ""
      )
    ])
  end

  it "renders a list of groups_banks_limits" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
