require 'rails_helper'

RSpec.describe "groups_banks_limits/new", type: :view do
  before(:each) do
    assign(:groups_banks_limit, GroupsBanksLimit.new(
      :bank_type => 1,
      :group_id => 1,
      :maximum_daily_withdraw => "",
      :maximum_monthly_withdraw => "",
      :maximum_daily_deposite => "",
      :maximum_monthly_deposite => ""
    ))
  end

  it "renders new groups_banks_limit form" do
    render

    assert_select "form[action=?][method=?]", groups_banks_limits_path, "post" do

      assert_select "input[name=?]", "groups_banks_limit[bank_type]"

      assert_select "input[name=?]", "groups_banks_limit[group_id]"

      assert_select "input[name=?]", "groups_banks_limit[maximum_daily_withdraw]"

      assert_select "input[name=?]", "groups_banks_limit[maximum_monthly_withdraw]"

      assert_select "input[name=?]", "groups_banks_limit[maximum_daily_deposite]"

      assert_select "input[name=?]", "groups_banks_limit[maximum_monthly_deposite]"
    end
  end
end
