# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
env :PATH, ENV['PATH']
env :GEM_PATH, ENV['GEM_PATH']
set :output, "#{path}/log/cron_log.log"

every 1.minute do
rake "cron:test" , :environment => :development
end

every 5.minutes do
  rake "cron:create_reply" , :environment => :development
end

every 1.day, at: '9:00 am' do
    rake "cron:ticket_reminder" , :environment => :development
end

every 1.day, at: '9:00 am' do
  rake "cron:reset_user_daily_limits" , :environment => :development
end

#every 1.month
every '0 0 1 * *'  do
  rake "cron:reset_user_monthly_limits" , :environment => :development
end