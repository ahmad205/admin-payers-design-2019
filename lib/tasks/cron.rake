require 'rake'
# task :cron => :environment do
 
#     Cart.cornjob

# end





namespace :cron do
  desc "TODO"

  task test:  :environment do
    @unconfirmed_users = User.where("status =? ",0).all
    @timeago = (Time.now - 15.to_i.minutes)
    @unconfirmed_users.all.each do |user|       
       @user_created = user.created_at
       if @user_created < @timeago
        @user_log = Watchdog.where("user_id = ?",user.id).all
        if @user_log
          @user_log.all.each do |log|
            log.destroy
          end
        end
        @user_verification = Verification.where("user_id =?" ,user.id).first
        if @user_verification
          @user_verification.destroy
           user.destroy 
          end
        end
    end
  end 


  task ticket_reminder:  :environment do
    @tickets = Ticket.all
    @tickets.all.each do |ticket|
      if Date.today == ticket.reminder
        ticket.update(status: 0)
        @user = Admin.where("id =?", ticket.reminder_id.to_i).first
        @user_mail = @user.email
        @text = "#{ticket.id}"
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'Ticket Reminder',text:@text)
      end
    end
  end

# reset user daily limits and check total monthly receive amount to upgrade user if he exceeded the upgrade limit
  task reset_user_daily_limits: :environment do
    @users = User.includes(:users_group).all
    @users.all.each do |user|
      user.update(:total_daily_send => 0, :total_daily_recieve => 0, :total_daily_withdraw => 0, :total_daily_deposite => 0, :daily_send_number => 0, :daily_withdraw_number => 0, :daily_deposite_number => 0)
      if (user.total_monthly_recieve.to_f >= user.users_group.upgrade_limit.to_f) and (user.users_group.classification != 0)  and (user.users_group.classification != 1)  and (user.users_group.name != "Public_Unverified")
        @new_group_id = UsersGroup.where("country_code =? AND classification < ?",user.users_group.country_code ,user.users_group.classification.to_i).order('classification DESC').first.id
        user.update(:users_group_id => @new_group_id)
      end
    end
  end

  task reset_user_monthly_limits: :environment do
    User.includes(:user_info).all.each do |user|
      if user.user_info.status == "Verified"
        user.update(:total_monthly_send => 0, :total_monthly_recieve => 0, :total_monthly_withdraw => 0, :total_monthly_deposite => 0)
      end
    end
  end

  task create_reply: :environment do
    @tickets = Ticket.where(responded: 0).all
    @tickets.each do |ticket|
    TicketReply.create(:content => 'مرحبا في بايرز شكرا للتذكره جاري التواصل مع القسم المختص', :ticket_id => ticket.id, :sender_type => 0, :sender_id => 0)
    Ticket.where(id: ticket.id).update_all(responded: 1)
    end
  end

end





