class TicketPolicy
    attr_reader :admin, :model
   
    def initialize(admin, model)
      @admin = admin
      @model = model
    end
    
    def index?
      admin.role? :tickets_index
    end

    def show?
      admin.role? :show_tickets
    end

    def new?
      admin.role? :new_tickets
    end

    def create?
      new?
    end

    def create_reply?
      admin.role? :reply_tickets
    end

    def close_ticket?
      admin.role? :close_tickets
    end

    def close_tickets?
      close_ticket?
    end

    def changepriority?
      admin.role? :tickets_priority
    end

    def changereminder?
      admin.role? :tickets_reminder
    end

    def statistics?
      admin.role? :tickets_statistics
    end

end

