json.extract! company_bank, :id, :bank_key, :bank_name, :encryptkey, :currency, :country, :city, :branch, :branch_code, :phone, :account_name, :account_email, :account_number, :swiftcode, :ibancode, :visa_cvv, :bank_category, :bank_subcategory, :fees, :ratio, :logo, :expire_at, :created_at, :updated_at
json.url company_bank_url(company_bank, format: :json)
