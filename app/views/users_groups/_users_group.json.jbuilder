json.extract! users_group, :id, :name, :min_transfer, :max_transfer, :created_at, :updated_at
json.url users_group_url(users_group, format: :json)
