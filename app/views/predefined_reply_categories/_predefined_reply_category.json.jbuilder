json.extract! predefined_reply_category, :id, :name, :created_at, :updated_at
json.url predefined_reply_category_url(predefined_reply_category, format: :json)
