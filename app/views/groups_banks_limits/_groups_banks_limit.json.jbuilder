json.extract! groups_banks_limit, :id, :bank_type, :group_id, :maximum_daily_withdraw, :maximum_monthly_withdraw, :maximum_daily_deposite, :maximum_monthly_deposite, :created_at, :updated_at
json.url groups_banks_limit_url(groups_banks_limit, format: :json)
