json.extract! users_bank, :id, :bank_id, :user_id, :account_name, :account_number, :active, :is_iban, :created_at, :updated_at
json.url users_bank_url(users_bank, format: :json)
