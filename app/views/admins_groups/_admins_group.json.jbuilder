json.extract! admins_group, :id, :name, :created_at, :updated_at
json.url admins_group_url(admins_group, format: :json)
