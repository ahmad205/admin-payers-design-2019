json.extract! blogs_category, :id, :name, :description, :created_at, :updated_at
json.url blogs_category_url(blogs_category, format: :json)
