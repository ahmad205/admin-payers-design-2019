json.extract! user_announcement, :id, :user_id, :active, :slug, :keyword, :title, :description, :created_at, :updated_at
json.url user_announcement_url(user_announcement, format: :json)
