json.extract! quick_payment, :id, :user_id, :active, :availability, :title, :description, :created_at, :updated_at
json.url quick_payment_url(quick_payment, format: :json)
