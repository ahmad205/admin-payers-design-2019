json.extract! predefined_reply, :id, :title, :content, :category_id, :created_at, :updated_at
json.url predefined_reply_url(predefined_reply, format: :json)
