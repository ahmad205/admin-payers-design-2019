# app/services/sms_service.rb

class SMSService
    attr_accessor :tel, :smstext , :code

    # GET SMS params
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    # @param [Integer] code SMS Verification Pin Code.
    def initialize(tel:0,smstext:"",code:0)
      
      @tel = tel
      @smstext = smstext
      @code = code
      # configuration  " Enabled - is_global - countries "
      @configuration = "is_global"
    end

    # Call a specific SMS Provider and a specific function accourding to configuration
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    # @param [Integer] code SMS Verification Pin Code.
    # @param [String] configuration SMS system configuration(Enabled - is_global - countries).
    def call

        if @configuration == "is_global"
            if @tel != 0 and @code == 0 and @smstext == ""
              SMSInfobip.new(tel:@tel).send_pin_sms
            elsif @tel != 0 and @code == 0 and @smstext != ""
              SMSInfobip.new(tel:@tel,smstext:@smstext).send_text_sms
            elsif @tel == 0 and @code != 0 and @smstext == "" 
              SMSInfobip.new(code:@code).verify_pin_sms
            end
        end

    end

end