class AddressVerification < ApplicationRecord
  has_one_attached :attachment
  belongs_to :user ,:foreign_key => "user_id"
  has_one :user_info , foreign_key: "nationalid_verification_id"
  enum document_type: { RentContract: 0, ElectricityBill: 1 }
  enum status: { UnVerified: 0, Verified: 1, Pending: 2  }

  validates :address, length: { maximum: 100 }, if: :validate_address?    
  validates :name, length: { maximum: 100 }, if: :validate_name?
  validates :issue_date, :timeliness => {:on_or_before => lambda { Date.current }, :type => :date}, if: :validate_issue_date?
  validates_format_of :note, :with => /\A[^`!@#\$%\^&*+_=<>]+\z/, if: :validate_note?

  

  validate :attachment_type
 

  def attachment_type
      if attachment.attached?
        if !attachment.attachment.blob.content_type.in?(%w(image/png image/jpg image/jpeg))
          errors.add(:attachment, 'Must be an image file or extension is not included in the list')
        elsif attachment.blob.byte_size > (100 * 1024 * 1024) && attachment.blob.content_type.in?(%w(image/jpeg image/jpg image/png))
          errors.add(:attachment, 'The document oversize limited (100MB)')
        #elsif !attachment.attached?
            #errors.add(:attachment, 'attachment is required')
        end
      end
      
      #if attachment.attached? && !attachment.content_type.in?(%w(application/msword application/pdf))
       # errors.add(:attachment, 'Must be a PDF or a DOC file')
      #end
  end

  def validate_note?
      note.present?
  end
  def validate_address?
    address.present?
  end
  def validate_name?
    name.present?
  end
  def validate_issue_date?
    issue_date.present?
  end

  
end
