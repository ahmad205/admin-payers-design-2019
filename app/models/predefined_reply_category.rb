class PredefinedReplyCategory < ApplicationRecord
    has_many :predefined_reply , foreign_key: "category_id"
end
