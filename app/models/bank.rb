class Bank < ApplicationRecord
    belongs_to :country , foreign_key: "country_id"
    has_many :users_bank , foreign_key: "bank_id"
    enum is_iban: { swift: false, iban: true } 
    has_one_attached :logo

end
