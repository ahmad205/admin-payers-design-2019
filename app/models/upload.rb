class Upload < ApplicationRecord
    has_one_attached :avatar
    belongs_to :user , foreign_key: "user_id"

    def image_file=(avatar)
        self.image_file.attach(avatar)
    end 

    
end

