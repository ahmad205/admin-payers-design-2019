class AffilateProgram < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"

    def self.set_users_commission(userid,adminid)
      @user_id = userid.to_i
      @admin_id = adminid.to_i
      @marketing_profits_setting = Setting.where(key: "marketing_profits").first
      if (@marketing_profits_setting != nil and @marketing_profits_setting.value.to_i == 1)
        @invitor_commission_setting = Setting.where(key: "invitor_commision").first
        @user_commission_setting = Setting.where(key: "account_activation_reward").first
        if (@invitor_commission_setting != nil and @invitor_commission_setting.value.to_f > 0)
          @check_invitation = AffilateProgram.where(user_id: @user_id).first
          if @check_invitation != nil
            @new_user = User.where(id: @user_id).first.firstname
            @check_invitation.update(invitor_commission: @invitor_commission_setting.value.to_f, invitor_commission_status: true)
            @operation = MoneyOp.create(optype:5, amount:@invitor_commission_setting.value.to_f, status:0 ,user_id: @check_invitation.refered_by, payment_id:@check_invitation.id ,payment_date: Time.now)
            access_notification(@check_invitation.refered_by, @admin_id, "Marketing Profits", "You have been earned a new $ #{@invitor_commission_setting.value.to_f} from commission marketing operation from user #{@new_user}", "affilate_programs", @check_invitation.id)
          end
        end
        if (@user_commission_setting != nil and @user_commission_setting.value.to_f > 0)
          @user_wallet = UserWallet.where(user_id: @user_id).first
          @user_wallet.update(:amount => @user_wallet.amount.to_f + @user_commission_setting.value.to_f )
          @operation = MoneyOp.create(optype:5, amount:@user_commission_setting.value.to_f ,status:1 ,user_id: @user_id ,payment_date: Time.now)
          access_notification(@user_id, @admin_id, "Marketing Profits", "You have been earned $ #{@user_commission_setting.value.to_f} from registration under commission marketing operation", "affilate_programs", @operation.id)
        end
      end
    end

    def self.access_notification(user, adminid, title, smstext, control, opid)

      @user = User.where(id: user.to_i).first
      @admin_id = adminid
      @title = title
      @smstext = smstext
      @user_notification_setting = NotificationsSetting.where(user_id: @user.id.to_i).first      

      Notification.create(user_id: @user.id ,title: @title, description: @smstext , notification_type: @user_notification_setting.money_transactions, controller: control, opid: opid, admin_id: @admin_id)
      @userphone = @user.country.Phone_code.to_s + @user.telephone
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@userphone, @smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email, subject:@title, text: smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@userphone, @smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1, :sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email, subject:@title, text: smstext)
      end

    end

end
