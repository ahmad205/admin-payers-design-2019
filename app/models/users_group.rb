class UsersGroup < ApplicationRecord
    has_many :user , foreign_key: "users_group_id"
    validate :maximum_recieve
    validates_numericality_of :upgrade_limit, :maximum_daily_send, :maximum_monthly_send, :maximum_daily_recieve, :maximum_monthly_recieve, :minimum_send_value, :maximum_send_value, :daily_send_number, :minimum_withdraw_per_time, :minimum_deposite_per_time, :daily_withdraw_number, :daily_deposite_number, :recieve_fees, :recieve_ratio, :withdraw_fees, :withdraw_ratio, :withdraw_ratio, :deposite_fees, :deposite_ratio, :greater_than_or_equal_to => 0.0
    attr_accessor :local_maximum_daily_withdraw, :local_maximum_monthly_withdraw, :local_maximum_daily_deposite, :local_maximum_monthly_deposite,
                  :online_maximum_daily_withdraw, :online_maximum_monthly_withdraw, :online_maximum_daily_deposite, :online_maximum_monthly_deposite,
                  :crypto_maximum_daily_withdraw, :crypto_maximum_monthly_withdraw, :crypto_maximum_daily_deposite, :crypto_maximum_monthly_deposite
        
    def maximum_recieve
        errors.add(:maximum_monthly_recieve, "Should be greater than or equal to upgrade_limit") unless self.maximum_monthly_recieve >= self.upgrade_limit
    end

    def self.group_bank_limit(group_id)
      @groups_banks_limits = GroupsBanksLimit.where(group_id: group_id.to_i ).all
      @local_limits = @groups_banks_limits.where(bank_type: "local_bank").first
      @online_limits = @groups_banks_limits.where(bank_type: "electronic_bank").first
      @crypto_limits = @groups_banks_limits.where(bank_type: "digital_bank").first

      return @local_limits , @online_limits, @crypto_limits
    end

    def self.edit_group_bank_params( group_id, local_limits , online_limits, crypto_limits)
        @users_group = UsersGroup.where(id: group_id.to_i).first

        @users_group.local_maximum_daily_withdraw = local_limits.maximum_daily_withdraw
        @users_group.local_maximum_monthly_withdraw = local_limits.maximum_monthly_withdraw
        @users_group.local_maximum_daily_deposite = local_limits.maximum_daily_deposite
        @users_group.local_maximum_monthly_deposite = local_limits.maximum_monthly_deposite
  
        @users_group.online_maximum_daily_withdraw = online_limits.maximum_daily_withdraw
        @users_group.online_maximum_monthly_withdraw = online_limits.maximum_monthly_withdraw
        @users_group.online_maximum_daily_deposite = online_limits.maximum_daily_deposite
        @users_group.online_maximum_monthly_deposite = online_limits.maximum_monthly_deposite
  
        @users_group.crypto_maximum_daily_withdraw = crypto_limits.maximum_daily_withdraw
        @users_group.crypto_maximum_monthly_withdraw = crypto_limits.maximum_monthly_withdraw
        @users_group.crypto_maximum_daily_deposite = crypto_limits.maximum_daily_deposite
        @users_group.crypto_maximum_monthly_deposite = crypto_limits.maximum_monthly_deposite

        return @users_group
    end

    def self.create_group_bank_params(id,params) 
        GroupsBanksLimit.create(bank_type: "local_bank", group_id: id, maximum_daily_withdraw: params[:users_group][:local_maximum_daily_withdraw] ,maximum_monthly_withdraw: params[:users_group][:local_maximum_monthly_withdraw], maximum_daily_deposite: params[:users_group][:local_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:local_maximum_monthly_deposite])
        GroupsBanksLimit.create(bank_type: "electronic_bank", group_id: id, maximum_daily_withdraw: params[:users_group][:online_maximum_daily_withdraw] ,maximum_monthly_withdraw: params[:users_group][:online_maximum_monthly_withdraw],maximum_daily_deposite: params[:users_group][:online_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:online_maximum_monthly_deposite])
        GroupsBanksLimit.create(bank_type: "digital_bank", group_id: id, maximum_daily_withdraw: params[:users_group][:crypto_maximum_daily_withdraw ] ,maximum_monthly_withdraw: params[:users_group][:crypto_maximum_monthly_withdraw],maximum_daily_deposite: params[:users_group][:crypto_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:crypto_maximum_monthly_deposite])
    end

    def self.update_group_bank_params(params, local_limits , online_limits, crypto_limits)  
        local_limits.update(maximum_daily_withdraw: params[:users_group][:local_maximum_daily_withdraw] ,maximum_monthly_withdraw: params[:users_group][:local_maximum_monthly_withdraw], maximum_daily_deposite: params[:users_group][:local_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:local_maximum_monthly_deposite]  )
        online_limits.update(maximum_daily_withdraw: params[:users_group][:online_maximum_daily_withdraw] ,maximum_monthly_withdraw: params[:users_group][:online_maximum_monthly_withdraw], maximum_daily_deposite: params[:users_group][:online_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:online_maximum_monthly_deposite]  )
        crypto_limits.update(maximum_daily_withdraw: params[:users_group][:crypto_maximum_daily_withdraw] ,maximum_monthly_withdraw: params[:users_group][:crypto_maximum_monthly_withdraw], maximum_daily_deposite: params[:users_group][:crypto_maximum_daily_deposite], maximum_monthly_deposite: params[:users_group][:crypto_maximum_monthly_deposite]  )
    end
end
