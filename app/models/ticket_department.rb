class TicketDepartment < ApplicationRecord
  validates :code, length: { is: 2 }, format: { with: /\A[A-Z]+\z/ }, presence: true
  validates_presence_of :name
  enum status: { Disabled: 0, Enabled: 1 }
  before_destroy :destroy_validation

  private

  def destroy_validation
    if TicketDepartment.count == 1
      throw(:abort)
    end
  end
end
