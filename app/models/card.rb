class Card < ApplicationRecord
    has_one_attached :payment_receipt
    validates_uniqueness_of :number
    validate :check_value
    validate :check_expiration
    before_save :default_values
    validates :value, numericality: { only_integer: true }
    validates :status, numericality: { only_integer: true }
    has_many :cards_log , foreign_key: "card_id"
    belongs_to :user , foreign_key: "user_id" , optional: true

    def default_values
        self.number ||= [*('0'..'9'),*('a'..'z')].to_a.shuffle[0,16].join
        self.invoice_id ||= 0
    end

    def check_value
      errors.add(:value, "must be equal or greater than 5 USD") if value == nil || value < 5
    end

    def check_expiration
        errors.add(:expired_at, "Must be at least two days from now") if expired_at < Date.today + 2.days
    end

end
