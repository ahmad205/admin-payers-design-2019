class Withdraw < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    belongs_to :users_bank ,:foreign_key => "bank_id"
    validates :amount, presence: true
    validates :bank_id, presence: true
    validates :withdraw_type, presence: true

    def self.checkwithdraw(user_id,withdraw_amount, user_bank_id)

        @user_wallet = UserWallet.where(user_id: user_id.to_i).first
        @user = User.where(id: user_id.to_i).first
        @user_group = UsersGroup.where(id: @user.users_group_id).first
        @group_bank_limits = GroupsBanksLimit.where(bank_type: "local_bank", group_id: @user.users_group_id).first
        @user_bank = UsersBank.where(id: user_bank_id.to_i).first
        @bank = Bank.where(id: @user_bank.bank_id.to_i).first
               
        @message = ""

        if @user_wallet != nil
            if @user_wallet.amount.to_f < withdraw_amount.to_f
                @message  = @message + " This user doesn't have enough money for this operation"
            end

            if withdraw_amount.to_f <= 0
                @message  = @message + " withdraw amount can't be zero"
            end

            if @user_wallet.status == 0
                @message  = @message + " Sorry, This user Wallet was Disabled"
            end
        else
            @message  = @message + " This User Wasn't Stored in Our Database"
        end

        if @user_bank.active == false
            @message  = @message + " User has been disabled this bank "
        end

        if @bank.verified == false
            @message  = @message + " this bank is disabled by admin "
        end

        if withdraw_amount.to_f < @user_group.minimum_withdraw_per_time
            @message  = @message + " withdrawal amount must be greater than #{@user_group.minimum_withdraw_per_time} "
        end

        if (@user.total_daily_withdraw.to_f + withdraw_amount.to_f) > (@group_bank_limits.maximum_daily_withdraw.to_f)
            @message  = @message + " this user has exceeded the maximum withdrawal amount for this day "
        end

        if (@user.total_monthly_withdraw.to_f + withdraw_amount.to_f) > (@group_bank_limits.maximum_monthly_withdraw.to_f)
            @message  = @message + " this user has exceeded the maximum withdrawal amount for this month "
        end

        if (@user.daily_withdraw_number.to_i) >= (@user_group.daily_withdraw_number.to_i)
            @message  = @message + " this user has exceeded the maximum withdrawal request number for this day "
        end

        return   @message 
    end
end
