class UsersBank < ApplicationRecord
    belongs_to :bank , foreign_key: "bank_id"
    belongs_to :user , foreign_key: "user_id"
    has_many :withdraw , foreign_key: "bank_id"
    attr_accessor :bank_name, :country_id
    enum is_iban: { swift: false, iban: true }
end
