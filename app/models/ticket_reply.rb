class TicketReply < ApplicationRecord
    belongs_to :ticket
    validates_presence_of :content, :message => "the content can't be empty"
    enum sender_type: { "Payers Support": 0, User: 1 }
end