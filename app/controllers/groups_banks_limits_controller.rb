class GroupsBanksLimitsController < ApplicationController
  before_action :set_groups_banks_limit, only: [:show, :edit, :update, :destroy]

  # GET /groups_banks_limits
  # GET /groups_banks_limits.json
  def index
    @groups_banks_limits = GroupsBanksLimit.all
  end

  # GET /groups_banks_limits/1
  # GET /groups_banks_limits/1.json
  def show
  end

  # GET /groups_banks_limits/new
  def new
    @groups_banks_limit = GroupsBanksLimit.new
  end

  # GET /groups_banks_limits/1/edit
  def edit
  end

  # POST /groups_banks_limits
  # POST /groups_banks_limits.json
  def create
    @groups_banks_limit = GroupsBanksLimit.new(groups_banks_limit_params)

    respond_to do |format|
      if @groups_banks_limit.save
        format.html { redirect_to @groups_banks_limit, notice: 'Groups banks limit was successfully created.' }
        format.json { render :show, status: :created, location: @groups_banks_limit }
      else
        format.html { render :new }
        format.json { render json: @groups_banks_limit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups_banks_limits/1
  # PATCH/PUT /groups_banks_limits/1.json
  def update
    respond_to do |format|
      if @groups_banks_limit.update(groups_banks_limit_params)
        format.html { redirect_to @groups_banks_limit, notice: 'Groups banks limit was successfully updated.' }
        format.json { render :show, status: :ok, location: @groups_banks_limit }
      else
        format.html { render :edit }
        format.json { render json: @groups_banks_limit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups_banks_limits/1
  # DELETE /groups_banks_limits/1.json
  def destroy
    @groups_banks_limit.destroy
    respond_to do |format|
      format.html { redirect_to groups_banks_limits_url, notice: 'Groups banks limit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groups_banks_limit
      @groups_banks_limit = GroupsBanksLimit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groups_banks_limit_params
      params.require(:groups_banks_limit).permit(:bank_type, :group_id, :maximum_daily_withdraw, :maximum_monthly_withdraw, :maximum_daily_deposite, :maximum_monthly_deposite)
    end
end
