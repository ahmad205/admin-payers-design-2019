class CardsController < ApplicationController
  before_action :set_card, only: [:show, :edit, :update, :destroy]

  # GET list of Cards and display it
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for Specific user, 2 for Specific country, 3 for Public).
  # @return [expired_at] card expired Date( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index()
    @cards = Card.all
    @countries = Country.all
  end

  # GET a Specific card and display it
  # @param [Integer] id card unique ID.
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show()
  end

  # GET a new Card params
  # @param [Integer] id card unique ID (Created automatically).
  # @param [Integer] number card unique number (Created automatically and must be 16 digit).
  # @param [Integer] value card value.
  # @param [Integer] status card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @param [Integer] country_id card spacific country.
  # @param [Integer] card_type card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @param [Date] expired_at card expired Date ( Must be equal or greater than 2 days from now ).
  # @param [Integer] invoice_id card invoice ID.
  def new
    @card = Card.new
  end

  # POST a new Card and save it
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    
    @card = Card.new(card_params)
    @user = params[:card][:user_id]
    @card_status = params[:card][:status].to_i
    @card_type = params[:card][:card_type].to_i
    if @user != "0"
      @userdata = User.where("account_number LIKE ? OR email LIKE ?", "%#{@user}%","%#{@user}%").first
      @card.user_id = @userdata.id.to_i
      ActiveRecord::Base.transaction do
        @user_wallet = UserWallet.where(user_id: @userdata.id.to_i).first
        @new_balance = @user_wallet.amount.to_f + params[:card][:value].to_f
        @user_notification_setting = NotificationsSetting.where(user_id: @userdata.id.to_i).first
        if @card_status == 2
          @money_status = 1
          @user_wallet.update(amount:@new_balance)
          @smstext = "admin_payers_has_processed_anew_deposit_of/#{params[:card][:value]}/USD_to_your_account/by_prepaid_Payers_card/transaction_was_successfully_created/the_amount_has_been_successfully_added_to_your_account"
          # @smstext = "New Card was successfully created and balance with value of #{params[:card][:value]} USD was add to your wallet by payers admin"
        elsif @card_status == 1
          @money_status = 0
          @smstext = "admin_payers_has_created_anew_active_card_with_value_of/#{params[:card][:value]}/USD_to_your_account/transaction_was_successfully_created/the_card_is_ready_to_charge/the_amount_willbe_added_to_your_wallet_after_charging"
          # @smstext = "New active card with value of #{params[:card][:value]} USD was successfully created to your account by payers admin"
        elsif @card_status == 3
          @money_status = 0
          @smstext = "admin_payers_has_created_anew_pending_card_with_value_of/#{params[:card][:value]}/USD_to_your_account/transaction_was_successfully_created/the_card_is_pending/you_willnot_be_able_to_charge_card_until_it_becomes_active"
          # @smstext = "New Pending card with value of #{params[:card][:value]} USD was successfully created to your account by payers admin"
        end
        @money_operation = MoneyOp.create(:optype => 4,:amount => params[:card][:value] ,:payment_gateway => "Payers" ,:status => @money_status,:payment_date => DateTime.now,:user_id => @userdata.id.to_i)
        @card.operation_id = @money_operation.opid
        access_notification(@userdata,@smstext,@money_operation.id)
      end
    else
      @card.user_id = 0
    end

    respond_to do |format|
      if @card.save
        CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
        format.html { redirect_to @card, notice: 'Card was successfully created.' }
        format.json { render json: @card, status: :created, location: @card }
      else
        format.html { render :new }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET an existing card to edit params
  # @param [Integer] id card unique ID.
  # @param [Integer] value card value.
  # @param [Integer] status card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @param [Integer] country_id card spacific country.
  # @param [Integer] card_type card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @param [Date] expired_at card expired Date ( Must be equal or greater than 2 days from now ).
  def edit
  end

  # Change an existing card params(value,status,expired_at,user_id)
  # @return [id] card unique ID.
  # @return [number] card unique number (unique number can not be changed).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @card.update(card_params)

        if params[:card][:status].to_i == 2
          CardsLog.create(:user_id => current_user.id.to_i,:action_type => 2,:ip => request.remote_ip,:card_id => @card.id)
        end

        format.html { redirect_to @card, notice: 'Card was successfully updated.' }
        format.json { render :show, status: :ok, location: @card }
      else
        format.html { render :edit }
        format.json { render json: @card.errors, status: :unprocessable_entity }
      end
    end
  end

  # Create a new card with a spacific Category value
  # @param [Integer]  value card value. (Created automatically from Card Category value).
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country, 3 for Public).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def buy_card

    @new_card =  Card.create(:value => params[:val],:expired_at => Date.today + 3.days,:status => 1,:user_id => 1)
    @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
    
    if @new_card
      redirect_to(display_cards_categories_path,:notice => 'Card was successfully created')
    end
    

  end

  # Change Card status from enabled to suspend
  # @param [Integer] id card unique ID (Created automatically).
  # @param [Integer] status card status (1 for Active, 4 for Disabled).
  # @return [number] card unique number.
  # @return [value] card value.
  # @return [status] card status (1 for Active, 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country).
  # @return [expired_at] card expired Date.
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def cards_status

    @card_status = Card.where(:id => params[:id].to_i).first
    if @card_status.status ==  4
      @card_status.update(:status => 1)
      redirect_to(cards_path,:notice => 'Card was successfully enabled')
    else
      @card_status.update(:status => 4)
      redirect_to(cards_path,:notice => 'Card was successfully disabled')
    end

  end

  # GET a spacific Card date and search for any existing card
  # @param [String] search search word that you want to find (number OR value).
  # @param [Date] searchdatefrom search expiration date from.
  # @param [Date] searchdateto search expiration date to.
  # @param [Integer] status search status than you want.
  # @param [Integer] search_user search user ID than you want.
  # @return [id] card unique ID
  # @return [value] card value.
  # @return [number] card unique number.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date.
  # @return [user_id] card user ID.
  # @return [invoice_id] card invoice ID.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def search

    @search = params[:search].delete('-')
    @searchdatefrom = params[:searchdatefrom]
    @searchdateto = params[:searchdateto]
    @status = params[:status]
    @search_user = params[:search_user]
    @card_type = params[:card_type]
    @country_id = params[:country_id]
    @countries = Country.all
    
    @Cards = Card.all
    @Cards = @Cards.where("cards.expired_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
    @Cards = @Cards.where("cards.expired_at <= ? ", @searchdateto)   if  @searchdateto.present?
    @Cards = @Cards.where("cards.expired_at <= ? AND cards.expired_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    @Cards = @Cards.where(["cards.number LIKE ? OR cards.operation_id LIKE ? OR cast(cards.value as text) LIKE ?","%#{@search}%","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    @Cards = @Cards.where("cards.status = ? ", @status)   if  @status.present?
    @Cards = @Cards.where("cards.card_type = ? ", @card_type)   if  @card_type.present?
    @Cards = @Cards.where("cards.country_id = ? ", @country_id)   if  @country_id.present?
    @Cards = @Cards.joins("INNER JOIN users ON users.id = cards.user_id").distinct.where("users.account_number LIKE ? OR users.email LIKE ?", "%#{@search_user}%","%#{@search_user}%")   if  @search_user.present?

  end

  def searchpost
  end

  # Add admin note on a spacific operation
  # @param [String] admin_note admin text note.
  # @param [String] operation_id card id.
  def admin_add_note
    @note = params[:admin_add_note][:admin_note]
    @operation_id = params[:admin_add_note][:operation_id]
    @operation = MoneyOp.where(opid: @operation_id).first
    @operation.update(admin_note: @note)
    @user = User.where(id: @operation.user_id).first
    @smstext = "payers_admin_add_comment_on_your_transaction/#{@operation.opid}/review_this_operation/see_admin_note/if_you_need_any_help/contact_support_team"
    access_notification(@user,@smstext,@operation.id)

    redirect_back fallback_location: root_path, notice: 'Message was successfully Sent to user.'
  end

  # Send a spacific country card to a spacific user email
  # @param [Integer] card_id card unique ID.
  # @param [Integer] card_value card value.
  # @param [String] user_email card user email.
  # @param [String] card_number card unique number.
  # @param [String] admin_msg admin text message.
  def send_card
    @user_email = params[:send_card][:user_email]
    @card_number = params[:send_card][:card_number]
    @card_value = params[:send_card][:card_value]
    @admin_msg = params[:send_card][:admin_msg]
    @card_id = params[:send_card][:card_id].to_i
    @card = Card.where(id: @card_id).first
    @card.update(note: @admin_msg)
    @smstext = "#{@admin_msg}
    Card Number is : #{@card_number}
    Card value is : #{@card_value} USD"
    EmailNotification.email_notification_setting(user_mail:@user_email,subject:'Recharge Wallet Balance',text:@smstext)
    redirect_back fallback_location: root_path, notice: 'Card was successfully Sent to user.'
  end

  # DELETE an existing card
  # @param [id] card unique ID.
  def destroy
    @card.destroy
    CardsLog.create(:user_id => 1,:action_type => 3,:ip => request.remote_ip,:card_id => @card.id)
    respond_to do |format|
      format.html { redirect_to cards_url, notice: 'Card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card
      @card = Card.find(params[:id])
    end

    def access_notification(user,smstext,operation_id)

      @user = user
      @smstext = smstext
      @splitxt = @smstext.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + t("mails.#{@splitxt[3]}") + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      @user_notification_setting = NotificationsSetting.where(user_id: @user.id).first
      Notification.create(user_id: @user.id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions,admin_id: current_user.id,controller: "money_ops",opid: operation_id)
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def card_params
      params.require(:card).permit(:operation_id, :number, :value, :status, :expired_at, :user_id, :invoice_id, :country_id, :card_type, :user_account_number, :note)
    end
end
