class AdminsGroupsController < ApplicationController
  before_action :set_admins_group, only: [:show, :edit, :update, :destroy]

  # GET /admins_groups
  # GET /admins_groups.json
  def index
    @admins_groups = AdminsGroup.all
  end

  # GET /admins_groups/1
  # GET /admins_groups/1.json
  def show
  end

  # GET /admins_groups/new
  def new
    @admins_group = AdminsGroup.new
  end

  # GET /admins_groups/1/edit
  def edit
  end

  # POST /admins_groups
  # POST /admins_groups.json
  def create
    @admins_group = AdminsGroup.new(admins_group_params)

    respond_to do |format|
      if @admins_group.save
        format.html { redirect_to @admins_group, notice: 'Admins group was successfully created.' }
        format.json { render :show, status: :created, location: @admins_group }
      else
        format.html { render :new }
        format.json { render json: @admins_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admins_groups/1
  # PATCH/PUT /admins_groups/1.json
  def update
    respond_to do |format|
      if @admins_group.update(admins_group_params)
        format.html { redirect_to @admins_group, notice: 'Admins group was successfully updated.' }
        format.json { render :show, status: :ok, location: @admins_group }
      else
        format.html { render :edit }
        format.json { render json: @admins_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins_groups/1
  # DELETE /admins_groups/1.json
  def destroy
    @admins_group.destroy
    respond_to do |format|
      format.html { redirect_to admins_groups_url, notice: 'Admins group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def group_roles
    if params[:id].to_i == 1
      redirect_to(admins_groups_path, notice: 'you cannot edit roles for super admin group.')
    else
      @roles = Role.all.group(:caption)
      @assignments = AdmingroupsRole.where(:admins_group_id => params[:id]).all
      @group_name = AdminsGroup.where(:id => params[:id]).first.name
    end 
  end

  def edit_group_roles
    if AdmingroupsRole.where(:admins_group_id => params[:groupid]).blank?
      if params[:role]
        params[:role].each do |role|
          AdmingroupsRole.create(:admins_group_id => params[:groupid],:role_id => role )
        end
      end
        redirect_back fallback_location: root_path, notice: 'Group roles was successfully created'
    else
      if params[:role]
        AdmingroupsRole.where(:admins_group_id => params[:groupid]).destroy_all
        params[:role].each do |role|
          AdmingroupsRole.create(:admins_group_id => params[:groupid],:role_id => role )
        end
      end
      redirect_back fallback_location: root_path, notice: 'Group roles was successfully updated'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admins_group
      @admins_group = AdminsGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admins_group_params
      params.require(:admins_group).permit(:name)
    end
end
