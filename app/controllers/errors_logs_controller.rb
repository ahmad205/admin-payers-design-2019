class ErrorsLogsController < ApplicationController
  before_action :set_errors_log, only: [:show, :edit, :update, :destroy, :page_error]

  # GET list of errors logs
  # @return [id] error log unique ID.
  # @return [user_id] the user unique id.
  # @return [user_type] user type (1 for admin , 2 for user).
  # @return [code] error log unique code.
  # @return [message] error log message.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @errors_logs_count = ErrorsLog.where(user_type: 2).count
    @errors_logs = ErrorsLog.where(user_type: 2).joins("INNER JOIN users ON users.id = errors_logs.user_id").select("errors_logs.*,users.firstname,users.lastname,users.account_number").all
  end

  def admins_errors_logs
    @admin_logs_count = ErrorsLog.where(user_type: 1).count
    @admin_logs = ErrorsLog.where(user_type: 1).joins("INNER JOIN admins ON admins.id = errors_logs.user_id").select("errors_logs.*,admins.firstname,admins.lastname,admins.account_number").all
  end

  # GET /errors_logs/1
  # GET /errors_logs/1.json
  def show
  end

  def page_error
    render :layout => false
  end

  # GET /errors_logs/new
  def new
    @errors_log = ErrorsLog.new
  end


  # POST /errors_logs
  # POST /errors_logs.json
  def create
    @errors_log = ErrorsLog.new(errors_log_params)

    respond_to do |format|
      if @errors_log.save
        format.html { redirect_to @errors_log, notice: 'Errors log was successfully created.' }
        format.json { render :show, status: :created, location: @errors_log }
      else
        format.html { render :new }
        format.json { render json: @errors_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /errors_logs/1
  # DELETE /errors_logs/1.json
  def destroy
    @errors_log.destroy
    respond_to do |format|
      format.html { redirect_to errors_logs_url, notice: 'Errors log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_errors_log
      @errors_log = ErrorsLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def errors_log_params
      params.require(:errors_log).permit(:user_id, :user_type, :code, :message)
    end
end
