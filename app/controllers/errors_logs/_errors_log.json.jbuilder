json.extract! errors_log, :id, :user_id, :user_type, :code, :message, :created_at, :updated_at
json.url errors_log_url(errors_log, format: :json)
