class UserAnnouncementsController < ApplicationController
  before_action :set_user_announcement, only: [:show, :edit, :update, :destroy]

  # GET list of user announcements and display it
  # @return [id] announcement unique ID.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keyword.
  # @return [availability] announcement available for (1 for all users , 2 for user group , 3 for a specific user).
  # @return [user_group_id] the user group unique id.
  # @return [user_id] the user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @user_announcements = UserAnnouncement.all
  end

  # GET a Specific existing user announcement
  # @param [Integer] id announcement unique ID.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keyword.
  # @return [availability] announcement available for (1 for all users , 2 for user group , 3 for a specific user).
  # @return [user_group_id] the user group unique id.
  # @return [user_id] the user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    if @user_announcement.user_id != 0 && @user_announcement.user_id != nil
    @user_data = User.where(id:@user_announcement.user_id).first
    end
  end

  # GET a new announcement
  # @param [Integer] id announcement unique ID.
  # @param [Integer] active announcement activity (1 for enable , 0 for disable).
  # @param [String] title announcement title.
  # @param [String] slug announcement slug.
  # @param [String] description announcement description.
  # @param [String] keywords announcement keyword.
  # @param [Integer] availability announcement available for (1 for all users , 2 for user group , 3 for a specific user).
  # @param [Integer] user_group_id the user group unique id.
  # @param [Integer] user_id The user unique id.
  def new
    @user_announcement = UserAnnouncement.new
  end

  
  # GET an existing announcement to edit params
  # @param [Integer] active announcement activity (1 for enable , 0 for disable).
  # @param [String] title announcement title.
  # @param [String] slug announcement slug.
  # @param [String] description announcement description.
  # @param [String] keywords announcement keywords.
  # @param [Integer] availability announcement available for (1 for all users , 2 for user group , 3 for a specific user).
  # @param [Integer] user_group_id the user group unique id.
  def edit
    if @user_announcement.user_id != 0 && @user_announcement.user_id != nil
      @user_data = User.where(id:@user_announcement.user_id).first
      @usermail = @user_data.email
    else
      @usermail = ""
    end

  end

  # POST a new announcement and save it
  # @return [id] announcement unique ID.
  # @return [order] announcement unique order.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keyword.
  # @return [user_id] the user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @user_announcement = UserAnnouncement.new(user_announcement_params)
    if @user_announcement.availability == 1
      @user_announcement.user_group_id = 0
      @user_announcement.user_id = 0
    elsif @user_announcement.availability == 2
      @user_announcement.user_id = 0
    elsif @user_announcement.availability == 3
      @user_announcement.user_group_id = 0
      @user_announcement.user_id = User.where("email = ? OR account_number = ?",params[:user_announcement][:user_id],params[:user_announcement][:user_id]).pluck(:id).first
    end

    respond_to do |format|
      if @user_announcement.save
        format.html { redirect_to @user_announcement, notice: 'Announcement was successfully created.' }
        format.json { render :show, status: :created, location: @user_announcement }
      else
        format.html { render :new }
        format.json { render json: @user_announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing announcement params
  # @return [id] announcement unique ID.
  # @return [order] announcement unique order.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keywords.
  # @return [availability] announcement available for (1 for all users , 2 for user group , 3 for a specific user).
  # @return [user_group_id] the user group unique id.
  # @return [user_id] The user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update

    if params[:user_announcement][:availability].to_i == 1
      @user_group_id = 0
      @user_id = 0
    elsif params[:user_announcement][:availability].to_i == 2
      @user_id = 0
      @user_group_id = params[:user_announcement][:user_group_id]
    elsif params[:user_announcement][:availability].to_i == 3
      @user_group_id = 0
      @user_id = User.where("email = ? OR account_number = ?",params[:user_announcement][:user_id],params[:user_announcement][:user_id]).pluck(:id).first
    end
    respond_to do |format|
      if @user_announcement.update(user_announcement_params)
        @user_announcement.update(user_id:@user_id , user_group_id:@user_group_id)
        format.html { redirect_to @user_announcement, notice: 'Announcement was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_announcement }
      else
        format.html { render :edit }
        format.json { render json: @user_announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_announcements/1
  # DELETE /user_announcements/1.json
  def destroy
    @user_announcement.destroy
    respond_to do |format|
      format.html { redirect_to user_announcements_url, notice: 'Announcement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_announcement
      @user_announcement = UserAnnouncement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_announcement_params
      params.require(:user_announcement).permit(:user_id, :active, :slug, :keyword, :title, :description, :availability, :user_group_id)
    end
end
