class MoneyOpsController < ApplicationController
  before_action :set_money_op, only: [:show]

  # GET list of Money Operations and display it
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number  (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @money_ops = MoneyOp.all
  end

  # GET a spacific Money Operation and display it
  # @param [Integer] id Money operation unique ID.
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    @bank_type = ""
    if set_money_op.optype == 4
      @remittance_data = Card.where("operation_id = ?",set_money_op.opid).first
      @bank = CompanyBank.where("bank_key = ?",set_money_op.payment_gateway).first
      @bank_type = @bank.bank_category.to_s
    end
  end

  # GET a new Money Operation
  # @param [Integer] id Money operation unique ID (Created automatically).
  # @param [String] opid Operation unique number (Created automatically and must be 12 digits and letters ).
  # @param [Integer] optype Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance).
  # @param [Integer] amount Operation amount.
  # @param [Integer] payment_gateway Operation payment gateway (paypal,bitcoin,...).
  # @param [Integer] status Operation status (0 for pending & 1 for compeleted).
  # @param [Date] payment_date Payment operation creation date.
  # @param [Integer] payment_id Payment ID for the operation itself.
  # @param [Integer] user_id Operation user id.
  def new
    @money_op = MoneyOp.new
  end

  # POST a new Money Operation and save it
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create

    @money_op = MoneyOp.new(money_op_params)

    respond_to do |format|
      if @money_op.save
        format.html { redirect_to @money_op, notice: 'Money op was successfully created.' }
        format.json { render :show, status: :created, location: @money_op }
      else
        format.html { render :new }
        format.json { render json: @money_op.errors, status: :unprocessable_entity }
      end
    end
  end


  # Change Money Operations status
  # @param [Integer] id Money Operation unique ID (Created automatically).
  # @param [Integer] status Money Operation status (0 for Pending, 1 for Compeleted).
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance, 5 for affiliate program reward).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def operations_status

    @new_status = params[:operations_status][:status].to_i
    @operations_status = MoneyOp.where(:id => params[:operations_status][:id].to_i).first
    @operation_card = Card.where(:operation_id => @operations_status.opid).first
    @user_wallet = UserWallet.where(:user_id => @operations_status.user_id).first
    @user_balance = @user_wallet.amount
    @bank = CompanyBank.where(:bank_key => @operations_status.payment_gateway).first
    #@original_amount = (((@operations_status.amount.to_f - @bank.fees) * 100 )/(@bank.ratio + 100))
    @original_amount = @operation_card.net_value
    @user = User.where("id = ?", @operations_status.user_id.to_i).first
    @user_notification_setting = NotificationsSetting.where(user_id: @operations_status.user_id).first

    if @new_status ==  1
      @operations_status.update(:status => 1)
      @operation_card.update(:status => 2)

      if @bank.wallet_type == 2
        @user_new_balance = @user_wallet.transfer_amount.to_f + @original_amount.to_f
        @user_wallet.update(:transfer_amount => @user_new_balance)
      else
        @user_new_balance = @user_wallet.amount.to_f + @original_amount.to_f
        @user_wallet.update(:amount => @user_new_balance)
      end
      @smstext = "admin_payers_has_confirmed_your_deposit_of/#{@original_amount}/USD_to_your_account/via_direct_bank_deposit/transaction_was_successfully_created/the_amount_has_been_successfully_added_to_your_account"
      @splitxt = @smstext.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + t("mails.#{@splitxt[3]}") + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      # @smstext = "Your #{@original_amount} USD payers card has been confirmed and added to your wallet by admin"
      Notification.create(user_id: @operations_status.user_id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions,controller:"money_ops",opid:@operations_status.id,admin_id: current_user.id)
   
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      end
      redirect_back fallback_location: root_path,:notice => 'Operation status was successfully updated to be Compeleted'
    elsif @new_status ==  2
      @operations_status.update(:status => @new_status)
      @operation_card.update(:status => 4)
      @smstext = "admin_payers_has_cancelled_your_deposit_of/#{@original_amount}/USD_to_your_account/via_direct_bank_deposit/transaction_was_cancelled/for_more_information_you_can_contact_the_admin"
      @splitxt = @smstext.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + t("mails.#{@splitxt[3]}") + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      # @smstext = "Your #{@original_amount} USD payers card has been cancelled by admin payers"
      Notification.create(user_id: @operations_status.user_id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions,controller:"money_ops",opid:@operations_status.id,admin_id: current_user.id)
   
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext_translation)
      end
      redirect_to(money_ops_path,:notice => 'Operation status was successfully updated to be Pending')
    else
      redirect_to(money_ops_path,:notice => 'Sorry, something went wrong')
    end

  end

  def remittance_operations
    @operations = MoneyOp.joins("INNER JOIN cards ON cards.operation_id = money_ops.opid").select("cards.number,cards.status,money_ops.user_id,money_ops.opid,money_ops.amount,money_ops.fees,money_ops.payment_gateway,money_ops.payment_date,money_ops.id").all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_money_op
      @money_op = MoneyOp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def money_op_params
      params.require(:money_op).permit(:opid, :optype, :amount, :fees, :payment_gateway, :status, :payment_date, :payment_id, :user_id , :admin_note)
    end
end
