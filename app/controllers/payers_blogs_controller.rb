class PayersBlogsController < ApplicationController
  before_action :set_payers_blog, only: [:show, :edit, :update, :destroy]

  # GET /payers_blogs
  # GET /payers_blogs.json
  def index
    @payers_blogs = PayersBlog.all
  end

  # GET /payers_blogs/1
  # GET /payers_blogs/1.json
  def show
  end

  # GET /payers_blogs/new
  def new
    @payers_blog = PayersBlog.new
  end

  # GET /payers_blogs/1/edit
  def edit
  end

  # POST /payers_blogs
  # POST /payers_blogs.json
  def create
    @payers_blog = PayersBlog.new(payers_blog_params)
    @payers_blog.author_id = User.where("email = ? OR account_number = ?",params[:payers_blog][:author_id],params[:payers_blog][:author_id]).pluck(:id).first


    respond_to do |format|
      if @payers_blog.save
        format.html { redirect_to @payers_blog, notice: 'Payers blog was successfully created.' }
        format.json { render :show, status: :created, location: @payers_blog }
      else
        format.html { render :new }
        format.json { render json: @payers_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payers_blogs/1
  # PATCH/PUT /payers_blogs/1.json
  def update
    respond_to do |format|
      if @payers_blog.update(payers_blog_params)
        format.html { redirect_to @payers_blog, notice: 'Payers blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @payers_blog }
      else
        format.html { render :edit }
        format.json { render json: @payers_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payers_blogs/1
  # DELETE /payers_blogs/1.json
  def destroy
    @payers_blog.destroy
    respond_to do |format|
      format.html { redirect_to payers_blogs_url, notice: 'Payers blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payers_blog
      @payers_blog = PayersBlog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payers_blog_params
      params.require(:payers_blog).permit(:author_id, :title, :slug, :content, :keywords, :category_id, :active)
    end
end
