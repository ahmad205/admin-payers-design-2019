class QuickPaymentsController < ApplicationController
  before_action :set_quick_payment, only: [:show, :edit, :update, :destroy]

  # GET list of quick payments pages
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @quick_payments_count = QuickPayment.all.count
    @quick_payments = QuickPayment.joins("INNER JOIN users ON users.id = quick_payments.user_id").select("quick_payments.*,users.firstname,users.lastname").all

  end

  # GET a Specific existing quick payment page
  # @param [Integer] id quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    @user_data = User.where(id:@quick_payment.user_id).first
  end

  # GET a new quick payment page
  # @param [Integer] id quick payment unique ID.
  # @param [Integer] user_id the user unique id.
  # @param [Integer] active quick payment page activity (1 for enable , 0 for disable).
  # @param [Integer] availability quick payment page availability (1 for public , 2 for payers users only).
  # @param [String] slug quick payment page link name.
  # @param [String] title quick payment page title.
  # @param [String] description quick payment page description.
  def new
    @quick_payment = QuickPayment.new
  end

  # GET an existing quick payment page to edit params
  # @param [Integer] user_id the user unique id.
  # @param [Integer] active quick payment page activity (1 for enable , 0 for disable).
  # @param [Integer] availability quick payment page availability (1 for public , 2 for payers users only).
  # @param [String] title quick payment page title.
  # @param [String] description quick payment page description.
  def edit
    @user_email = User.where(id:@quick_payment.user_id).pluck(:email).first
    @page_url = "#{request.protocol}#{request.host}:#{request.port}/fast_pay/#{@quick_payment.slug}"
  end

  def link_check
    @quick_payment = QuickPayment.where(slug: params[:slug]).all
    if @quick_payment == []
      @note = "true"
    else
      @note = "false"
    end
    render :json => {'check': @note }
  end


  def quick_transfers
    @users_wallets_transfers = UsersWalletsTransfer.where(transfer_type: "Quick Payment").all
  end


  # POST a new quick payment page and save it
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @quick_payment = QuickPayment.new(quick_payment_params)
    @quick_payment.user_id = User.where("email = ? OR account_number = ?",params[:quick_payment][:user_id],params[:quick_payment][:user_id]).pluck(:id).first

    respond_to do |format|
      if @quick_payment.save
        format.html { redirect_to @quick_payment, notice: 'Quick payment was successfully created.' }
        format.json { render :show, status: :created, location: @quick_payment }
      else
        format.html { render :new }
        format.json { render json: @quick_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing quick payment page params
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @quick_payment.update(quick_payment_params)
        format.html { redirect_to @quick_payment, notice: 'Quick payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @quick_payment }
      else
        format.html { render :edit }
        format.json { render json: @quick_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quick_payments/1
  # DELETE /quick_payments/1.json
  def destroy
    @quick_payment.destroy
    respond_to do |format|
      format.html { redirect_to quick_payments_url, notice: 'Quick payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quick_payment
      @quick_payment = QuickPayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quick_payment_params
      params.require(:quick_payment).permit(:user_id, :active, :availability, :title, :description, :slug)
    end
end
