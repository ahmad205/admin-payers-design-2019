class BankAccountsController < ApplicationController
  before_action :set_bank_account, only: [:show, :edit, :update, :destroy]

  # GET list of all bank accounts and display it
  # @return [id] bank unique ID (Created automatically).
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @bank_accounts = BankAccount.all
  end

  # GET a spacific bank account and display it
  # @param  [Integer] id bank unique ID.
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new bank account
  # @param [Integer] id bank unique ID (Created automatically).
  # @param [String] bank_name the bank name.
  # @param [String] branche_name the branche name of the bank.
  # @param [String] swift_code the bank swift code.
  # @param [String] country the bank country.
  # @param [String] account_name the bank account name.
  # @param [String] account_number the bank account number.
  def new
    @bank_account = BankAccount.new
  end

  # GET an existing bank account and edit params
  # @param [String] bank_name the bank name.
  # @param [String] branche_name the branche name of the bank.
  # @param [String] swift_code the bank swift code.
  # @param [String] country the bank country.
  # @param [String] account_name the bank account name.
  # @param [String] account_number the bank account number.
  def edit
  end

  # POST a new bank account and save it
  # @return [id] bank unique ID (Created automatically).
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @bank_account = BankAccount.new(bank_account_params)

    respond_to do |format|
      if @bank_account.save
        format.html { redirect_to @bank_account, notice: 'Bank account was successfully created.' }
        format.json { render :show, status: :created, location: @bank_account }
      else
        format.html { render :new }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing bank params(bank_name,branche_name,swift_code,country,account_name,account_number)
  # @return [id] bank unique ID.
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @bank_account.update(bank_account_params)
        format.html { redirect_to @bank_account, notice: 'Bank account was successfully updated.' }
        format.json { render :show, status: :ok, location: @bank_account }
      else
        format.html { render :edit }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bank_accounts/1
  # DELETE /bank_accounts/1.json
  def destroy
    @bank_account.destroy
    respond_to do |format|
      format.html { redirect_to bank_accounts_url, notice: 'Bank account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account
      @bank_account = BankAccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_account_params
      params.require(:bank_account).permit(:bank_name, :branche_name, :swift_code, :country, :account_name, :account_number)
    end
end
