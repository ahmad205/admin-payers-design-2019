class PredefinedReplyCategoriesController < ApplicationController
  before_action :set_predefined_reply_category, only: [:show, :edit, :update, :destroy]

  # GET /predefined_reply_categories
  # GET /predefined_reply_categories.json
  def index
    @predefined_reply_categories = PredefinedReplyCategory.all
  end

  # GET /predefined_reply_categories/1
  # GET /predefined_reply_categories/1.json
  def show
  end

  # GET /predefined_reply_categories/new
  def new
    @predefined_reply_category = PredefinedReplyCategory.new
  end

  # GET /predefined_reply_categories/1/edit
  def edit
  end

  # POST /predefined_reply_categories
  # POST /predefined_reply_categories.json
  def create
    @predefined_reply_category = PredefinedReplyCategory.new(predefined_reply_category_params)

    respond_to do |format|
      if @predefined_reply_category.save
        format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply category was successfully created.' }
        format.json { render :show, status: :created, location: predefined_reply_categories_url }
      else
        format.html { render :new }
        format.json { render json: @predefined_reply_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /predefined_reply_categories/1
  # PATCH/PUT /predefined_reply_categories/1.json
  def update
    respond_to do |format|
      if @predefined_reply_category.update(predefined_reply_category_params)
        format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply category was successfully updated.' }
        format.json { render :show, status: :ok, location: predefined_reply_categories_url }
      else
        format.html { render :edit }
        format.json { render json: @predefined_reply_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /predefined_reply_categories/1
  # DELETE /predefined_reply_categories/1.json
  def destroy
    @predefined_reply_category.destroy
    respond_to do |format|
      format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_predefined_reply_category
      @predefined_reply_category = PredefinedReplyCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def predefined_reply_category_params
      params.require(:predefined_reply_category).permit(:name)
    end
end
