class PredefinedRepliesController < ApplicationController
  before_action :set_predefined_reply, only: [:show, :edit, :update, :destroy]

  # GET /predefined_replies
  # GET /predefined_replies.json
  def index
    @predefined_replies = PredefinedReply.joins("INNER JOIN predefined_reply_categories ON predefined_reply_categories.id = predefined_replies.category_id").select("predefined_replies.*,predefined_reply_categories.name").all
    @predefined_reply_categories = PredefinedReplyCategory.all
  end

  # GET /predefined_replies/1
  # GET /predefined_replies/1.json
  def show
    @predefined_reply = PredefinedReply.joins("INNER JOIN predefined_reply_categories ON predefined_reply_categories.id = predefined_replies.category_id").select("predefined_replies.*,predefined_reply_categories.name").find(params[:id])

  end

  # GET /predefined_replies/new
  def new
    @predefined_reply = PredefinedReply.new
    @category = PredefinedReplyCategory.all
  end

  # GET /predefined_replies/1/edit
  def edit
    @category = PredefinedReplyCategory.all
  end

  # POST /predefined_replies
  # POST /predefined_replies.json
  def create
    @predefined_reply = PredefinedReply.new(predefined_reply_params)
    @category = PredefinedReplyCategory.all

    respond_to do |format|
      if @predefined_reply.save
        format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply was successfully created.' }
        format.json { render :show, status: :created, location: predefined_reply_categories_url }
      else
        format.html { render :new }
        format.json { render json: @predefined_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /predefined_replies/1
  # PATCH/PUT /predefined_replies/1.json
  def update
    respond_to do |format|
      if @predefined_reply.update(predefined_reply_params)
        format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply was successfully updated.' }
        format.json { render :show, status: :ok, location: predefined_reply_categories_url }
      else
        format.html { render :edit }
        format.json { render json: @predefined_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  def allreplies
    @category_id = params[:category_id].to_i
    @category_replies = PredefinedReply.where(:category_id => @category_id ).all
    render :json => {'category': @category_replies }
  end

  def replycontent
    @reply_id = params[:reply_id].to_i
    @replycontent = PredefinedReply.where(:id => @reply_id ).pluck(:content).first
    render :json => {'content': @replycontent }
  end

  # DELETE /predefined_replies/1
  # DELETE /predefined_replies/1.json
  def destroy
    @predefined_reply.destroy
    respond_to do |format|
      format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_predefined_reply
      @predefined_reply = PredefinedReply.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def predefined_reply_params
      params.require(:predefined_reply).permit(:title, :content, :category_id)
    end
end
