class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update, :destroy]

  # GET list of all addresses and display it
  # @return [id] address unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    begin
    @addresses = Address.all

    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 1,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'page_error', id: @log.id
    
    end
  end

  # GET a spacific address and display it
  # @param [Integer] id address unique ID.
  # @param [Integer] user_id the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end


  # GET a new address
  # @param [id] address unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @param [String] address the detailed address.
  # @param [String] country the user address country.
  # @param [String] governorate the user address governorate.
  # @param [String] city the user address city.
  # @param [String] street the user address street.
  # @param [Boolean] default_address defualt address for a user(true , false).
  def new
    @address = Address.new
  end

  # GET an existing address and edit params
  # @param [String] address the detailed address.
  # @param [String] country the user address country.
  # @param [String] governorate the user address governorate.
  # @param [String] city the user address city.
  # @param [String] street the user address street.
  # @param [Boolean] default_address defualt address for a user(true , false).
  def edit
  end

  # POST a new address and save it
  # @return [id] address unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @address = Address.new(address_params)
    respond_to do |format|
      if @address.save
        format.html { redirect_to @address, notice: 'Address was successfully created.' }
        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing address params(address,country,governorate,governorate,street,default_address)
  # @return [id] address unique ID.
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @address.update(address_params)
        format.html { redirect_to @address, notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET a spacific address data and search for any existing address
  # @param [String] search search word that you want to find.
  # @param [Date] searchdatefrom search created at date from.
  # @param [Date] searchdateto search created at date to.
  # @param [Boolean] address_status search default address status than you want(1 for Defualt , 0 for Not Default).
  # @param [Integer] search_user search user ID than you want.
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(1 , 0).
  # @return [created_at] Date created.
  def search

    @search = params[:search]
    @searchdatefrom = params[:searchdatefrom]
    @searchdateto = params[:searchdateto]
    @address_status = params[:address_status]
    @search_user = params[:search_user]

    @addresses = Address.joins("INNER JOIN users ON users.id = addresses.user_id").distinct.all
    @addresses = @addresses.where("addresses.created_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
    @addresses = @addresses.where("addresses.created_at <= ? ", @searchdateto)   if  @searchdateto.present?
    @addresses = @addresses.where("addresses.created_at <= ? AND addresses.created_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    @addresses = @addresses.where(["addresses.address LIKE  ? OR addresses.country LIKE  ? OR addresses.governorate LIKE ? OR addresses.city LIKE ? OR addresses.street LIKE ?","%#{@search}%","%#{@search}%","%#{@search}%","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    @addresses = @addresses.where("addresses.default_address = ? ", @address_status)   if  @address_status.present?
    @addresses = @addresses.where("users.username LIKE ? ", "%#{@search_user}%")   if  @search_user.present?

  end

  def searchpost
  end

  def user_addresses
    @user_id = params[:user_id]
    @data = User.where("email = ? OR account_number = ?", @user_id,@user_id).first
    @addresses = Address.where(:user_id => @data.id ).pluck('address' , 'id')
    render :json => {'useraddresses': @addresses }
  end

  # DELETE an existing address
  # @param [id] address unique ID.
  def destroy
    respond_to do |format|
        @addresses = Address.where(:user_id => @address.user_id).all
        if  @addresses.count == 1
          format.html { redirect_to addresses_url, notice: 'Cannot Delete this last Address' }
        else

          if @address.default_address == true
            format.html { redirect_to addresses_url, notice: 'Cannot Delete a Default Address' }
          else
            @address.destroy
            format.html { redirect_to addresses_url, notice: 'Address was successfully destroyed.' }
            format.json { head :no_content }
          end

        end
    end
  end

  # DELETE all addresses of a specific user
  # @param [user_id] the user unique id.
  def delete_user_addresses
    @user_id = params[:user_id]
    @addresses = Address.where("user_id = ? AND default_address = ?", @user_id,0).all
    respond_to do |format|
      if @addresses != []
        @addresses.destroy_all
        format.html { redirect_to addresses_url, notice: 'All addresses for this user was successfully destroyed.' }
      else
        format.html { redirect_to addresses_url, notice: 'This user was not found in our database' }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:user_id, :address, :country, :governorate, :city, :street, :default_address)
    end
end
