class UsersWalletsTransfersController < ApplicationController
  before_action :set_users_wallets_transfer, only: [:show, :edit, :update, :destroy]

  # GET /users_wallets_transfers
  # GET /users_wallets_transfers.json
  def index
    @users_wallets_transfers = UsersWalletsTransfer.all
    #@users_wallets_transfers = UsersWalletsTransfer.joins("INNER JOIN addresses ON addresses.id = users_wallets_transfers.address_id").select("users_wallets_transfers.*,addresses.address").all

  end

  # GET /users_wallets_transfers/1
  # GET /users_wallets_transfers/1.json
  def show
    if @users_wallets_transfer.address_id.to_i != 0 || @users_wallets_transfer.address_id.to_i != nil || @users_wallets_transfer.address_id.to_i != ""
      @user_address = Address.where(id: @users_wallets_transfer.address_id).pluck(:address).first
    else
      @user_address = "-" 
    end
  end

  # GET /users_wallets_transfers/new
  def new
    @users_wallets_transfer = UsersWalletsTransfer.new
  end

  # GET /users_wallets_transfers/1/edit
  def edit
  end

  # POST /users_wallets_transfers
  # POST /users_wallets_transfers.json
  def create
    @users_wallets_transfer = UsersWalletsTransfer.new(users_wallets_transfer_params)
    @user1 = User.where("email = ? OR account_number = ?", params[:users_wallets_transfer][:user_id], params[:users_wallets_transfer][:user_id]).first
    @user = User.where("email = ? OR account_number = ?", params[:users_wallets_transfer][:user_to_id],params[:users_wallets_transfer][:user_to_id]).first
    @user_from = @user1.id
    @user_to = @user.id
    @users_wallets_transfer.user_id = @user1.id
    @users_wallets_transfer.user_to_id = @user.id
    @transfer_amount = @users_wallets_transfer.amount.to_f
    @message = UserWallet.checktransfer(@user1,@user,@transfer_amount,@users_wallets_transfer.hold_period)
    if @message == ""
      ActiveRecord::Base.transaction do

        @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
        @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
        
        if @user_wallet_from.transfer_amount.to_f >= @transfer_amount
          @new_balance_from = @user_wallet_from.transfer_amount.to_f - @transfer_amount
          @user_wallet_from.update(:transfer_amount => @new_balance_from )
        elsif @user_wallet_from.transfer_amount.to_f < @transfer_amount.to_f
          @remaining_amount = @transfer_amount - @user_wallet_from.transfer_amount.to_f
          @new_balance_from = @user_wallet_from.amount.to_f - @remaining_amount.to_f
          @user_wallet_from.update(:amount => @new_balance_from , :transfer_amount => 0)
        end

        @expenses = (@transfer_amount * @user.users_group.recieve_ratio.to_f/100) + @user.users_group.recieve_fees.to_f
        @net_balance  = @transfer_amount -  @expenses
        @new_balance_to = @user_wallet_to.amount.to_f + @net_balance

        if @users_wallets_transfer.hold_period == "" || @users_wallets_transfer.hold_period.to_i == 0
          @users_wallets_transfer.approve = 1
          @user_wallet_to.update(:amount => @new_balance_to )
          @smstext = "admin_payers_has_processed_anew_sending_of/#{@transfer_amount.to_f}/USD_from_your_wallet_to_user/#{@user.firstname} #{@user.lastname}/transaction_was_successfully_created/the_amount_has_been_added_to_the_user_wallet"
          @smstext_to = "admin_payers_has_processed_anew_sending_of/#{@net_balance.round(2).to_f}/USD_to_your_wallet_from_user/#{@user1.firstname} #{@user1.lastname}/transaction_was_successfully_created/the_amount_has_been_successfully_added_to_your_account"
          # @smstext = "#{@transfer_amount.to_f} USD has been successfully transferred to user #{@user.firstname} #{@user.lastname}"
          # @smstext_to = "#{@net_balance.to_f} USD has been successfully added to your wallet by user #{@user1.firstname} #{@user1.lastname}"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => @user1.id.to_i)
          @user.update(:total_monthly_recieve => @user.total_monthly_recieve.to_f + @transfer_amount, :total_daily_recieve => @user.total_daily_recieve.to_f + @transfer_amount)

        else
          @users_wallets_transfer.approve = 0
          @smstext = "admin_payers_has_processed_anew_sending_of/#{@transfer_amount.to_f}/USD_from_your_wallet_to_user/#{@user.firstname} #{@user.lastname}/transaction_is_in_progress/the_amount_will_be_added_to_the_user_the_holding_period"
          @smstext_to = "admin_payers_has_processed_anew_sending_of/#{@net_balance.round(2).to_f}/USD_to_your_wallet_from_user/#{@user1.firstname} #{@user1.lastname}/transaction_is_in_progress/it_will_be_added_to_your_wallet_after_the_holding_period"
          # @smstext = "#{@transfer_amount.to_f} USD transfer request to user #{@user.firstname} #{@user.lastname} has been successfully created and waiting the hold period"
          # @smstext_to = "#{@net_balance.to_f} USD has been successfully added to you by user #{@user1.firstname} #{@user1.lastname} and waiting the hold period"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount ,:payment_gateway => "Payers" ,:status => 0,:payment_date => DateTime.now,:user_id => @user1.id.to_i)
        end

        if @users_wallets_transfer.transfer_type == "Service"
          @users_wallets_transfer.address_id = 0
        end
        @user1.update(:total_monthly_send =>  @user1.total_monthly_send.to_f + @transfer_amount, :total_daily_send =>  @user1.total_daily_send.to_f + @transfer_amount, :daily_send_number =>  @user1.daily_send_number.to_i + 1 )
        AuditLog.create(user_id: @user1.id, user_name: current_user.uuid, action_type: "Balance transfer", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
        @users_wallets_transfer.operation_id = @money_operation.opid
        @users_wallets_transfer.ratio = @expenses
        
        respond_to do |format|
          if @users_wallets_transfer.save
            @notification = access_notification(@smstext,@smstext_to,@user,@user1,"users_wallets_transfers",@users_wallets_transfer.id)

            format.html { redirect_to @users_wallets_transfer, notice: 'Users wallets transfer was successfully created.' }
            format.json { render :show, status: :created, location: @users_wallets_transfer }
          else
            format.html { render :new }
            format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
          end
        end
      end
    else
      redirect_to(new_users_wallets_transfer_path,:notice => @message )
    end
  end

  # PATCH/PUT /users_wallets_transfers/1
  # PATCH/PUT /users_wallets_transfers/1.json
  def update
    respond_to do |format|
      if @users_wallets_transfer.update(users_wallets_transfer_params)
        format.html { redirect_to @users_wallets_transfer, notice: 'Users wallets transfer was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_wallets_transfer }
      else
        format.html { render :edit }
        format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_wallets_transfers/1
  # DELETE /users_wallets_transfers/1.json
  def destroy
    @users_wallets_transfer.destroy
    respond_to do |format|
      format.html { redirect_to users_wallets_transfers_url, notice: 'Users wallets transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_wallets_transfer
      @users_wallets_transfer = UsersWalletsTransfer.find(params[:id])
    end

    def access_notification(smstext,smstext_to,user,user1,control,opid)
      @user1 = user1
      @tel = @user1.telephone
      @user_mail = @user1.email
      @user_from = @user1.id.to_i
      @user = user
      @user_to = @user.id
      @smstext = smstext
      @smstext_to = smstext_to
      @splitxt = @smstext.split('/')
      @splitxt_to = @smstext_to.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + "#{@splitxt[3]}" + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      @smstext_to_translation = t("mails.#{@splitxt_to[0]}") + "#{@splitxt_to[1]}" + t("mails.#{@splitxt_to[2]}") + "#{@splitxt_to[3]}" + t("mails.#{@splitxt_to[4]}") + t("mails.#{@splitxt_to[5]}")
      @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
      @user_notification_setting_to = NotificationsSetting.where(user_id: @user_to).first
      Notification.create([{user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions,controller: control,opid: opid,admin_id: current_user.id},
      {user_id: @user_to ,title: "Balance transfer", description: @smstext_to , notification_type: @user_notification_setting_to.money_transactions,controller: control,opid: opid,admin_id: current_user.id}])

      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@tel,@smstext_translation)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext_translation)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@tel,@smstext_translation)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext_translation)
      end

      if @user_notification_setting_to.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to_translation)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@smstext_to_translation)
      elsif @user_notification_setting_to.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to_translation)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting_to.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@smstext_to_translation)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_wallets_transfer_params
      params.require(:users_wallets_transfer).permit(:user_id, :user_to_id, :transfer_method, :transfer_type, :hold_period, :amount, :ratio, :approve, :note , :address_id, :service_status)
    end
end
