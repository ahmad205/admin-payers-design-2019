class UsersGroupsController < ApplicationController
  before_action :require_login 
  before_action :set_users_group, only: [:show, :edit, :update, :destroy]

  
  # show list of all users groups
  # return with details for all users groups, it works only if current user is an admin
  # @return [String]     name                          UsersGroup's-name.
  # @return [String]     country_code                  UsersGroup's country code.
  # @return [Integer]    classification                UsersGroup's classification.
  # @return [String]     description                   UsersGroup's description.
  # @return [Float]      upgrade_limit                 If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
  # @return [Float]      maximum_daily_send            The maximum amount allowed to be sent daily.
  # @return [Float]      maximum_monthly_send          The maximum amount allowed to be sent monthly.
  # @return [Float]      maximum_daily_recieve         The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [Float]      maximum_monthly_recieve       The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [Float]      minimum_send_value            The minimum amount allowed to be sent per time.
  # @return [Float]      maximum_send_value            The maximum amount allowed to be sent per time.
  # @return [Integer]    daily_send_number             The maximum Number of sending times allowed daily.
  # @return [Float]      minimum_withdraw_per_time     The minimum amount allowed to be withdrawn per time.
  # @return [Float]      minimum_deposite_per_time     The minimum amount allowed to be deposited per time.
  # @return [Integer]    daily_withdraw_number         Maximum number of daily withdrawals allowed.
  # @return [Integer]    daily_deposite_number         The maximum number of times the deposit allowed daily.
  # @return [Float]      recieve_fees                  fees for receiving payments.
  # @return [Float]      recieve_ratio                 The percentage for receiving payments
  # @return [Float]      withdraw_fees                 fees for withdrawing.
  # @return [Float]      withdraw_ratio                The percentage for withdrawing
  # @return [Float]      deposite_fees                 fees for depositing
  # @return [Float]      deposite_ratio                The percentage for depositing
  # @return [datetime]   created_at                    date of creating the UsersGroup.
  # @return [datetime]   updated_at                    date of updating the UsersGroup.
 
  def index
    @users_groups = UsersGroup.all
  end

  
  # show data of users group
  # @param [Integer] id 
  # @return [String]     name                          UsersGroup's-name.
  # @return [String]     country_code                  UsersGroup's country code.
  # @return [Integer]    classification                UsersGroup's classification.
  # @return [String]     description                   UsersGroup's description.
  # @return [Float]      upgrade_limit                 If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
  # @return [Float]      maximum_daily_send            The maximum amount allowed to be sent daily.
  # @return [Float]      maximum_monthly_send          The maximum amount allowed to be sent monthly.
  # @return [Float]      maximum_daily_recieve         The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [Float]      maximum_monthly_recieve       The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [Float]      minimum_send_value            The minimum amount allowed to be sent per time.
  # @return [Float]      maximum_send_value            The maximum amount allowed to be sent per time.
  # @return [Integer]    daily_send_number             The maximum Number of sending times allowed daily.
  # @return [Float]      minimum_withdraw_per_time     The minimum amount allowed to be withdrawn per time.
  # @return [Float]      minimum_deposite_per_time     The minimum amount allowed to be deposited per time.
  # @return [Integer]    daily_withdraw_number         Maximum number of daily withdrawals allowed.
  # @return [Integer]    daily_deposite_number         The maximum number of times the deposit allowed daily.
  # @return [Float]      recieve_fees                  fees for receiving payments.
  # @return [Float]      recieve_ratio                 The percentage for receiving payments
  # @return [Float]      withdraw_fees                 fees for withdrawing.
  # @return [Float]      withdraw_ratio                The percentage for withdrawing
  # @return [Float]      deposite_fees                 fees for depositing
  # @return [Float]      deposite_ratio                The percentage for depositing
  # @return [datetime]   created_at                    date of creating the UsersGroup.
  # @return [datetime]   updated_at                    date of updating the UsersGroup.
  def show
    @local_limits , @online_limits, @crypto_limits = UsersGroup.group_bank_limit( @users_group.id)
  end

  # GET /users_groups/new
  # GET a new Users Groups params
  # @param [Integer]    id                                UsersGroup unique ID (Created automatically).
  # @param [String]     name                              UsersGroup's-name.
  # @param [String]     country_code                      UsersGroup's country code.
  # @param [Integer]    classification                    UsersGroup's classification.
  # @param [String]     description                       UsersGroup's description.
  # @param [Float]      upgrade_limit                     If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
  # @param [Float]      maximum_daily_send                The maximum amount allowed to be sent daily.
  # @param [Float]      maximum_monthly_send              The maximum amount allowed to be sent monthly.
  # @param [Float]      maximum_daily_recieve             The maximum amount allowed to be recieved daily - Not actually applied -.
  # @param [Float]      maximum_monthly_recieve           The maximum amount allowed to be recieved daily - Not actually applied -.
  # @param [Float]      minimum_send_value                The minimum amount allowed to be sent per time.
  # @param [Float]      maximum_send_value                The maximum amount allowed to be sent per time.
  # @param [Integer]    daily_send_number                 The maximum Number of sending times allowed daily.
  # @param [Float]      minimum_withdraw_per_time         The minimum amount allowed to be withdrawn per time.
  # @param [Float]      minimum_deposite_per_time         The minimum amount allowed to be deposited per time.
  # @param [Integer]    daily_withdraw_number             Maximum number of daily withdrawals allowed.
  # @param [Integer]    daily_deposite_number             The maximum number of times the deposit allowed daily.
  # @param {Float}      local_maximum_daily_withdraw      The maximum amount allowed to be withdrawn daily from local banks.
  # @param {Float}      local_maximum_monthly_withdraw    The maximum amount allowed to be withdrawn monthly from local banks.
  # @param {Float}      local_maximum_daily_deposite      The maximum amount allowed to be deposited daily to local banks.
  # @param {Float}      local_maximum_monthly_deposite    The maximum amount allowed to be deposited monthly to local banks.
  # @param {Float}      online_maximum_daily_withdraw     The maximum amount allowed to be withdrawn daily from online banks.
  # @param {Float}      online_maximum_monthly_withdraw   The maximum amount allowed to be withdrawn monthly from online banks.
  # @param {Float}      online_maximum_daily_deposite     The maximum amount allowed to be deposited daily to online banks.
  # @param {Float}      online_maximum_monthly_deposite   The maximum amount allowed to be deposited monthly to online banks.
  # @param {Float}      crypto_maximum_daily_withdraw     The maximum amount allowed to be withdrawn daily from cryptocurrencies.
  # @param {Float}      crypto_maximum_monthly_withdraw   The maximum amount allowed to be withdrawn monthly from
  # @param {Float}      crypto_maximum_daily_deposite     The maximum amount allowed to be deposited daily to cryptocurrencies.
  # @param {Float}      crypto_maximum_monthly_deposite   The maximum amount allowed to be deposited monthly to cryptocurrencies.        
  # @param [Float]      recieve_fees                      fees for receiving payments.
  # @param [Float]      recieve_ratio                     The percentage for receiving payments
  # @param [Float]      withdraw_fees                     fees for withdrawing.
  # @param [Float]      withdraw_ratio                    The percentage for withdrawing
  # @param [Float]      deposite_fees                     fees for depositing
  # @param [Float]      deposite_ratio                    The percentage for depositing
  
  def new
    @users_group = UsersGroup.new
  end

  # GET /users_groups/1/edit
  # GET an existing User Group to edit params

  def edit
    @local_limits , @online_limits, @crypto_limits = UsersGroup.group_bank_limit( @users_group.id)    
    @users_group = UsersGroup.edit_group_bank_params(@users_group.id, @local_limits , @online_limits, @crypto_limits)    

  end

  # POST /users_groups
  # POST /users_groups.json
  # POST a new Users Group and save it
  # @return [id]                                      UsersGroup unique ID (Created automatically).
  # @return [name]                                    UsersGroup's-name.
  # @return [country_code]                            UsersGroup's country code.
  # @return [classification]                          UsersGroup's classification.
  # @return [description]                             UsersGroup's description.
  # @return [upgrade_limit]                           If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
  # @return [maximum_daily_send]                      The maximum amount allowed to be sent daily.
  # @return [maximum_monthly_send]                    The maximum amount allowed to be sent monthly.
  # @return [maximum_daily_recieve]                   The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [maximum_monthly_recieve]                 The maximum amount allowed to be recieved daily - Not actually applied -.
  # @return [minimum_send_value]                      The minimum amount allowed to be sent per time.
  # @return [maximum_send_value]                      The maximum amount allowed to be sent per time.
  # @return [daily_send_number]                       The maximum Number of sending times allowed daily.
  # @return [minimum_withdraw_per_time]               The minimum amount allowed to be withdrawn per time.
  # @return [minimum_deposite_per_time]               The minimum amount allowed to be deposited per time.
  # @return [daily_withdraw_number]                   Maximum number of daily withdrawals allowed.
  # @return [daily_deposite_number]                   The maximum number of times the deposit allowed daily.
  # @return {local_maximum_daily_withdraw}            The maximum amount allowed to be withdrawn daily from local banks.
  # @return {local_maximum_monthly_withdraw}          The maximum amount allowed to be withdrawn monthly from local banks.
  # @return {local_maximum_daily_deposite}            The maximum amount allowed to be deposited daily to local banks.
  # @return {local_maximum_monthly_deposite}          The maximum amount allowed to be deposited monthly to local banks.
  # @return {online_maximum_daily_withdraw}           The maximum amount allowed to be withdrawn daily from online banks.
  # @return {online_maximum_monthly_withdraw}         The maximum amount allowed to be withdrawn monthly from online banks.
  # @return {online_maximum_daily_deposite}           The maximum amount allowed to be deposited daily to online banks.
  # @return {online_maximum_monthly_deposite}         The maximum amount allowed to be deposited monthly to online banks.
  # @return {crypto_maximum_daily_withdraw}           The maximum amount allowed to be withdrawn daily from cryptocurrencies.
  # @return {crypto_maximum_monthly_withdraw}         The maximum amount allowed to be withdrawn monthly from
  # @return {crypto_maximum_daily_deposite}           The maximum amount allowed to be deposited daily to cryptocurrencies.
  # @return {crypto_maximum_monthly_deposite}         The maximum amount allowed to be deposited monthly to cryptocurrencies.        
  # @return [recieve_fees]                            fees for receiving payments.
  # @return [recieve_ratio]                           The percentage for receiving payments
  # @return [withdraw_fees]                           fees for withdrawing.
  # @return [withdraw_ratio]                          The percentage for withdrawing
  # @return [deposite_fees]                           fees for depositing
  # @return [deposite_ratio]                          The percentage for depositing

  def create
    @users_group = UsersGroup.new(users_group_params)

    respond_to do |format|
      if @users_group.save
        UsersGroup.create_group_bank_params(@users_group.id.to_i,params)        
        format.html { redirect_to @users_group, notice: 'Users group was successfully created.' }
        format.json { render :show, status: :created, location: @users_group }
      else
        format.html { render :new }
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_groups/1
  # PATCH/PUT /users_groups/1.json
  def update
    respond_to do |format|
      if @users_group.update(users_group_params)
        @local_limits , @online_limits, @crypto_limits = UsersGroup.group_bank_limit( @users_group.id)
        UsersGroup.update_group_bank_params(params, @local_limits , @online_limits, @crypto_limits)
        format.html { redirect_to @users_group, notice: 'Users group was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_group }
      else
        format.html { render :edit }
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_groups/1
  # DELETE /users_groups/1.json
  def destroy
    GroupsBanksLimit.where(:group_id => @users_group.id).destroy_all
    @users_group.destroy
    respond_to do |format|
      format.html { redirect_to users_groups_url, notice: 'Users group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_group
      @users_group = UsersGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_group_params
      params.require(:users_group).permit(:country_code, :classification, :name, :description, :upgrade_limit, :maximum_daily_send, :maximum_monthly_send, :maximum_daily_recieve, :maximum_monthly_recieve, :minimum_send_value, :maximum_send_value, :daily_send_number, :minimum_withdraw_per_time, :minimum_deposite_per_time, :daily_withdraw_number, :daily_deposite_number, :recieve_fees, :recieve_ratio, :withdraw_fees, :withdraw_ratio, :deposite_fees, :deposite_ratio)
    end
end
