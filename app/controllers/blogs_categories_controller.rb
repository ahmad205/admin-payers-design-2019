class BlogsCategoriesController < ApplicationController
  before_action :set_blogs_category, only: [:show, :edit, :update, :destroy]

  # GET /blogs_categories
  # GET /blogs_categories.json
  def index
    @blogs_categories = BlogsCategory.all
  end

  # GET /blogs_categories/1
  # GET /blogs_categories/1.json
  def show
  end

  # GET /blogs_categories/new
  def new
    @blogs_category = BlogsCategory.new
  end

  # GET /blogs_categories/1/edit
  def edit
  end

  # POST /blogs_categories
  # POST /blogs_categories.json
  def create
    @blogs_category = BlogsCategory.new(blogs_category_params)

    respond_to do |format|
      if @blogs_category.save
        format.html { redirect_to @blogs_category, notice: 'Blogs category was successfully created.' }
        format.json { render :show, status: :created, location: @blogs_category }
      else
        format.html { render :new }
        format.json { render json: @blogs_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blogs_categories/1
  # PATCH/PUT /blogs_categories/1.json
  def update
    respond_to do |format|
      if @blogs_category.update(blogs_category_params)
        format.html { redirect_to @blogs_category, notice: 'Blogs category was successfully updated.' }
        format.json { render :show, status: :ok, location: @blogs_category }
      else
        format.html { render :edit }
        format.json { render json: @blogs_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs_categories/1
  # DELETE /blogs_categories/1.json
  def destroy
    @blogs_category.destroy
    respond_to do |format|
      format.html { redirect_to blogs_categories_url, notice: 'Blogs category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blogs_category
      @blogs_category = BlogsCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blogs_category_params
      params.require(:blogs_category).permit(:name, :description)
    end
end
