class UsersController < ApplicationController
  before_action :require_login , only: [:show, :edit, :update, :destroy,:index, :homepage, :add_admin, :add_admin_post]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_action :check2fa ,only: [:new, :destroy, :create]
  skip_before_action :check_active_session ,only: [:new, :destroy, :create]
  
  #include Clearance::Authentication 
  #before_action :get_google_auth ,only: [:show, :edit, :update,:index]
  require 'rqrcode'

  # show list of all users
  # return with details for all users, it works only if current user is an admin
  # @return [String] username
  # @return [String] email
  # @return [String] account_number
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def index
    if current_user.roleid == 1
    @users = User.all
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  def search

    @search = params[:search]
    @disabled_status = params[:disabled_status]

    @users = User.all
    @users = @users.where(["users.email LIKE ? OR users.account_number LIKE ?","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    @users = @users.where("users.disabled = ? ", @disabled_status)   if  @disabled_status.present?
   
  end

  # show data of user
  # @param [Integer] id 
  # @return [String] username
  # @return [String] email
  # @return [String] account_number
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
    if current_user.roleid == 1 
      @user = User.where("id =? " ,params[:id]).first
      @user_wallet = UserWallet.where(user_id: params[:id] ).first
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end
  
  # when activating google authentication, if there is a problem send the code to user's mail
  # send confirmation email if google authentication has a problem
  def send_confirmation_email
    @token = SecureRandom.hex(4)
    session[:token_code] = @token
    UserMailer.confirmsignin(current_user,@token).deliver_later
    redirect_to two_factor_path
  end
 
  # admin can show count of all users, active users and disabled users
  # @param [String] the selected status 
  # @return sum of the accounts which share this status
  def statistics
    @result = User.statics(params[:account_status].to_s)
  end

  # admin can enable or disable user's accounts
  # @param [Integer] user_id 
  def lock_unlock_account
    if current_user.roleid == 1
       @user = User.where("id = ?",params[:id]).first
       if @user.disabled == 1
          @user.disabled = 0
          @status = "enabled"
       else
          @user.disabled = 1
          @status = "disabled"
       end
       @user.save    
       redirect_to @user , notice: @user.firstname + ' ' + @user.lastname  + '  has been ' + @status 
       AuditLog.create(user_id: @user.id, user_name: current_user.uuid, action_type: "account " + @status , action_meta: "user account has been" + @status , ip: request.env['REMOTE_ADDR'])
    else
      redirect_to @user , notice: "not allowed" 
    end

  end


  # edit user account
  def edit
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    end
  end


  # set otp_secret_key for pre-registered users
  def set_otp_code    
        User.all.each { |user| user.update_attribute(:otp_secret_key, ROTP::Base32.random_base32) }
  end




  # update data of a user .
  # edit users data, only admins and account owner can edit this data.
  # @param [Integer] username 
  # @param [Integer] email
  # @param [password] password 
  # @param [Integer] country_id 
  # @param [Integer] firstname
  # @param [Integer] lastname
  # @param [Integer] active_otp
  # @param [boolean] account_currency
  # @param [password] confirm_password 
  # @return [Integer] username 
  # @return [Integer] email
  # @return [Integer] password 
  # @return [Integer] country_id 
  # @return [Integer] firstname
  # @return [Integer] lastname 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  def update
    if current_user.roleid == 1 
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
          AuditLog.create(user_id: @user.id, user_name: current_user.uuid, action_type: "Edit profile" , action_meta: "User was successfully updated." , ip: request.env['REMOTE_ADDR'])
        else
          #if (params[:user][:password])
           #  format.html { render :edit_password }
          #else
            format.html { render :edit }
          #end
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end    
  end

  # edit user password
  def edit_password
    @user = User.where("id =?", params[:id]).first
  end
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if current_user.roleid == 1
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end


  # on change confirmation way invoke this function to make changes
  # if mail confirmation is selected, send token to user mail, to confirm changes
  # if google authentacation has been selected, generate new QR,user read it and enter qrcode to comfirm changes
  # @param [String] active_otp
  def change_confirmation_code
    if params[:id] == "mail"
    @token = SecureRandom.hex(4)
    session[:token_code] = @token
    UserMailer.confirmsignin(current_user,@token).deliver_later
    @user = current_user
    @user.update_attribute(:otp_secret_key, '')
    render :json => { 'result': @token }
    elsif params[:id] == "google"
      @user = current_user
      @user.update_attribute(:otp_secret_key, ROTP::Base32.random_base32)
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
      render :json => { 'result': @qr  , 'otp_secret_key': @user.otp_secret_key}
    elsif params[:id] == "nochangemail"
      @user = current_user
      @user.update_attribute(:otp_secret_key, '')
    end
  end


  # check correctnes of the code in case of google code
  # @param  [String] code
  # @return [boolean] result
  def confirm_google_code
    @user = current_user
    @user_code = params[:code]
    if (@user.authenticate_otp(@user_code, drift: 60))
       @result = true
    else
      @result = false
    end 
    render :json => { 'result': @result }
  end

  # form to add new admin
  # @param [string] username
  # @param [string] firstname
  # @param [string] lastname
  # @param [string] email
  # @param [string] password 
  # @param [Integer] country_id 
  # @param [boolean] status 
  def add_user
    @user = User.new
  end

  # create new user
  # @return [Integer] id
  # @return [string] account_number
  # @return [Integer] username
  # @return [string] firstname
  # @return [string] lastname
  # @return [Integer] email
  # @return [Integer] password 
  # @return [Integer] country_id  
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [datetime] created_at
  # @return [datetime] updated_at
    def add_user_post

      require 'uri'
      require 'net/http'

      url = URI("https://payers-design-2019.herokuapp.com/api/v1/users")

      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Post.new(url)
      request["content-type"] = 'application/json'
      request["cache-control"] = 'no-cache'
      request.body = JSON.dump({    
        "username": params[:user][:username],
        "firstname": params[:user][:firstname],
        "lastname": params[:user][:lastname],
        "email": params[:user][:email],
        "password": params[:user][:password],
        "telephone": params[:user][:telephone],
        "country_id": params[:user][:country_id]
      })
      response = http.request(request)
      puts @res =  response.body

      @applications_list = ActiveSupport::JSON.decode(@res)

      if @applications_list
        if @applications_list["id"]
          redirect_to "/users/#{@applications_list["id"]}", notice: 'user was successfully created.'
        else
          redirect_back fallback_location: root_path, notice: "#{@applications_list}"
        end
      end

    end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:uuid, :users_group_id,  :firstname, :lastname, :language, :disabled, :loginattempts, :username, :email, :encrypted_password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :unlock_token, :refered_by, :avatar, :allowed_bank_level)
      
    end
end
