class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  # GET list of all tickets and display it
  # @return [id] ticket unique ID (Created automatically).
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @tickets = Ticket.joins("INNER JOIN ticket_departments ON ticket_departments.code = tickets.number").select("tickets.*,ticket_departments.name").all
    @department = TicketDepartment.all
    authorize @tickets
  end


  # GET a spacific ticket and display it
  # @param  [Integer] id ticket unique ID.
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    authorize @ticket
    @ticket_reply = TicketReply.where(:ticket_id => params[:id]).all
    @predefined_reply_categories = PredefinedReplyCategory.all
  end


  # GET a new ticket
  # @param [Integer] id ticket unique ID (Created automatically).
  # @param [String] number the ticket number.
  # @param [String] title the ticket title.
  # @param [String] content the ticket content.
  # @param [String] attachment the ticket attachment.
  def new
    @ticket = Ticket.new
    @department = TicketDepartment.where(:status => 1).all
    authorize @ticket
  end

  # POST a new ticket and save it
  # @return [id] ticket unique ID (Created automatically).
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @ticket = Ticket.new(ticket_params)
    @department = TicketDepartment.where(:status => 1).all
    @ticket.status = 0
    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
    authorize @ticket
  end

  # POST a new reply to a ticket and save it
  # @return [ticket_id] ticket unique ID.
  # @return [sender_type] ticket sender (0 for Admin , 1 for User).
  # @return [content] the ticket replies content.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create_reply
    authorize Ticket
    ticket_reply = TicketReply.new(ticket_reply_params)
    ticket_reply.sender_type = 0
    ticket_reply.sender_id = current_user.id
    if ticket_reply.save
      @ticket = Ticket.where(id: ticket_reply.ticket_id).first
      @ticket.update(responded: 1,status: 2)
      @smstext = "Payers Support has replied to your ticket with number #{@ticket.number}"
      @title = "Tickets Replies"
      @notification = access_notification(@smstext,@title,@ticket.user_id,"tickets",@ticket.id)
      if  @ticket.cc_recipient != nil
        EmailNotification.email_notification_setting(user_mail:@ticket.cc_recipient,subject:@title,text:@smstext)
      end
    redirect_back fallback_location: root_path, notice: 'Ticket reply was successfully created.'
    else 
      redirect_back fallback_location: root_path, notice: "the content can't be empty"
    end
  end
  
  # Close a specific ticket
  # @param [Integer] id ticket unique ID.
  def close_ticket
    authorize Ticket
    @ticket = Ticket.where(:id => params[:selected_tickets]).all
    @ticket.update(status: 3)
    @ticket.each do |ticket|
    @smstext = "You Payers Ticket with number #{ticket.number}#{ticket.id} was successfully Closed by Payers Support."
    @title = "Tickets Status Updates"
    @notification = access_notification(@smstext,@title,ticket.user_id,"tickets",@ticket.id)
    end
    redirect_back fallback_location: root_path, notice: 'Ticket was successfully Closed.'
  end

  def close_tickets
    authorize Ticket
    @ticket = Ticket.where(:id => params[:id]).first
    @ticket.update(status: 3)
    @smstext = "You Payers Ticket with number #{@ticket.number}#{@ticket.id} was successfully Closed by Payers Support."
    @title = "Tickets Status Updates"
    @notification = access_notification(@smstext,@title,@ticket.user_id,"tickets",@ticket.id)
    redirect_back fallback_location: root_path, notice: 'Ticket was successfully Closed.'
  end

  

  def changepriority
    authorize Ticket
    @ticket = Ticket.where(:id => params[:id]).update(reminder: params[:priority])
    redirect_back fallback_location: root_path, notice: 'Ticket priority was successfully changed.'
  end

  def changereminder
    authorize Ticket
    @ticket = Ticket.where(:id => params[:id]).update(reminder: params[:reminder], reminder_id: current_user.id)
    redirect_back fallback_location: root_path, notice: 'Ticket reminder was successfully added.'
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def statistics
    authorize Ticket
    @admin_reply = TicketReply.where(:sender_type => "Payers Support").joins("INNER JOIN admins ON admins.id = ticket_replies.sender_id").group('admins.username').count('ticket_replies.id')
    @number_of_tickets_per_department = Ticket.group('number').count('id')
    @tickets_type = Ticket.group('status').count('id')
    @tickets_number = Ticket.count('id')
  end


  def search
    @departments = TicketDepartment.all

    @name = params[:name]
    @email = params[:email]
    @recipient = params[:cc_recipient]
    @subject = params[:subject]
    @department = params[:department]
    @priority = params[:priority]
    @status = params[:status]
    @countries = Country.all
    
    @tickets = Ticket.joins("INNER JOIN ticket_departments ON ticket_departments.code = tickets.number").select("tickets.*,ticket_departments.name").all
    @tickets = @tickets.where("tickets.title LIKE ?","%#{@subject}%")   if  @subject.present?
    @tickets = @tickets.where("tickets.cc_recipient LIKE ?","%#{@recipient}%")   if  @recipient.present?
    @tickets = @tickets.where("tickets.status = ? ", @status)   if  @status.present?
    @tickets = @tickets.where("tickets.number = ? ", @department)   if  @department.present?
    @tickets = @tickets.where("tickets.priority = ? ", @priority)   if  @priority.present?
    @tickets = @tickets.joins("INNER JOIN users ON users.id = tickets.user_id").distinct.where("users.firstname LIKE ? OR users.lastname LIKE ?", "%#{@name}%","%#{@name}%")   if  @name.present?
    @tickets = @tickets.joins("INNER JOIN users ON users.id = tickets.user_id").distinct.where("users.email LIKE ?","%#{@email}%")   if  @email.present?


    # @Ticket = @Cards.where("cards.expired_at <= ? ", @searchdateto)   if  @searchdateto.present?
    # @Ticket = @Cards.where("cards.expired_at <= ? AND cards.expired_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    # @Ticket = @Cards.where(["cards.number LIKE ? OR cards.operation_id LIKE ? OR cast(cards.value as text) LIKE ?","%#{@search}%","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    # @Ticket = @Cards.where("cards.status = ? ", @status)   if  @status.present?
    # @Ticket = @Cards.where("cards.card_type = ? ", @card_type)   if  @card_type.present?
    # @Ticket = @Cards.where("cards.country_id = ? ", @country_id)   if  @country_id.present?
    # @Ticket = @Cards.joins("INNER JOIN users ON users.id = cards.user_id").distinct.where("users.account_number LIKE ? OR users.email LIKE ?", "%#{@search_user}%","%#{@search_user}%")   if  @search_user.present?

  end

  def searchpost
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    def access_notification(smstext,title,user_id,control,opid)

      @smstext = smstext
      @title = title
      @user_data = User.where(id: user_id.to_i).first
      @user_notification_setting = NotificationsSetting.where(user_id: user_id.to_i).first
        
      Notification.create(user_id: user_id.to_i ,title: @title, description: @smstext , notification_type: @user_notification_setting.help_tickets_updates, controller: control, opid: opid,admin_id: current_user.id)
        
      if @user_notification_setting.help_tickets_updates == 3
        SMSNotification.sms_notification_setting(@user_data.telephone,@smstext)
        SmsLog.create(:user_id => @user_data.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:@title,text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.help_tickets_updates(@user_data.telephone,@smstext)
        SmsLog.create(:user_id => @user_data.id, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.help_tickets_updates == 1
        EmailNotification.email_notification_setting(user_mail:@user_data.email,subject:@title,text:@smstext)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:number, :title, :content, :attachment, :status, :evaluation, :user_id, :priority, :cc_recipient)
    end

    def ticket_reply_params
      params.require(:ticket_reply).permit(:content, :ticket_id, :internal)
    end
end
