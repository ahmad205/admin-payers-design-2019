class SettingsController < ApplicationController
  before_action :set_setting, only: [:show, :edit, :update, :destroy]

  # GET /settings
  # GET /settings.json
  def index
    @settings = Setting.all
    @name = @settings.where(key: "website_name").first
    @language = @settings.where(key: "language").first
    @country = @settings.where(key: "default_country").first
    @time_zone = @settings.where(key: "time_zone").first
    @date_format = @settings.where(key: "date_format").first
    @maintenance_mode = @settings.where(key: "maintenance_mode").first
    @withdraw_msg = @settings.where(key: "withdraw_msg").pluck(:value).first
    @deposit_msg = @settings.where(key: "deposit_msg").pluck(:value).first
    @transfer_msg = @settings.where(key: "transfer_msg").pluck(:value).first
    @cards_msg = @settings.where(key: "cards_msg").pluck(:value).first
    @charging_card_status = @settings.where(key: "charging_card_status").pluck(:value).first
    @support_msg = @settings.where(key: "support_msg").pluck(:value).first
    @withdraw_status = @settings.where(key: "withdraw_status").pluck(:value).first
    @deposit_status = @settings.where(key: "deposit_status").pluck(:value).first
    @transfer_status = @settings.where(key: "transfer_status").pluck(:value).first
    @cards_status = @settings.where(key: "cards_status").pluck(:value).first
    @support_status = @settings.where(key: "support_status").pluck(:value).first
    @usd_to_sar = @settings.where(key: "usd_to_sar").pluck(:value).first
    @usd_to_egp = @settings.where(key: "usd_to_egp").pluck(:value).first
    @marketing_profits = @settings.where(key: "marketing_profits").pluck(:value).first
    @invitor_commision = @settings.where(key: "invitor_commision").pluck(:value).first
    @new_user_commision = @settings.where(key: "account_activation_reward").pluck(:value).first

    @email_data = YAML.load_file "config/mail_setting.yml"
  end

  # GET /settings/1
  # GET /settings/1.json
  def show
  end

  # GET /settings/new
  def new
    @setting = Setting.new
  end

  # GET /settings/1/edit
  def edit
  end


  def edit_settings

    @settings = Setting.all
    @name = @settings.where(key: "website_name").first.update(value: params[:website_name])
    @lang = @settings.where(key: "language").first.update(value: params[:language])
    @country = @settings.where(key: "default_country").first.update(value: params[:default_country])
    @zone = @settings.where(key: "time_zone").first.update(value: params[:time_zone])
    @format = @settings.where(key: "date_format").first.update(value: params[:date_format])
    @maintenance_mode = @settings.where(key: "maintenance_mode").first.update(value: params[:maintenance_mode])
    @marketing_profits = @settings.where(key: "marketing_profits").first.update(value: params[:marketing_profits])
    @invitor_commision = @settings.where(key: "invitor_commision").first.update(value: params[:invitor_commision])
    @new_user_commision = @settings.where(key: "account_activation_reward").first.update(value: params[:new_user_commision])

    redirect_back fallback_location: root_path, notice: 'General setting was successfully updated.'

  end

  def mail_setting

      @via = params[:via]
      @provider = params[:provider]
      @default_subject = params[:default_subject]
      @default_text = params[:default_text]
      @default_user_mail = params[:default_user_mail]
      @data = YAML.load_file "config/mail_setting.yml"
      @data["mail_setting"]["provider"] = @provider
      @data["mail_setting"]["via"] = @via
      @data["mail_setting"]["default_subject"] = @default_subject
      @data["mail_setting"]["default_text"] = @default_text
      @data["mail_setting"]["default_user_mail"] = @default_user_mail
      File.open("config/mail_setting.yml", 'w') { |f| YAML.dump(@data, f) }

      redirect_back fallback_location: root_path, notice: 'Email setting was successfully updated.'

  end 

  def money_setting
    @settings = Setting.all
    @withdraw_msg = @settings.where(key: "withdraw_msg").first.update(value: params[:withdraw_msg])
    @deposit_msg = @settings.where(key: "deposit_msg").first.update(value: params[:deposit_msg])
    @transfer_msg = @settings.where(key: "transfer_msg").first.update(value: params[:transfer_msg])
    @cards_msg = @settings.where(key: "cards_msg").first.update(value: params[:cards_msg])
    @charging_card_status = @settings.where(key: "charging_card_status").first.update(value: params[:charging_card_status])
    @support_msg = @settings.where(key: "support_msg").first.update(value: params[:support_msg])
    @withdraw_status = @settings.where(key: "withdraw_status").first.update(value: params[:withdraw_status])
    @deposit_status = @settings.where(key: "deposit_status").first.update(value: params[:deposit_status])
    @transfer_status = @settings.where(key: "transfer_status").first.update(value: params[:transfer_status])
    @cards_status = @settings.where(key: "cards_status").first.update(value: params[:cards_status])
    @support_status = @settings.where(key: "support_status").first.update(value: params[:support_status])
    @usd_to_sar = @settings.where(key: "usd_to_sar").first.update(value: params[:usd_to_sar])
    @usd_to_egp = @settings.where(key: "usd_to_egp").first.update(value: params[:usd_to_egp])
    
    redirect_back fallback_location: root_path, notice: 'Money setting was successfully updated.'
  end


  def payers_statistics
    @money_ops = MoneyOp.all
    @withdraw_ops = @money_ops.where(optype: 3).all
    @deposit_ops = @money_ops.where(optype: 4).all
    @transfers_ops = @money_ops.where(optype: 1).all
    @net_fees = @money_ops.where(status: 1).sum(:fees)
    @banks_withdraw_ops = @money_ops.where(optype: 3,status: 1).select("SUM(amount) as amount, count(amount) as count, SUM(fees) as fees  ,money_ops.payment_gateway")
    .group("money_ops.payment_gateway")
    @banks_deposit_ops = @money_ops.where(optype: 4,status: 1).select("SUM(amount) as amount, count(amount) as count, SUM(fees) as fees  ,money_ops.payment_gateway")
    .group("money_ops.payment_gateway")
  end

  # POST /settings
  # POST /settings.json
  def create
    @setting = Setting.new(setting_params)

    respond_to do |format|
      if @setting.save
        format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
        format.json { render :show, status: :created, location: @setting }
      else
        format.html { render :new }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /settings/1
  # PATCH/PUT /settings/1.json
  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.html { redirect_to @setting, notice: 'Setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @setting }
      else
        format.html { render :edit }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /settings/1
  # DELETE /settings/1.json
  def destroy
    @setting.destroy
    respond_to do |format|
      format.html { redirect_to settings_url, notice: 'Setting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def setting_params
      params.require(:setting).permit(:key, :value, :description, :attachment)
    end
end
