module Api
  module V1
    class TicketsController < ApplicationController
      before_action :set_ticket, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token
    
      # GET /tickets
      # GET /tickets.json
      # =begin
      # @api {get} /api/v1/tickets 1-Request tickets List
      # @apiVersion 0.3.0
      # @apiName GetTicket
      # @apiGroup Tickets
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/tickets
      # @apiSuccess {Number} id             Ticket unique ID.
      # @apiSuccess {string} number         Ticket unique number.
      # @apiSuccess {string} title          Ticket title.
      # @apiSuccess {string} content        Ticket content.
      # @apiSuccess {string} attachment     Ticket attachment.
      # @apiSuccess {Date}   created_at     Date created.
      # @apiSuccess {Date}   updated_at     Date Updated.
      # @apiSuccess {Number} responded      Ticket responded.
      # @apiSuccess {Number} status         Ticket status (0 for Open & 1 for OnHold & 2 for Answered & 3 for Closed).
      # @apiSuccess {Number} evaluation     Ticket evaluation (1 for Very Bad & 2 for Bad & 3 for Good & 4 for Very Good & 5 for Excellent).
      # @apiSuccess {Number} user_id        Ticke t's User ID.
      # @apiSuccess {String} cc_recipient   Ticket's User Recipient Email.
      # @apiSuccess {Number} priority       Ticket priority (1 for Low & 2 for Normal & 3 for High & 4 for Urgent & 5 for Report to management).
      # @apiSuccess {Date}   reminder       Ticket's reminder Date.
      # @apiSuccess {Number} reminder_id    Reminder's User ID.
      # @apiSuccess {Number} name           Tickets's Department Name.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # [
      #      {
      #         "id": 1,
      #         "number": "AA",
      #         "title": "aaaa",
      #         "content": "Content",
      #         "attachment": "Content",
      #         "created_at": "2018-12-12T11:52:59.914Z",
      #         "updated_at": "2019-01-29T12:20:02.888Z",
      #         "responded": 1,
      #         "status": "Answered",
      #         "evaluation": "Excellent",
      #         "user_id": 1,
      #         "cc_recipient": "omararansa10@gmail.com",
      #         "priority": "Normal",
      #         "reminder": null,
      #         "reminder_id": 3,
      #         "name": "department 1"
      #     },
      #     {
      #         "id": 2,
      #         "number": "AA",
      #         "title": "fff",
      #         "content": "Content1",
      #         "attachment": "Attachment1",
      #         "created_at": "2018-12-12T12:13:23.858Z",
      #         "updated_at": "2019-02-03T11:30:25.091Z",
      #         "responded": 1,
      #         "status": "Answered",
      #         "evaluation": null,
      #         "user_id": 1,
      #         "cc_recipient": null,
      #         "priority": "Low",
      #         "reminder": null,
      #         "reminder_id": 3,
      #         "name": "department 1"
      #     },
      # ]
      # @apiError MissingToken invalid Token.
      # @apiErrorExample Error-Response:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def index
        @tickets = Ticket.joins("INNER JOIN ticket_departments ON ticket_departments.code = tickets.number").select("tickets.*,ticket_departments.name").all
        respond_to do |format|
          format.json { render json: @tickets }
        end
      end
    
      # GET /tickets/1
      # GET /tickets/1.json
      # =begin
      # @api {get} /api/v1/tickets/{:id} 3-Request Specific Ticket
      # @apiVersion 0.3.0
      # @apiName GetSpecificTicket
      # @apiGroup Tickets
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/tickets/1
      # @apiParam {Number} id Ticket ID.
      # @apiSuccess {Number} id             Ticket unique ID.
      # @apiSuccess {string} number         Ticket unique number.
      # @apiSuccess {string} title          Ticket title.
      # @apiSuccess {string} content        Ticket content.
      # @apiSuccess {string} attachment     Ticket attachment.
      # @apiSuccess {Number} responded      Ticket responded.
      # @apiSuccess {Number} status         Ticket status (0 for Open & 1 for OnHold & 2 for Answered & 3 for Closed).
      # @apiSuccess {Number} evaluation     Ticket evaluation (1 for Very Bad & 2 for Bad & 3 for Good & 4 for Very Good & 5 for Excellent).
      # @apiSuccess {Number} user_id        Ticke t's User ID.
      # @apiSuccess {String} cc_recipient   Ticket's User Recipient Email.
      # @apiSuccess {Number} priority       Ticket priority (1 for Low & 2 for Normal & 3 for High & 4 for Urgent & 5 for Report to management).
      # @apiSuccess {Date}   reminder       Ticket's reminder Date.
      # @apiSuccess {Number} reminder_id    Reminder's User ID.
      # @apiSuccess {Number} name           Tickets's Department Name.
      # @apiSuccess {Number} id             Reply unique ID.
      # @apiSuccess {string} content        Reply content.
      # @apiSuccess {string} ticket_id      Ticket ID.
      # @apiSuccess {Number} sender_id      Ticket reply sender id.
      # @apiSuccess {Number} sender_type    Ticket reply sender type (0 for Payers Support & 1 for User).
      # @apiSuccess {Number} internal       Ticket reply internal.
      # @apiSuccess {Date} created_at       Date created.
      # @apiSuccess {Date} updated_at       Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #   {
      #     "ticket": {
      #         "id": 1,
      #         "number": "AA",
      #         "title": "aaaa",
      #         "content": "Content",
      #         "attachment": "Content",
      #         "created_at": "2018-12-12T11:52:59.914Z",
      #         "updated_at": "2019-01-29T12:20:02.888Z",
      #         "responded": 1,
      #         "status": "Answered",
      #         "evaluation": "Excellent",
      #         "user_id": 1,
      #         "cc_recipient": "omararansa10@gmail.com",
      #         "priority": "Normal",
      #         "reminder": null,
      #         "reminder_id": 3
      #     },
      #     "ticket_reply": [
      #         {
      #             "id": 25,
      #             "content": "من فضلك انتظر ",
      #             "created_at": "2019-02-03T13:56:41.930Z",
      #             "updated_at": "2019-02-03T13:56:41.930Z",
      #             "ticket_id": 1,
      #             "sender_type": "Payers Support",
      #             "sender_id": 3,
      #             "internal": 0
      #         },
      #         {
      #             "id": 26,
      #             "content": "من فضلك انتظر قليلا وسنقوم بالرد",
      #             "created_at": "2019-02-03T13:56:56.705Z",
      #             "updated_at": "2019-02-03T13:56:56.705Z",
      #             "ticket_id": 1,
      #             "sender_type": "Payers Support",
      #             "sender_id": 3,
      #             "internal": 1
      #         }
      #     ]
      # }
      # @apiError TicketNotFound The id of the Ticket was not found. 
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 404 Not Found
      #   {
      #     "error": "TicketNotFound"
      #   }
      # @apiError MissingToken invalid token.
      # @apiErrorExample Error-Response2:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def show
        @ticket_reply = TicketReply.where(:ticket_id => params[:id]).all
        respond_to do |format|
          format.json  { render :json => {:ticket => @ticket, :ticket_reply => @ticket_reply }}
        end
      end

      # GET /tickets/new
      def new
        @ticket = Ticket.new
      end
    
      # GET /tickets/1/edit
      def edit
      end

      # POST /tickets
      # POST /tickets.json
      # =begin
      # @api {post} /api/tickets 2-Create a new ticket
      # @apiVersion 0.3.0
      # @apiName PostTicket
      # @apiGroup Tickets
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/tickets \
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #   "number": "AA",
      #   "title": "new ticket",
      #   "content": "new Content",
      #   "attachment": "new attachment",
      #   "user_id": 1,
      #   "cc_recipient": "omararansa11@gmail.com",
      #   "priority": "Normal",
      # }'
      # @apiParam {string} number Ticket unique number.
      # @apiParam {string} title Ticket title.
      # @apiParam {string} content Ticket content.
      # @apiParam {string} attachment Ticket attachment.
      # @apiParam {Number} user_id        Ticke t's User ID.
      # @apiParam {String} cc_recipient   Ticket's User Recipient Email.
      # @apiParam {Number} priority       Ticket priority (1 for Low & 2 for Normal & 3 for High & 4 for Urgent & 5 for Report to management).
      # @apiSuccess {Number} id             Ticket unique ID.
      # @apiSuccess {string} number         Ticket unique number.
      # @apiSuccess {string} title          Ticket title.
      # @apiSuccess {string} content        Ticket content.
      # @apiSuccess {string} attachment     Ticket attachment.
      # @apiSuccess {Date}   created_at     Date created.
      # @apiSuccess {Date}   updated_at     Date Updated.
      # @apiSuccess {Number} responded      Ticket responded.
      # @apiSuccess {Number} status         Ticket status (0 for Open & 1 for OnHold & 2 for Answered & 3 for Closed).
      # @apiSuccess {Number} evaluation     Ticket evaluation (1 for Very Bad & 2 for Bad & 3 for Good & 4 for Very Good & 5 for Excellent).
      # @apiSuccess {Number} user_id        Ticke t's User ID.
      # @apiSuccess {String} cc_recipient   Ticket's User Recipient Email.
      # @apiSuccess {Number} priority       Ticket priority (1 for Low & 2 for Normal & 3 for High & 4 for Urgent & 5 for Report to management).
      # @apiSuccess {Date}   reminder       Ticket's reminder Date.
      # @apiSuccess {Number} reminder_id    Reminder's User ID.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # {
      #   "id": 15,
      #   "number": "AA",
      #   "title": "new ticket",
      #   "content": "new Content",
      #   "attachment": "new attachment",
      #   "created_at": "2019-02-05T11:09:04.430Z",
      #   "updated_at": "2019-02-05T11:09:04.430Z",
      #   "responded": 0,
      #   "status": null,
      #   "evaluation": null,
      #   "user_id": 1,
      #   "cc_recipient": "omararansa11@gmail.com",
      #   "priority": "Normal",
      #   "reminder": null,
      #   "reminder_id": null
      # }

      # @apiError MissingToken missing user name or password.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def create
        @ticket = Ticket.new(ticket_params)
        respond_to do |format|
          if @ticket.save
            format.json { render json: @ticket, status: :created, location: @ticket }
          else
            format.json { render json: @ticket.errors, status: :unprocessable_entity }
          end
        end
      end

      # =begin
      # @api {post} /api/tickets 5-Create a new ticket reply
      # @apiVersion 0.3.0
      # @apiName PostTicketReply
      # @apiGroup Tickets
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/tickets/reply \
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #   "content": "content5",
      #   "ticket_id": 3,
      #   "sender_type": 0,
      #   "sender_id": 3,
      #   "internal": 1
      # }'
      # @apiParam {string} content Reply content.
      # @apiParam {Number} ticket_id Ticket ID.
      # @apiParam {Number} sender_type Reply Sender Type.
      # @apiParam {Number} sender_id Reply Sender ID.
      # @apiParam {Number} internal Reply Internal.

      # @apiSuccess {Number} id Reply unique ID.
      # @apiSuccess {string} content Reply content.
      # @apiSuccess {Number} ticket_id Ticket ID.
      # @apiSuccess {Number} sender_type Reply Sender Type.
      # @apiSuccess {Number} sender_id Reply Sender ID.
      # @apiSuccess {Number} internal Reply Internal.
      # @apiSuccess {Date} created_at  Date created.
      # @apiSuccess {Date} updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #          "id": 11,
      #          "content": "content5",
      #          "created_at": "2018-09-12T13:15:58.000Z",
      #          "updated_at": "2018-09-12T13:15:58.000Z",
      #          "ticket_id": 3
      #          "sender_type": 0,
      #          "sender_id": 3,
      #          "internal": 1
      #     }
      # @apiError MissingToken missing user name or password.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def create_reply
        @ticket_reply = TicketReply.new(ticket_reply_params)
        @ticket_reply.save
        respond_to do |format|
          format.json { render json: @ticket_reply }
        end
      end

      # PATCH/PUT /tickets/1
      # PATCH/PUT /tickets/1.json
      def update
        # =begin
          # @api {put} /api/tickets/{:id} 6-Update an ticket
          # @apiVersion 0.3.0
          # @apiName PutTicket
          # @apiGroup Tickets
          # @apiExample Example usage:
          # curl -X PUT \
          # http://localhost:3000/api/tickets/6 \
          # -H 'cache-control: no-cache' \
          # -H 'content-type: application/json' \
          # -d '{
          #   "number": "AA",
          #   "title": "new ticket",
          #   "content": "new Content",
          #   "attachment": "new attachment",
          # }'
          # @apiParam {string} title Ticket title.
          # @apiParam {string} content Ticket content.
          # @apiParam {string} attachment Ticket attachment.
        
          # @apiSuccess {Number} id Ticket unique ID.
          # @apiSuccess {string} number Ticket unique number.
          # @apiSuccess {string} title Ticket title.
          # @apiSuccess {string} content Ticket content.
          # @apiSuccess {string} attachment Ticket attachment.
          # @apiSuccess {Date} created_at  Date created.
          # @apiSuccess {Date} updated_at  Date Updated.
          # @apiSuccessExample Success-Response:
          # HTTP/1.1 200 OK
          #   {
          #     "id": 15,
          #     "number": "AA",
          #     "title": "new ticket",
          #     "content": "new Content",
          #     "user_id": 1,
          #     "attachment": "new attachment",
          #     "created_at": "2019-02-05T11:09:04.430Z",
          #     "updated_at": "2019-02-05T11:09:04.430Z",
          #     "responded": 1,
          #     "status": null,
          #     "evaluation": null,
          #     "cc_recipient": "omararansa11@gmail.com",
          #     "priority": "Normal",
          #     "reminder": null,
          #     "reminder_id": null
          # }
          # @apiError MissingToken missing user name or password.
          # @apiErrorExample Error-Response1:
          # HTTP/1.1 422 Missing token
          #   {
          #     "error": "Missing token"
          #   }
          # @apiError TicketNotFound The id of the Ticket was not found. 
          # @apiErrorExample Error-Response2:
          # HTTP/1.1 404 Not Found
          #   {
          #     "error": "TicketNotFound"
          #   }
          # =end
        respond_to do |format|
          if @ticket.update(ticket_params)
            format.json { render json: @ticket, status: :ok, location: @ticket }
          else
            format.json { render json: @ticket.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /tickets/1
      # DELETE /tickets/1.json
      def destroy
        @ticket.destroy
        respond_to do |format|
          format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
          format.json { head :no_content }
        end
      end
    
      private
        # Use callbacks to share common setup or constraints between actions.
        def set_ticket
          @ticket = Ticket.find(params[:id])
        end
    
        # Never trust parameters from the scary internet, only allow the white list through.
        def ticket_params
          params.permit(:number, :title, :content, :attachment, :status, :evaluation, :user_id, :priority, :cc_recipient)
        end

        def ticket_reply_params
          params.permit(:content, :ticket_id, :sender_type, :sender_id, :internal)
        end
    end
  end
end