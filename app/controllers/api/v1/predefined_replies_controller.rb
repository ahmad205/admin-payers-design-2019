module Api
  module V1     
    class PredefinedRepliesController < ApplicationController
      before_action :set_predefined_reply, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /predefined_replies
      # GET /predefined_replies.json
      def index
        @predefined_replies = PredefinedReply.joins("INNER JOIN predefined_reply_categories ON predefined_reply_categories.id = predefined_replies.category_id").select("predefined_replies.*,predefined_reply_categories.name").all
        @predefined_reply_categories = PredefinedReplyCategory.all
      end

      # GET /predefined_replies/1
      # GET /predefined_replies/1.json
      # =begin
      # @api {get} /api/v1/predefined_replies/{:id} 3-Request Specific Predefined Reply
      # @apiVersion 0.3.0
      # @apiName GetSpecificPredefinedReply
      # @apiGroup Predefined replies
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/predefined_replies/7
      # @apiParam {Number}   id          predefined reply unique ID.
      # @apiSuccess {Number} id          predefined reply unique ID.
      # @apiSuccess {string} title       predefined reply title.
      # @apiSuccess {string} content     predefined reply content.
      # @apiSuccess {Number} category_id predefined reply category id.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccess {string} name        predefined reply category name.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 7,
      #     "title": "انتظار 1",
      #     "content": "من فضلك انتظر قليلا",
      #     "category_id": 4,
      #     "created_at": "2019-01-20T14:49:48.696Z",
      #     "updated_at": "2019-01-20T14:49:48.696Z",
      #     "name": "Support Users"
      # }
      # @apiError PredefinedReplyNotFound The id of the Predefined reply was not found. 
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 404 Not Found
      #   {
      #     "error": "PredefinedReplyNotFound"
      #   }
      # @apiError MissingToken invalid token.
      # @apiErrorExample Error-Response2:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def show
        @predefined_reply = PredefinedReply.joins("INNER JOIN predefined_reply_categories ON predefined_reply_categories.id = predefined_replies.category_id").select("predefined_replies.*,predefined_reply_categories.name").find(params[:id])    
        respond_to do |format|
            format.json { render json: @predefined_reply }
        end
      end


      # GET /predefined_replies/new
      def new
        @predefined_reply = PredefinedReply.new
        @category = PredefinedReplyCategory.all
      end

      # GET /predefined_replies/1/edit
      def edit
        @category = PredefinedReplyCategory.all
      end

      # POST /predefined_replies
      # POST /predefined_replies.json
      # =begin
      # @api {post} /api/v1/predefined_replies 2-Create a new Predefined Reply
      # @apiVersion 0.3.0
      # @apiName PostPredefinedReplies
      # @apiGroup Predefined replies
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/v1/predefined_replies
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #     "title": "test api",
      #     "content": "test content",
      #     "category_id": 4
      # }'
      # @apiParam {string} title       predefined reply title.
      # @apiParam {string} content     predefined reply content.
      # @apiParam {Number} category_id predefined reply category id.

      # @apiSuccess {Number} id          predefined reply unique ID.
      # @apiSuccess {string} title       predefined reply title.
      # @apiSuccess {string} content     predefined reply content.
      # @apiSuccess {Number} category_id predefined reply category id.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 25,
      #     "title": "test api",
      #     "content": "test content",
      #     "category_id": 4,
      #     "created_at": "2019-02-06T12:06:42.710Z",
      #     "updated_at": "2019-02-06T12:06:42.710Z"
      # }
      # @apiError MissingToken.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def create
        @predefined_reply = PredefinedReply.new(predefined_reply_params)
        @category = PredefinedReplyCategory.all

        respond_to do |format|
          if @predefined_reply.save
            format.json { render json: @predefined_reply, status: :created, location: @predefined_reply }
          else
            format.json { render json: @predefined_reply.errors, status: :unprocessable_entity }
          end
        end
      end















      # PATCH/PUT /predefined_replies/1
      # PATCH/PUT /predefined_replies/1.json
      # =begin
      # @api {put} /api/v1/predefined_replies/{:id} 4-Update an existing predefined reply
      # @apiVersion 0.3.0
      # @apiName PutPredefinedReply
      # @apiGroup Predefined replies
      # @apiExample Example usage:
      # curl -X PUT \
      # http://localhost:3000/api/v1/predefined_replies/25
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #     "title": "update title",
      #     "content": "update content",
      #     "category_id": 8
      # }'
      # @apiParam {string} title       predefined reply title.
      # @apiParam {string} content     predefined reply content.
      # @apiParam {Number} category_id predefined reply category id.
      # @apiSuccess {Number} id          predefined reply unique ID.
      # @apiSuccess {string} title       predefined reply title.
      # @apiSuccess {string} content     predefined reply content.
      # @apiSuccess {Number} category_id predefined reply category id.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 25,
      #     "title": "update title",
      #     "content": "update content",
      #     "category_id": 8,
      #     "created_at": "2019-02-06T12:06:42.710Z",
      #     "updated_at": "2019-02-06T12:20:34.835Z",
      #     "predefined_reply_category_id": null
      # }
      # @apiError MissingToken.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # @apiError PredefinedReplyNotFound The id of the Predefined Reply was not found. 
      # @apiErrorExample Error-Response2:
      # HTTP/1.1 404 Not Found
      #   {
      #     "error": "PredefinedReplyNotFound"
      #   }
      # =end
      def update
        respond_to do |format|
            if @predefined_reply.update(predefined_reply_params)
            format.json { render json: @predefined_reply, status: :ok, location: @predefined_reply }
            else
            format.json { render json: @predefined_reply.errors, status: :unprocessable_entity }
            end
        end
      end


      def allreplies
        @category_id = params[:category_id].to_i
        @category_replies = PredefinedReply.where(:category_id => @category_id ).all
        render :json => {'category': @category_replies }
      end

      def replycontent
        @reply_id = params[:reply_id].to_i
        @replycontent = PredefinedReply.where(:id => @reply_id ).pluck(:content).first
        render :json => {'content': @replycontent }
      end

      # DELETE /predefined_replies/1
      # DELETE /predefined_replies/1.json
      def destroy
        @predefined_reply.destroy
        respond_to do |format|
          format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_predefined_reply
          @predefined_reply = PredefinedReply.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def predefined_reply_params
          params.require(:predefined_reply).permit(:title, :content, :category_id)
        end
    end
  end
end