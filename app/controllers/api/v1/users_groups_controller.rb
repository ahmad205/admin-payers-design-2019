class Api::V1::UsersGroupsController < ApplicationController
  before_action :authenticate_request!
  before_action :set_users_group, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  attr_reader :current_user
  
    # GET /users_groups
    # GET /users_groups.json
    # =begin
    # @api {get} /api/v1/users_groups List all users groups
    # @apiVersion 0.3.0
    # @apiName GetUsersGroups
    # @apiGroup UsersGroups
    # @apiDescription get details of all users.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.fbVXWD42k6uNIIWfvmdj14BbjaS7eyofiZLTh9GWS9U" -i http://localhost:3000/api/v1/users_groups
    #
    # @apiSuccess {Integer}    id                            UsersGroup's-ID.
    # @apiSuccess {String}     name                          UsersGroup's-name.
    # @apiSuccess {String}     country_code                  UsersGroup's country code.
    # @apiSuccess {String}     classification                UsersGroup's classification.
    # @apiSuccess {String}     description                   UsersGroup's description.
    # @apiSuccess {Float}      upgrade_limit                 If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
    # @apiSuccess {Float}      maximum_daily_send            The maximum amount allowed to be sent daily.
    # @apiSuccess {Float}      maximum_monthly_send          The maximum amount allowed to be sent monthly.
    # @apiSuccess {Float}      maximum_daily_recieve         The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      maximum_monthly_recieve       The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      minimum_send_value            The minimum amount allowed to be sent per time.
    # @apiSuccess {Float}      maximum_send_value            The maximum amount allowed to be sent per time.
    # @apiSuccess {Integer}    daily_send_number             The maximum Number of sending times allowed daily.
    # @apiSuccess {Float}      minimum_withdraw_per_time     The minimum amount allowed to be withdrawn per time.
    # @apiSuccess {Float}      minimum_deposite_per_time     The minimum amount allowed to be deposited per time.
    # @apiSuccess {Integer}    daily_withdraw_number         Maximum number of daily withdrawals allowed.
    # @apiSuccess {Integer}    daily_deposite_number         The maximum number of times the deposit allowed daily.
    # @apiSuccess {Float}      recieve_fees                  fees for receiving payments.
    # @apiSuccess {Float}      recieve_ratio                 The percentage for receiving payments
    # @apiSuccess {Float}      withdraw_fees                 fees for withdrawing.
    # @apiSuccess {Float}      withdraw_ratio                The percentage for withdrawing
    # @apiSuccess {Float}      deposite_fees                 fees for depositing
    # @apiSuccess {Float}      deposite_ratio                The percentage for depositing
    # @apiSuccess {Datetime}   created_at                    date of creating the UsersGroup.
    # @apiSuccess {Datetime}   updated_at                    date of updating the UsersGroup.
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    #   {
    #  "id": 1,
    #  "name": "EG_class3",
    #  "country_code": "EGY",
    #  "classification": 3,
    #  "description": "third class for Egyptian Groups",
    #  "upgrade_limit": "10000",
    #  "maximum_daily_send": "1000",
    #  "maximum_monthly_send": "30000",
    #  "maximum_daily_recieve": "1000",
    #  "maximum_monthly_recieve": "30000",
    #  "minimum_send_value": "50",
    #  "maximum_send_value": "1000",
    #  "daily_send_number": "1",
    #  "minimum_withdraw_per_time": "100",
    #  "minimum_deposite_per_time": "100",
    #  "daily_withdraw_number": "1",
    #  "daily_deposite_number": "1",
    #  "recieve_fees": "5",
    #  "recieve_ratio": "0.05",
    #  "withdraw_fees": "5",
    #  "withdraw_ratio": "0.05",
    #  "deposite_fees": "5",
    #  "deposite_ratio": "0.05",
    #  "created_at": "2018-07-30T13:19:38.031Z",
    #  "updated_at": "2018-08-02T13:02:55.156Z",
    # },
    # @apiError NoAccessRight Only authenticated users can access the data.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end
  def index
    flash[:error]= "Not Allowed"
    respond_to do |format|
      if current_user.roleid == 1
        @users_groups = UsersGroup.all             
        format.json { render json: @users_groups }                
      else            
        format.json { render json: flash }
      end
    end
    
  end

    # GET /users_groups/1
    # GET /users_groups/1.json
    # =begin
    # @api {get} /api/users_groups/{:id} Get users Group Data
    # @apiVersion 0.3.0
    # @apiName GetUsersGroups
    # @apiGroup UsersGroups
    # @apiDescription get details of specific Users Group.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.fbVXWD42k6uNIIWfvmdj14BbjaS7eyofiZLTh9GWS9U" -i http://localhost:3000/api/v1/users_groups/1
    #
    # @apiSuccess {Integer}    id                            UsersGroup's-ID.
    # @apiSuccess {String}     name                          UsersGroup's-name.
    # @apiSuccess {String}     country_code                  UsersGroup's country code.
    # @apiSuccess {String}     classification                UsersGroup's classification.
    # @apiSuccess {String}     description                   UsersGroup's description.
    # @apiSuccess {Float}      upgrade_limit                 If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
    # @apiSuccess {Float}      maximum_daily_send            The maximum amount allowed to be sent daily.
    # @apiSuccess {Float}      maximum_monthly_send          The maximum amount allowed to be sent monthly.
    # @apiSuccess {Float}      maximum_daily_recieve         The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      maximum_monthly_recieve       The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      minimum_send_value            The minimum amount allowed to be sent per time.
    # @apiSuccess {Float}      maximum_send_value            The maximum amount allowed to be sent per time.
    # @apiSuccess {Integer}    daily_send_number             The maximum Number of sending times allowed daily.
    # @apiSuccess {Float}      minimum_withdraw_per_time     The minimum amount allowed to be withdrawn per time.
    # @apiSuccess {Float}      minimum_deposite_per_time     The minimum amount allowed to be deposited per time.
    # @apiSuccess {Integer}    daily_withdraw_number         Maximum number of daily withdrawals allowed.
    # @apiSuccess {Integer}    daily_deposite_number         The maximum number of times the deposit allowed daily.
    # @apiSuccess {Float}      recieve_fees                  fees for receiving payments.
    # @apiSuccess {Float}      recieve_ratio                 The percentage for receiving payments
    # @apiSuccess {Float}      withdraw_fees                 fees for withdrawing.
    # @apiSuccess {Float}      withdraw_ratio                The percentage for withdrawing
    # @apiSuccess {Float}      deposite_fees                 fees for depositing
    # @apiSuccess {Float}      deposite_ratio                The percentage for depositing
    # @apiSuccess {Datetime}   created_at                    date of creating the UsersGroup.
    # @apiSuccess {Datetime}   updated_at                    date of updating the UsersGroup.
    #
    # @apiError NoAccessRight Only authenticated users can access the data.
    # @apiError UsersGroupNotFound   The <code>id</code> of the Users Group was not found.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "Users Group NotFound"
    #    }
    # =end
  def show
    @users_group = UsersGroup.where("id =? " ,params[:id]).first
    @local_limits , @online_limits, @crypto_limits = UsersGroup.group_bank_limit( @users_group.id)
      respond_to do |format|
        if current_user.roleid == 1       
          format.json { render json: @users_group }
        else
          format.json { render json: "Not Allowed" }
        end
      end
  end


    # POST /users_groups
    # POST /users_groups.json
    # =begin
    # @api {post} /users_groups/ Create new Users Group
    # @apiVersion 0.3.0
    # @apiName postUsersGroup
    # @apiGroup UsersGroup
    # @apiDescription create new users group .
    # @apiParam {String}     firstname         User's First Name 
    # @apiParam {String}     lastname          User's Last Name 
    # @apiParam {Integer}    country_id        User's Country 
    # @apiParam {String}     email             User's Email
    # @apiParam {String}     username          User's Username
    # @apiParam {String}     password          User's Password
    # @apiParam {String}     refered_by        Invitation Code Of the Invitor
    #
    # @apiExample Example usage:       
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.fbVXWD42k6uNIIWfvmdj14BbjaS7eyofiZLTh9GWS9U" -X POST  http://localhost:3000/api/v1/users_groups -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"users_group": {"name": "EG_class3","country_code": "EGY","classification": "3", "description": "third class for Egyptian Groups","upgrade_limit": "10000","maximum_daily_send": "1000", "maximum_monthly_send": "30000","maximum_daily_recieve": "1000", "maximum_monthly_recieve": "30000","minimum_send_value": "50","maximum_send_value": "1000", "daily_send_number": "1", "minimum_withdraw_per_time": "100","minimum_deposite_per_time": "100", "daily_withdraw_number": "1","daily_deposite_number": "1","recieve_fees": "5", "recieve_ratio": "0.05","withdraw_fees": "5", "withdraw_ratio": "0.05","deposite_fees": "5","deposite_ratio": "0.05", "local_maximum_daily_withdraw": "3000", "local_maximum_monthly_withdraw": "30000","local_maximum_daily_deposite": "3000","local_maximum_monthly_deposite": "30000", "online_maximum_daily_withdraw": "3000", "online_maximum_monthly_withdraw": "30000", "online_maximum_daily_deposite": "3000","online_maximum_monthly_deposite": "30000", "crypto_maximum_daily_withdraw": "3000", "crypto_maximum_monthly_withdraw": "30000","crypto_maximum_daily_deposite": "3000","crypto_maximum_monthly_deposite": "30000"}}' 
    #
    # @apiSuccess {Integer}    id                                UsersGroup's-ID.
    # @apiSuccess {String}     name                              UsersGroup's-name.
    # @apiSuccess {String}     country_code                      UsersGroup's country code.
    # @apiSuccess {String}     classification                    UsersGroup's classification.
    # @apiSuccess {String}     description                       UsersGroup's description.
    # @apiSuccess {Float}      upgrade_limit                     If the user in this group reaches this value in receiving payments, He is upgraded automatically to a higher class in his group.
    # @apiSuccess {Float}      maximum_daily_send                The maximum amount allowed to be sent daily.
    # @apiSuccess {Float}      maximum_monthly_send              The maximum amount allowed to be sent monthly.
    # @apiSuccess {Float}      maximum_daily_recieve             The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      maximum_monthly_recieve           The maximum amount allowed to be recieved daily - Not actually applied -.
    # @apiSuccess {Float}      minimum_send_value                The minimum amount allowed to be sent per time.
    # @apiSuccess {Float}      maximum_send_value                The maximum amount allowed to be sent per time.
    # @apiSuccess {Integer}    daily_send_number                 The maximum Number of sending times allowed daily.
    # @apiSuccess {Float}      minimum_withdraw_per_time         The minimum amount allowed to be withdrawn per time.
    # @apiSuccess {Float}      minimum_deposite_per_time         The minimum amount allowed to be deposited per time.
    # @apiSuccess {Integer}    daily_withdraw_number             Maximum number of daily withdrawals allowed.
    # @apiSuccess {Integer}    daily_deposite_number             The maximum number of times the deposit allowed daily.
    # @apiSuccess {Float}      local_maximum_daily_withdraw      The maximum amount allowed to be withdrawn daily from local banks.
    # @apiSuccess {Float}      local_maximum_monthly_withdraw    The maximum amount allowed to be withdrawn monthly from local banks.
    # @apiSuccess {Float}      local_maximum_daily_deposite      The maximum amount allowed to be deposited daily to local banks.
    # @apiSuccess {Float}      local_maximum_monthly_deposite    The maximum amount allowed to be deposited monthly to local banks.
    # @apiSuccess {Float}      online_maximum_daily_withdraw     The maximum amount allowed to be withdrawn daily from online banks.
    # @apiSuccess {Float}      online_maximum_monthly_withdraw   The maximum amount allowed to be withdrawn monthly from online banks.
    # @apiSuccess {Float}      online_maximum_daily_deposite     The maximum amount allowed to be deposited daily to online banks.
    # @apiSuccess {Float}      online_maximum_monthly_deposite   The maximum amount allowed to be deposited monthly to online banks.
    # @apiSuccess {Float}      crypto_maximum_daily_withdraw     The maximum amount allowed to be withdrawn daily from cryptocurrencies.
    # @apiSuccess {Float}      crypto_maximum_monthly_withdraw   The maximum amount allowed to be withdrawn monthly from
    # @apiSuccess {Float}      crypto_maximum_daily_deposite     The maximum amount allowed to be deposited daily to cryptocurrencies.
    # @apiSuccess {Float}      crypto_maximum_monthly_deposite   The maximum amount allowed to be deposited monthly to cryptocurrencies.        
    # @apiSuccess {Float}      recieve_fees                  fees for receiving payments.
    # @apiSuccess {Float}      recieve_ratio                 The percentage for receiving payments
    # @apiSuccess {Float}      withdraw_fees                 fees for withdrawing.
    # @apiSuccess {Float}      withdraw_ratio                The percentage for withdrawing
    # @apiSuccess {Float}      deposite_fees                 fees for depositing
    # @apiSuccess {Float}      deposite_ratio                The percentage for depositing
    # @apiSuccess {Datetime}   created_at                    date of creating the UsersGroup.
    # @apiSuccess {Datetime}   updated_at                    date of updating the UsersGroup.
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "success": "200 ok"
    #    }
    #
    # @apiError NoAccessRight Only authenticated users can create User.
    #
    # @apiError MissingToken invalid-token.
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "error": "Missing Token"
    #    }
    # =end
  def create
    @users_group = UsersGroup.new(users_group_params)

    respond_to do |format|
      if @users_group.save
        UsersGroup.create_group_bank_params(@users_group.id.to_i,params) 
        format.json { render json: @users_group, status: :created }       
      else
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_groups/1
  # PATCH/PUT /users_groups/1.json
  def update
    respond_to do |format|
      if @users_group.update(users_group_params)
        @local_limits , @online_limits, @crypto_limits = UsersGroup.group_bank_limit( @users_group.id)
        UsersGroup.update_group_bank_params(params, @local_limits , @online_limits, @crypto_limits)
        format.html { redirect_to @users_group, notice: 'Users group was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_group }
      else
        format.html { render :edit }
        format.json { render json: @users_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_groups/1
  # DELETE /users_groups/1.json
  def destroy
    GroupsBanksLimit.where(:group_id => @users_group.id).destroy_all
    @users_group.destroy
    respond_to do |format|
      format.html { redirect_to users_groups_url, notice: 'Users group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_group
      @users_group = UsersGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_group_params
      params.require(:users_group).permit(:country_code, :classification, :name, :description, :upgrade_limit, :maximum_daily_send, :maximum_monthly_send, :maximum_daily_recieve, :maximum_monthly_recieve, :minimum_send_value, :maximum_send_value, :daily_send_number, :minimum_withdraw_per_time, :minimum_deposite_per_time, :daily_withdraw_number, :daily_deposite_number, :recieve_fees, :recieve_ratio, :withdraw_fees, :withdraw_ratio, :deposite_fees, :deposite_ratio)
    end
end
