module Api
  module V1    
    class PredefinedReplyCategoriesController < ApplicationController
      before_action :set_predefined_reply_category, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /predefined_reply_categories
      # GET /predefined_reply_categories.json
      # =begin
      # @api {get} /api/v1/predefined_reply_categories 1-Request predefined reply categories List
      # @apiVersion 0.3.0
      # @apiName GetPredefinedReplyCategories
      # @apiGroup Predefined reply categories
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/predefined_reply_categories
      # @apiSuccess {Number} id          category unique ID.
      # @apiSuccess {string} name        category name.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # [
      #     {
      #       "id": 4,
      #       "name": "Support Users",
      #       "created_at": "2019-01-20T14:50:08.261Z",
      #       "updated_at": "2019-01-31T13:19:30.956Z"
      #   },
      #   {
      #       "id": 5,
      #       "name": "Support admins",
      #       "created_at": "2019-01-30T12:06:45.225Z",
      #       "updated_at": "2019-01-30T12:06:45.225Z"
      #   }
      # ]
      # @apiError MissingToken invalid Token.
      # @apiErrorExample Error-Response:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def index
        @predefined_reply_categories = PredefinedReplyCategory.all
        respond_to do |format|
          format.json { render json: @predefined_reply_categories }
      end
      end

      # GET /predefined_reply_categories/1
      # GET /predefined_reply_categories/1.json
      # =begin
      # @api {get} /api/v1/predefined_reply_categories/{:id} 3-Request Specific Predefined Reply category
      # @apiVersion 0.3.0
      # @apiName GetSpecificPredefinedReplyCategory
      # @apiGroup Predefined reply categories
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/predefined_reply_categories/4
      # @apiParam {Number}   id          category unique ID.
      # @apiSuccess {Number} id          category unique ID.
      # @apiSuccess {string} name        category name.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 4,
      #     "name": "Support Users",
      #     "created_at": "2019-01-20T14:50:08.261Z",
      #     "updated_at": "2019-01-31T13:19:30.956Z"
      #     }
      # @apiError PredefinedReplyCategoryNotFound The id of the Predefined reply category was not found. 
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 404 Not Found
      #   {
      #     "error": "PredefinedReplyCategoryNotFound"
      #   }
      # @apiError MissingToken invalid token.
      # @apiErrorExample Error-Response2:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def show
        respond_to do |format|
            format.json { render json: @predefined_reply_category }
        end
      end











      # GET /predefined_reply_categories/new
      def new
        @predefined_reply_category = PredefinedReplyCategory.new
      end

      # GET /predefined_reply_categories/1/edit
      def edit
      end

      # POST /predefined_reply_categories
      # POST /predefined_reply_categories.json
      # =begin
      # @api {post} /api/v1/predefined_reply_categories 2-Create a new Predefined Reply category
      # @apiVersion 0.3.0
      # @apiName PostPredefinedReplyCategories
      # @apiGroup Predefined reply categories
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/v1/predefined_reply_categories
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #       "name": "test category"
      #     }'
      # @apiParam {string} name        category name.
      # @apiSuccess {Number} id          category unique ID.
      # @apiSuccess {string} name        category name.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 8,
      #     "name": "test category",
      #     "created_at": "2019-02-06T11:47:40.313Z",
      #     "updated_at": "2019-02-06T11:47:40.313Z"
      #      }
      # @apiError MissingToken missing user name or password.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      def create
        @predefined_reply_category = PredefinedReplyCategory.new(predefined_reply_category_params)

        respond_to do |format|
          if @predefined_reply_category.save
            format.json { render json: @predefined_reply_category, status: :created, location: @predefined_reply_category }
          else
            format.json { render json: @predefined_reply_category.errors, status: :unprocessable_entity }
          end
        end
      end

     











      # PATCH/PUT /predefined_reply_categories/1
      # PATCH/PUT /predefined_reply_categories/1.json
      # =begin
      # @api {put} /api/v1/predefined_reply_categories/{:id} 4-Update an existing predefined reply category
      # @apiVersion 0.3.0
      # @apiName PutPredefinedReplyCategories
      # @apiGroup Predefined reply categories
      # @apiExample Example usage:
      # curl -X PUT \
      # http://localhost:3000/api/v1/predefined_reply_categories/8
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #       "name": "update category"
      #     }'
      # @apiParam   {string} name        category name.
      # @apiSuccess {Number} id          category unique ID.
      # @apiSuccess {string} name        category name.
      # @apiSuccess {Date}   created_at  Date created.
      # @apiSuccess {Date}   updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      #     {
      #     "id": 8,
      #     "name": "update category",
      #     "created_at": "2019-02-06T11:47:40.313Z",
      #     "updated_at": "2019-02-06T12:10:42.655Z"
      # }
      # @apiError MissingToken.
      # @apiErrorExample Error-Response1:
      # HTTP/1.1 422 Missing token
      #   {
      #     "error": "Missing token"
      #   }
      # @apiError PredefinedReplyCategoryNotFound The id of the Predefined Reply Category was not found. 
      # @apiErrorExample Error-Response2:
      # HTTP/1.1 404 Not Found
      #   {
      #     "error": "PredefinedReplyCategoryNotFound"
      #   }
      # =end
      def update
        respond_to do |format|
            if @predefined_reply_category.update(predefined_reply_category_params)
            format.json { render json: @predefined_reply_category, status: :ok, location: @predefined_reply_category }
            else
            format.json { render json: @predefined_reply_category.errors, status: :unprocessable_entity }
            end
        end
      end


      

      # DELETE /predefined_reply_categories/1
      # DELETE /predefined_reply_categories/1.json
      def destroy
        @predefined_reply_category.destroy
        respond_to do |format|
          format.html { redirect_to predefined_reply_categories_url, notice: 'Predefined reply category was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_predefined_reply_category
          @predefined_reply_category = PredefinedReplyCategory.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def predefined_reply_category_params
          params.require(:predefined_reply_category).permit(:name)
        end
    end
  end
end
