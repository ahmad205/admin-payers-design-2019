module Api
  module V1
    class CardsLogsController < ApplicationController
      before_action :set_cards_log, only: [:show]

      # GET /cards_logs
      def index
        @cards_logs = CardsLog.all
        respond_to do |format|
          format.json { render json: @cards_logs }
        end

        # =begin
        # @api {get} /api/v1/cards_logs 1-Request card log List
        # @apiVersion 0.3.0
        # @apiName GetCardLog
        # @apiGroup Cards Logs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_logs
        # @apiSuccess {Number} id card log unique id.
        # @apiSuccess {Number} user_id card unique user id.
        # @apiSuccess {Number} action_type card log type (1 for created ,2 for charged ,3 for deleted).
        # @apiSuccess {String} ip card log user interface ip.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccess {Number} card_id card unique id.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "action_type": 2,
        #       "ip": "127.0.0.1",
        #       "created_at": "2018-09-09T08:10:43.950Z",
        #       "updated_at": "2018-09-09T08:10:43.950Z",
        #       "card_id": 36
        #   },
        #   {
        #       "id": 2,
        #       "user_id": 1,
        #       "action_type": 3,
        #       "ip": "127.0.0.1",
        #       "created_at": "2018-09-09T08:13:16.518Z",
        #       "updated_at": "2018-09-09T08:13:16.518Z",
        #       "card_id": 5
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET /cards_logs/1
      def show

        respond_to do |format|
          format.json { render json: @cards_log }
        end

        # =begin
        # @api {get} /api/cards_logs/{:id} 2-Request Specific Card Log
        # @apiVersion 0.3.0
        # @apiName GetSpecificCardLog
        # @apiGroup Cards Logs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_logs/2
        # @apiParam {Number} id card log unique id.
        # @apiSuccess {Number} user_id card unique user id.
        # @apiSuccess {Number} action_type card log type (1 for created ,2 for charged ,3 for deleted).
        # @apiSuccess {String} ip card log user interface ip.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccess {Number} card_id card unique id.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 2,
        #   "user_id": 1,
        #   "action_type": 1,
        #   "ip": "127.0.0.1",
        #   "created_at": "2018-09-09T07:52:48.179Z",
        #   "updated_at": "2018-09-09T07:52:48.179Z",
        #   "card_id": 36
        # }
        # @apiError CardLogNotFound The id of the Card log was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CardLogNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end


      private
        # Use callbacks to share common setup or constraints between actions.
        def set_cards_log
          @cards_log = CardsLog.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def cards_log_params
          params.require(:cards_log).permit(:user_id, :action_type, :ip, :card_id)
        end
    end
  end
end
