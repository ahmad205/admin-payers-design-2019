class Api::V1::AdminsController < ApplicationController
  before_action :authenticate_request!, except: [:unlockaccount, :confirmmail, :resend_confirmmail]
  before_action :set_admin, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
    attr_reader :current_user
  
  include Clearance::Authentication 
  
  # show list of all admins
   
    # GET /admins
    # GET /admins.json
    # =begin
    # @api {get} /api/admins Get Admin Data
    # @apiVersion 0.3.0
    # @apiName GetAdmin
    # @apiGroup Admin
    # @apiDescription get details of specific admin.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3fQ.SxMk2hw4XawJf5Ko--Ue68Tw8ps6IG2xOv3gFzIvd80" -i http://localhost:3000/api/v1/admins
    #
    # @apiSuccess {Integer}    id                   Admins-ID.
    # @apiSuccess {String}     firstname            Admin firstname.
    # @apiSuccess {String}     lastname             Admin lastname.
    # @apiSuccess {String}     email                Admin email.
    # @apiSuccess {Integer}    country_id           Admin country.
    # @apiSuccess {Datetime}   created_at           date of creating the Admin.
    # @apiSuccess {Datetime}   updated_at           date of updating the Admin.
    #
    # @apiError NoAccessRight Only authenticated admins can access the data.
    # @apiError AdminNotFound   The <code>id</code> of the Admin was not found.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "AdminNotFound"
    #    }
    # =end
  def index
    @admins = Admin.all
      respond_to do |format|
        if current_user.roleid == 1       
          format.json { render json: @admins }
        else
          format.json { render json: "Not Allowed" }
        end
      end
  end

    # GET /admins/1
    # GET /admins/1.json
    # =begin
    # @api {get} /api/admins/{:id} Get Admin Data
    # @apiVersion 0.3.0
    # @apiName GetAdmin
    # @apiGroup Admin
    # @apiDescription get details of specific admin.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3fQ.SxMk2hw4XawJf5Ko--Ue68Tw8ps6IG2xOv3gFzIvd80" -i http://localhost:3000/api/v1/admins/1
    #
    # @apiSuccess {Integer}    id                   Admins-ID.
    # @apiSuccess {String}     firstname            Admins firstname.
    # @apiSuccess {String}     lastname             Admins lastname.
    # @apiSuccess {String}     email                Admins email.
    # @apiSuccess {Integer}    country_id           Admins country.
    # @apiSuccess {Datetime}   created_at           date of creating the Admins.
    # @apiSuccess {Datetime}   updated_at           date of updating the Admins.
    #
    # @apiError NoAccessRight Only authenticated admins can access the data.
    # @apiError AdminNotFound   The <code>id</code> of the Admin was not found.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "UserNotFound"
    #    }
    # =end
  def show
    @admin = Admin.where("id =? " ,params[:id]).first
    respond_to do |format|
      if current_user.roleid == 1 or current_user.id == params[:id].to_i       
        format.json { render json: @admin }
      else
        format.json { render json: "Not Allowed" }
      end
    end
  end
 
  # admin can show count of all users, active users and disabled users
  # @param [String] the selected status 
  # @return sum of the accounts which share this status
  def statistics
    @result = Admin.statics(params[:account_status].to_s)
  end

  def homepage   
    @all_users = Admin.all
    @count_admins = @all_users.where("roleid = ?",1).count
    @count_users = @all_users.where("roleid = ?",2).count
    @disabled_users = @all_users.where("disabled = ?",1).count
  end

  # after number of failed login attempts, user's account will be disabled and link will be send to user's mail. 
  # when the user click the link, he is redirected here to unlock account and allow him to signin again. 
  # @param [Integer] user_id 
  # @param [String] token 
  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = Admin.where("id =?",@id ).first
    if @token  == @user.unlock_token      
       @user.failedattempts = 0
       @user.disabled = 0
       @user.unlock_token = ""
       @user.save
       flash.now.notice = 'Your account is Unlocked,please sign in .'
       render template: "sessions/new"  
    else
      flash.now.notice = 'InValid Token .'
      render template: "sessions/new"  
    end
  end

    # POST /admins
    # POST /admins.json
    # =begin
    # @api {post} /admins/ Create new Admin
    # @apiVersion 0.3.0
    # @apiName postAdmin
    # @apiGroup Admin
    # @apiDescription create new admin.
    # @apiParam {String}     firstname         Admin's First Name 
    # @apiParam {String}     lastname          Admin's Last Name 
    # @apiParam {Integer}    country_id        Admin's Country 
    # @apiParam {String}     email             Admin's Email
    # @apiParam {String}     password          Admin's Password
    #
    # @apiExample Example usage:       
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo3fQ.SxMk2hw4XawJf5Ko--Ue68Tw8ps6IG2xOv3gFzIvd80" -X POST  http://localhost:3000/api/v1/admins -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"admin": {"firstname": "amira2","lastname": "amira2","password": "Amira123456", "country_id": 1,"email": "tessst@gmail.com"}}' 
    # @apiSuccess {Integer}     id                   Admin's-ID.
    # @apiSuccess {String}      firstname            Admin's firstname.
    # @apiSuccess {String}      lastname             Admin's lastname.
    # @apiSuccess {String}      email                Admin's email.
    # @apiSuccess {Integer}     country_id           Admin's country.
    # @apiSuccess {String}      secret_code          Admin's Secret Code
    # @apiSuccess {String}      telephone            Admin's telephone
    # @apiSuccess {Integer}     account_currency     Admin's currency
    # @apiSuccess {Integer}     active_otp           Admin's active two factor authentication
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "success": "200 ok"
    #    }
    #
    # @apiError NoAccessRight Only authenticated users can create User.
    #
    # @apiError MissingToken invalid-token.
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "error": "Missing Token"
    #    }
    # =end
  def create
    @admin = Admin.new(admin_params)
    respond_to do |format|
      if @admin.save
        @acc_num =  "PS1" + "%08d" % @admin.id.to_i 
        @admin.update(:account_number => @acc_num, :status => 1)
        AdminWatchdog.create(:admin_id => @admin.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now ,:operation_type => "sign_up")
        format.json { render json: @admin, status: :created }
      else
        format.json { render json: @admin.errors, status: :unprocessable_entity }

      end
    end
  end

  # update data of a user .
  # edit users data, only admins and account owner can edit this data.
  # @param [Integer] username 
  # @param [Integer] email
  # @param [password] password 
  # @param [Integer] country_id 
  # @param [Integer] firstname
  # @param [Integer] lastname
  # @param [Integer] active_otp
  # @param [boolean] account_currency
  # @param [password] confirm_password 
  # @return [Integer] username 
  # @return [Integer] email
  # @return [Integer] password 
  # @return [Integer] country_id 
  # @return [Integer] firstname
  # @return [Integer] lastname 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  def update
    if params[:admin][:password] and current_user.id == params[:id].to_i
        @admin = Admin.authenticate(current_user.email,params[:admin][:confirm_password])
        if !(@admin)          
             redirect_to edit_admin_path(params[:id]) , notice: "wrong password confirmation" and return
        end
    elsif params[:admin][:password] and current_user.id != params[:id].to_i
      redirect_to edit_admin_path(params[:id]) , notice: "Not allowed" and return
    end
    if current_user.roleid != 1 and !(params[:admin][:active_otp])
        @admin = Admin.authenticate(current_user.email,params[:admin][:confirm_password])
        if !(@admin)
          #if (params[:admin][:password])
           #   redirect_to edit_password_path(params[:id]) , notice: "wrong password confirmation" and return
          #else
             redirect_to edit_admin_path(params[:id]) , notice: "wrong password confirmation" and return
          #end
        end
    end
    if current_user.roleid == 1 or current_user.id == params[:id].to_i
     respond_to do |format|
        if @admin.update(admin_params)
          format.html { redirect_to @admin, notice: 'Admin was successfully updated.' }
          format.json { render :show, status: :ok, location: @admin }
        else
          #if (params[:admin][:password])
           #  format.html { render :edit_password }
          #else
            format.html { render :edit }
          #end
          format.json { render json: @admin.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end    
  end

  # edit user password
  def edit_password
    @user = Admin.where("id =?", params[:id]).first
  end
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if current_user.roleid == 1
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params
      params.require(:admin).permit(:uuid, :roleid,  :firstname, :lastname, :language, :disabled, :loginattempts, :username, :email, :password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :unlock_token, :refered_by, :avatar)
      
    end
end
