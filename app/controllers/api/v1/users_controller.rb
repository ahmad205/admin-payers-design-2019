class Api::V1::UsersController < ApplicationController
    before_action :authenticate_request!, except: [:create, :confirmmail, :resend_confirmmail]
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    skip_before_action :verify_authenticity_token
    attr_reader :current_user
    
    # GET /users
    # GET /users.json
    # =begin
    # @api {get} /api/v1/users List all users
    # @apiVersion 0.3.0
    # @apiName GetUsers
    # @apiGroup User
    # @apiDescription get details of all users.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMn0.HjzZehfkPaDES35pXYwdlXNydDMWhwayCjMBs5KUcOE" -i http://localhost:3000/api/v1/users
    #
    # @apiSuccess {Integer}    id                   User's-ID.
    # @apiSuccess {Integer}    uuid                 User's-uuid.
    # @apiSuccess {String}     account_number       User's account_number.
    # @apiSuccess {String}     telephone            User's telephone.
    # @apiSuccess {Integer}    roleid               User's role id.
    # @apiSuccess {String}     username             User's username.
    # @apiSuccess {String}     firstname            User's first name.
    # @apiSuccess {String}     lastname             User's last name.
    # @apiSuccess {Integer}    disabled             User's account status.
    # @apiSuccess {Integer}    status               Email confirmation status.
    # @apiSuccess {Integer}    loginattempts        User's login attempts.
    # @apiSuccess {Integer}    failedattempts       User's failed attempts.
    # @apiSuccess {Integer}    account_currency     User's account currency.
    # @apiSuccess {Integer}    country_id           User's country id.
    # @apiSuccess {String}     email                User's email.
    # @apiSuccess {Integer}    country_id           User's country.
    # @apiSuccess {Datetime}   created_at           date of creating the User.
    # @apiSuccess {Datetime}   updated_at           date of updating the User.
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    #   {
    # "id": 1,
    #  "uuid": "8dddd2c5",
    #  "roleid": 1,
    #  "username": "amira",
    #  "firstname": "amira",
    #  "lastname": "amira2",
    #  "disabled": 1,
    #  "loginattempts": 7,
    #  "status": 1,
    #  "created_at": "2018-07-30T13:19:38.031Z",
    #  "updated_at": "2018-08-02T13:02:55.156Z",
    #  "email": "amira.elfayhhome@servicehigh.com",
    #  "failedattempts": 0,
    #  "account_number": "PS100000001",
    #  "telephone": "00201007460059",
    #  "account_currency": 0,
    #  "country_id": 1
    # },
    # {
    # "id": 2,
    #  "uuid": "dd89adb1",
    #  "roleid": 1,
    #  "username": "amira2",
    #  "firstname": "amira2",
    #  "lastname": "amira2",
    #  "disabled": 1,
    #  "loginattempts": 0,
    #  "status": 0,
    #  "created_at": "2018-07-30T13:19:58.031Z",
    #  "updated_at": "2018-08-02T13:02:55.156Z",
    #  "email": "amira.elfayhhome@servicehigh.com",
    #  "failedattempts": 0,
    #  "account_number": "PS100000002",
    #  "telephone": "0020102982852",
    #  "account_currency": 0,
    #  "country_id": 1
    # }
    # @apiError NoAccessRight Only authenticated users can access the data.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end

  def index
    flash[:error]= "Not Allowed"
    respond_to do |format|
        if current_user.roleid == 1
          @users = User.all             
          format.json { render json: @users }                
        else            
          format.json { render json: flash }
        end
    end
  end

    # GET /users/1
    # GET /users/1.json
    # =begin
    # @api {get} /api/users/{:id} Get User Data
    # @apiVersion 0.3.0
    # @apiName GetUser
    # @apiGroup User
    # @apiDescription get details of specific user.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/users/1
    #
    # @apiSuccess {Integer}    id                   Users-ID.
    # @apiSuccess {String}     username             Users username.
    # @apiSuccess {String}     firstname            Users firstname.
    # @apiSuccess {String}     lastname             Users lastname.
    # @apiSuccess {String}     email                Users email.
    # @apiSuccess {Integer}    country_id           Users country.
    # @apiSuccess {Datetime}   created_at           date of creating the User.
    # @apiSuccess {Datetime}   updated_at           date of updating the User.
    #
    # @apiError NoAccessRight Only authenticated users can access the data.
    # @apiError UserNotFound   The <code>id</code> of the User was not found.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "UserNotFound"
    #    }
    # =end

  def show    
    @user = User.where("id =? " ,params[:id]).first
      respond_to do |format|
        if current_user.roleid == 1 or current_user.id == params[:id].to_i        
          format.json { render json: @user }
          # @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
        else
          format.json { render json: "Not Allowed" }
        end
      end
  end
 
    # GET /statistics 
    # =begin
    # @api {get} /api/v1/statistics?&account_status={:search_status} Get Users Statistics
    # @apiVersion 0.3.0
    # @apiName GetUsersstatistics
    # @apiGroup User
    # @apiDescription get statistics of all users.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -i 'http://localhost:3000/api/v1/statistics?&account_status=confirmed'
    #
    # @apiSuccess {Integer}    id                   Users-ID.
    # @apiSuccess {String}     username             Users username.
    # @apiSuccess {String}     firstname            Users firstname.
    # @apiSuccess {String}     lastname             Users lastname.
    # @apiSuccess {String}     email                Users email.
    # @apiSuccess {Integer}    country_id           Users country.
    # @apiSuccess {Datetime}   created_at           date of creating the User.
    # @apiSuccess {Datetime}   updated_at           date of updating the User.
    #
    # @apiError NoAccessRight Only authenticated users can access the data.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end

  def statistics
      @result = User.statics(params[:account_status].to_s)
      respond_to do |format|
        if current_user.roleid == 1
          format.json { render json: @result }
        else
          format.json { render json: "Not Allowed" }
        end
      end
  end

    #disable/enable account
    # GET /lock_unlock_account 
    # =begin
    # @api {get} /api/v1/lock_unlock_account?id={:user-id} change account status
    # @apiVersion 0.3.0
    # @apiName GetAccountStatus
    # @apiGroup User
    # @apiDescription change account status for user.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.fbVXWD42k6uNIIWfvmdj14BbjaS7eyofiZLTh9GWS9U" -i http://localhost:3000/api/v1/lock_unlock_account?id=1
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # {
    # "mseeage": "user has been enabled/disabled"
    # }
    #
    # @apiError NoAccessRight Only authenticated users can perform this action.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end 

  def lock_unlock_account
      @user = User.where("id = ?",params[:id].to_i).first
    if @user.disabled == 1
        @user.disabled = 0
        @status = "enabled"
    else
        @user.disabled = 1
        @status = "disabled"
    end   
    respond_to do |format|
      if current_user.roleid == 1
        @user.save
        format.json { render json: @user.username + " has been " + @status }
      else
        format.json { render json: "Not Allowed" }
      end
    end 
  end

  def homepage   
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/homepage
    render json: {'logged_in' => true} 
  end

    # confirm user mail after registeration
    # POST /confirm user mail after registeration
    # =begin
    # @api {post} /api/v1/confirmmail confirm user mail
    # @apiVersion 0.3.0
    # @apiName POSTconfirmmail
    # @apiGroup User
    # @apiDescription confirm user mail after registeration.
    # @apiExample Example usage:
    # curl -X POST  http://localhost:3000/api/v1/confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"email": "engamira333@gmail.com","token": "3aa4e716"}' 
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # {
    #   "result : true"
    # }
    #
    # @apiError required_parameter_missing wrong code .
    # @apiError required_parameter_missing2 wrong email .
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 400 Badrequest
    #     {
    #       "result : false"
    #       "error": "MissingData"
    #       "status": 400- Bad Request
    #    }
    # =end 

  def confirmmail
      @email = params[:email]
      @token = params[:token]
      @user = User.where("email =?",@email ).first
        if @user
           @verify_code = Verification.where("user_id =?",@user.id ).first
           if @verify_code.email_confirmation_token == @token
              @user.update(:status => 1)
              @verify_code.update(:email_confirmed_at => Time.now) 
              render json: {'result' => true}
           else
              render json: {'result' => false, 'error' => 'wrong code'}
           end      
        else
              render json: {'result' => false, 'error' => 'Email Not Found'}
        end
  end

    # resend confirmation mail 
    # POST /resend confirmation mail to user after registeration
    # =begin
    # @api {post} /api/v1/resend_confirmmail resend confirmation mail
    # @apiVersion 0.3.0
    # @apiName POSTresend_confirmmail
    # @apiGroup User
    # @apiDescription resend confirmation mail to user after registeration.
    # @apiExample Example usage:
    # curl -X POST  http://localhost:3000/api/v1/resend_confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"email": "engamira333@gmail.com"}' 
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # {
    #    "result : true"
    # }
    #
    # @apiError required_parameter_missing wrong email .
    #
    #  @apiErrorExample Response (example):
    #     HTTP/1.1 400 Badrequest
    #     {
    #       "result : false"
    #       "error": "MissingData"
    #       "status": 400- Bad Request
    #    }
    # =end 

  def resend_confirmmail
    @email = params[:email]
    @user = User.where("email =?",@email ).first     
      if @user
         @verify_code = Verification.where("user_id =?",@user.id ).first      
         UserMailer.confirmmail(@user,@verify_code.email_confirmation_token).deliver_later
         render json: {'result' => true}
      else
         render json: {'result' => false, 'error' => 'Email Not Found'}
      end
  end

    # POST /users
    # POST /users.json
    # =begin
    # @api {post} /users/ Create new User
    # @apiVersion 0.3.0
    # @apiName postUser
    # @apiGroup User
    # @apiDescription create new user.
    # @apiParam {String}     firstname         User's First Name 
    # @apiParam {String}     lastname          User's Last Name 
    # @apiParam {Integer}    country_id        User's Country 
    # @apiParam {String}     email             User's Email
    # @apiParam {String}     username          User's Username
    # @apiParam {String}     password          User's Password
    # @apiParam {String}     refered_by        Invitation Code Of the Invitor
    #
    # @apiExample Example usage:       
    # curl -X POST  http://localhost:3000/api/v1/users -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"user": {"firstname": "amira2","lastname": "amira2","password": "Amira123456", "country_id": 1,"email": "tessst@gmail.com","username": "EngineeraAmira2", "refered_by": "71000e83"}}' 
    # @apiSuccess {Integer}     id                   Users-ID.
    # @apiSuccess {String}      username             Users username.
    # @apiSuccess {String}      firstname            Users firstname.
    # @apiSuccess {String}      lastname             Users lastname.
    # @apiSuccess {String}      email                Users email.
    # @apiSuccess {Integer}     country_id           Users country.
    # @apiSuccess {String}      secret_code          Users Secret Code
    # @apiSuccess {String}      telephone            Users telephone
    # @apiSuccess {Integer}     account_currency     Users currency
    # @apiSuccess {Integer}     active_otp           Users active two factor authentication
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "success": "200 ok"
    #    }
    #
    # @apiError NoAccessRight Only authenticated users can create User.
    #
    # @apiError MissingToken invalid-token.
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "error": "Missing Token"
    #    }
    # =end

  def create
    @user = User.new(user_params)
    @user.uuid = SecureRandom.hex(4)
    @user.invitation_code = SecureRandom.hex(4)
    @user.auth_token = SecureRandom.hex(6)
    @token = SecureRandom.hex(4)
    respond_to do |format|
        if @user.save        
          @acc_num =  "PS1" + "%08d" % @user.id.to_i 
          @user.update(:account_number => @acc_num)
          Verification.create(:user_id => @user.id,:email_confirmation_token => @token)
          Watchdog.create(:user_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now ,:operation_type => "sign_up")
          if (params[:user][:refered_by] != nil )
              @invited_by = User.where("invitation_code =? ",params[:user][:refered_by]).first
              if (@invited_by != nil)
                 AffilateProgram.create(:user_id => @user.id, :refered_by => @invited_by.id)
              end
          end
          UserMailer.confirmmail(@user,@token).deliver_later
          format.json { render json: @user, status: :created } 
          # format.json { render :show, status: :created, location: @user }
        else
        format.json { render json: @user.errors, status: :unprocessable_entity }
        end
    end
  end

    # PATCH/PUT /users/1
    # PATCH/PUT /users/1.json
    # =begin
    # @api {put} /users/:id/edit Update User 
    # @apiVersion 0.3.0
    # @apiName PutUser
    # @apiGroup User
    #
    # @apiDescription update details of a user.
    #
    # @apiParam {String}     username             Users username.
    # @apiParam {String}     firstname            Users firstname.
    # @apiParam {String}     lastname             Users lastname.
    # @apiParam {String}     email                Users email.
    # @apiParam {Integer}    country_id           Users country.
    # @apiParam {String}     secret_code          Users Secret Code
    # @apiParam {String}     telephone            Users telephone
    # @apiParam {Integer}    account_currency     Users currency
    # @apiParam {Integer}    active_otp           Users active two factor authentication 
    #
    # @apiExample Example usage:
    # curl -X PUT http://localhost:3000/api/v1/users/10 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ "email": "amira@ggggg.com"}'
    #
    # @apiSuccess {Integer}      id                     Users-ID.
    # @apiSuccess {String}       username               Users username.
    # @apiSuccess {String}       firstname              Users firstname.
    # @apiSuccess {String}       lastname               Users lastname.
    # @apiSuccess {String}       email                  Users email.
    # @apiSuccess {Integer}      country_id             Users country.
    # @apiSuccess {String}       secret_code            Users Secret Code
    # @apiSuccess {String}       telephone              Users telephone
    # @apiSuccess {Integer}      account_currency       Users currency
    # @apiSuccess {Integer}      active_otp             Users active two factor authentication 
    # @apiSuccess {Datetime}     created_at             Date of creating the User.
    # @apiSuccess {Datetime}     updated_at             Date of updating the User.
    #
    # @apiError NoAccessRight Only authenticated users can update the data.
    # @apiError UserNotFound   The <code>id</code> of the User was not found.
    #
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "UserNotFound"
    #    }
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Not Authenticated
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end
       
  def update
     
    respond_to do |format|
      if current_user.roleid == 1 or current_user.id == params[:id].to_i 
        if @user.update(user_params)
           format.json { render json: @user, status: :ok, location: @user }
        else
           format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        format.json { render json: "Not Allowed" }
      end
    end   
  end

    # DELETE /users/1
    # DELETE /users/1.json
  def destroy
    if current_user.roleid == 1
      @user.destroy
      respond_to do |format|
          format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
          format.json { head :no_content }
      end
    else
        redirect_to root_path , notice: "not allowed" 
    end
  end

  private
      # Use callbacks to share common setup or constraints between actions.
  def set_user
      @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:uuid, :roleid,  :firstname, :lastname, :status, :language, :disabled, :loginattempts, :username, :email, :password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :invitation_code, :unlock_token, :refered_by) 
  end

end
