module Api
    class BankAccountsController < ApplicationController
        before_action :set_bank_account, only: [:show, :edit, :update, :destroy]
        skip_before_action :verify_authenticity_token

        # GET /bank_accounts
        # GET /bank_accounts.json
        def index
        # =begin
        # @api {get} /api/bank_accounts 1-Request bank accounts List
        # @apiVersion 0.3.0
        # @apiName GetBank
        # @apiGroup Banks
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/bank_accounts
        # @apiSuccess {Number} id Bank unique ID.
        # @apiSuccess {string} bank_name Bank name.
        # @apiSuccess {string} branche_name Branche name.
        # @apiSuccess {string} swift_code Bank swift code.
        # @apiSuccess {string} country Bank country.
        # @apiSuccess {string} account_name Name of the owner of the acount.
        # @apiSuccess {string} account_number Bank account number.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #      {
        #         "id": 1,
        #         "bank_name": "بنك مصر",
        #         "branche_name": "بنك مصر الدولي",
        #         "swift_code": "47859654",
        #         "country": "111",
        #         "account_name": "omar",
        #         "account_number": "123",
        #         "created_at": "2018-09-05T11:07:13.000Z",
        #         "updated_at": "2018-09-05T11:07:13.000Z"
        #     },
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
        @bank_accounts = BankAccount.all
        respond_to do |format|
            format.json { render json: @bank_accounts }
        end
        end
    
        # GET /bank_accounts/1
        # GET /bank_accounts/1.json
        def show
        # =begin
        # @api {get} /api/bank_accounts/{:id} 3-Request Specific Bank account
        # @apiVersion 0.3.0
        # @apiName GetSpecificBank
        # @apiGroup Banks
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/bank_accounts/1
        # @apiParam {Number} id Bank ID.
        # @apiSuccess {Number} id Bank unique ID.
        # @apiSuccess {string} bank_name Bank name.
        # @apiSuccess {string} branche_name Branche name.
        # @apiSuccess {string} swift_code Bank swift code.
        # @apiSuccess {string} country Bank country.
        # @apiSuccess {string} account_name Name of the owner of the acount.
        # @apiSuccess {string} account_number Bank account number.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #       "id": 1,
        #       "bank_name": "بنك مصر",
        #       "branche_name": "بنك مصر الدولي",
        #       "swift_code": "47859654",
        #       "country": "111",
        #       "account_name": "omar",
        #       "account_number": "123",
        #       "created_at": "2018-09-05T11:07:13.000Z",
        #       "updated_at": "2018-09-05T11:07:13.000Z"
        #      }
        # @apiError BankAccountNotFound The id of the Bank was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "BankAccountNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
            format.json { render json: @bank_account }
          end
        end
    
        # GET /bank_accounts/new
        def new
        @bank_account = BankAccount.new
        end
    
        # GET /bank_accounts/1/edit
        def edit
        end
    
        # POST /bank_accounts
        # POST /bank_accounts.json
        def create
        # =begin
        # @api {post} /api/bank_accounts 2-Create a new bank account
        # @apiVersion 0.3.0
        # @apiName PostBank
        # @apiGroup Banks
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/bank_accounts \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "bank_name": "البنك العربي الافريقي",
        #   "branche_name": "البنك العربي الافريقي",
        #   "swift_code": "78965125",
        #   "country": "456",
        #   "account_name": "omar",
        #   "account_number": "7896423556"
        # }'
        # @apiParam {string} bank_name Bank name.
        # @apiParam {string} branche_name Branche name.
        # @apiParam {string} swift_code Bank swift code.
        # @apiParam {string} country Bank country.
        # @apiParam {string} account_name Name of the owner of the acount.
        # @apiParam {string} account_number Bank account number.

        # @apiSuccess {Number} id Bank unique ID.
        # @apiSuccess {string} bank_name Bank name.
        # @apiSuccess {string} branche_name Branche name.
        # @apiSuccess {string} swift_code Bank swift code length must be 8 or 11.
        # @apiSuccess {string} country Bank country should be 3 characters.
        # @apiSuccess {string} account_name Name of the owner of the acount.
        # @apiSuccess {string} account_number Bank account number.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 6,
        #         "bank_name": "البنك العربي الافريقي",
        #         "branche_name": "البنك العربي الافريقي",
        #         "swift_code": "78965125",
        #         "country": "456",
        #         "account_name": "omar",
        #         "account_number": "7896423556",
        #         "created_at": "2018-09-05T12:17:13.000Z",
        #         "updated_at": "2018-09-05T12:17:13.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError InvalidSwiftCode bank swift code is invalid.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Invalid Swift Code
        #   {
        #     "error": "is invalid"
        #   }
        # @apiError InvalidCountry bank country not equal 3 characters.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Country
        #   {
        #     "error": "is the wrong length (should be 3 characters)"
        #   }
        # =end


        @bank_account = BankAccount.new(bank_account_params)
    
        respond_to do |format|
            if @bank_account.save
            format.json { render json: @bank_account, status: :created, location: @bank_account }
            else
            format.json { render json: @bank_account.errors, status: :unprocessable_entity }
            end
        end
        end
    
        # PATCH/PUT /bank_accounts/1
        # PATCH/PUT /bank_accounts/1.json
        def update
        # =begin
        # @api {put} /api/bank_accounts/{:id} 4-Update an existing Bank
        # @apiVersion 0.3.0
        # @apiName PutBank
        # @apiGroup Banks
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/bank_accounts/6 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "country": "777",
        #   "account_name": "omar aransa"
        # }'
        # @apiParam {string} country Bank country.
        # @apiParam {string} account_name Name of the owner of the acount.
        # @apiSuccess {Number} id Bank unique ID.
        # @apiSuccess {string} bank_name Bank name.
        # @apiSuccess {string} branche_name Branche name.
        # @apiSuccess {string} swift_code Bank swift code length must be 8 or 11.
        # @apiSuccess {string} country Bank country should be 3 characters.
        # @apiSuccess {string} account_name Name of the owner of the acount.
        # @apiSuccess {string} account_number Bank account number.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 6,
        #         "country": "777",
        #         "account_name": "omar aransa",
        #         "swift_code": "78965125",
        #         "bank_name": "البنك العربي الافريقي",
        #         "branche_name": "البنك العربي الافريقي",
        #         "account_number": "7896423556",
        #         "created_at": "2018-09-05T12:17:13.000Z",
        #         "updated_at": "2018-09-05T12:40:43.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError BankNotFound The id of the Bank was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "BankNotFound"
        #   }
        # =end

        respond_to do |format|
            if @bank_account.update(bank_account_params)
                format.json { render json: @bank_account, status: :ok, location: @bank_account }
            else
            format.json { render json: @bank_account.errors, status: :unprocessable_entity }
            end
        end
        end
    
        # DELETE /bank_accounts/1
        # DELETE /bank_accounts/1.json
        def destroy
        @bank_account.destroy
        respond_to do |format|
            format.json { head :no_content }
        end
        end
    
        private
        # Use callbacks to share common setup or constraints between actions.
        def set_bank_account
            @bank_account = BankAccount.find(params[:id])
        end
    
        # Never trust parameters from the scary internet, only allow the white list through.
        def bank_account_params
            params.require(:bank_account).permit(:bank_name, :branche_name, :swift_code, :country, :account_name, :account_number)
        end
    end
end