class Clearance::SessionsController < Clearance::BaseController
    if respond_to?(:before_action)
      before_action :redirect_signed_in_users, only: [:new]
      skip_before_action :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_action :authorize,
        only: [:create, :new, :destroy],
        raise: false
    else
      before_filter :redirect_signed_in_users, only: [:new]
      skip_before_filter :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_filter :authorize,
        only: [:create, :new, :destroy],
        raise: false
    end
    skip_before_action :check2fa ,only: [:new, :destroy, :create]
    skip_before_action :check_active_session ,only: [:new, :destroy, :create]
    skip_before_action :check_password_expiration ,only: [:new, :destroy, :create]
    
  # if user credentials are true,he is signed in and redirected to confirmation page
    def create
      @user = authenticate(params)
      @token = Random.new.rand(111111..999999)


      # if @user and @user.disabled == false       
      # end
      

       if @user and @user.status == false # prevent user from logging untill confirming his email
        flash.now.notice = 'You should confirm your email first'
        render template: "sessions/new"

       elsif @user and @user.disabled == true # prevent user fron logging if his account is locked
          flash.now.notice = 'Your account is locked,please visit your email to activate it.'
          render template: "sessions/new"

       #elsif @user and @user.failed_attempts > 3

       else


   
            sign_in(@user) do |status|
              @device_id = SecureRandom.uuid
                if status.success?

                  # @last_log = AdminWatchdog.where("admin_id =? AND operation_type =? ", @user.id ,"sign_in" ).last
                  # @current_log_ip = request.env['REMOTE_ADDR']
                  
                  
                  
                    #cookies.permanent[:auth_token] = @user.auth_token
                    cookies.permanent[:device_id] = @device_id
                    @user.loginattempts += 1
                    @user.failedattempts = 0
                    @user.save
                    
                    session[:last_visit] = Time.now

                    #error in this query at first time login because no records !!!!!!
                    # get last signin to check if current ip is different from last ip send email to user
                    @last_ip = AdminWatchdog.where("admin_id =? AND operation_type =? ", @user.id ,"sign_in").last
                    if @last_ip == nil
                      @last_ip = AdminWatchdog.where("admin_id =?", @user.id).last
                    end
                    if (@last_ip)
                      @current_ip = request.env['REMOTE_ADDR']
                      @db = MaxMindDB.new('./GeoLite2-Country.mmdb')
                      @last_ip_country = @db.lookup(@last_ip.ipaddress)                  
                      @current_ip_country = @db.lookup(@current_ip) 
                      # @hour = Time.now.getutc.hour - @last_ip.lastvisit.hour
                      # (@current_ip == @last_ip.ipaddress) && (Time.now.getutc.strftime("%d/%m/%Y") == @last_ip.lastvisit.strftime("%d/%m/%Y")) && ((Time.now.getutc.min < @last_ip.lastvisit.min &&  @hour == 1) ||  @hour < 1)
                      @last_request_two_factor = AdminWatchdog.where("admin_id =? AND operation_type =? AND lastvisit >=?" , @user.id ,"request_two_factor",  DateTime.now - 30.minutes ).count

                      if @last_request_two_factor && @last_request_two_factor >= 3
                        @user_sessions = AdminLogin.where(admin_id: @user.id).all
                        @user_sessions.all.each do |user_sessions|
                          user_sessions.destroy
                        end
                        # flash.now.notice = 'Your Must wait 1 hour until your second login.'
                        sign_out
                        redirect_to root_path , notice: "Your Must wait 1 hour until your second login." 
                        # redirect_to root
                      else
                        if @last_ip_country.country.name != @current_ip_country.country.name
                          UserMailer.differentip(@user,@current_ip).deliver_later
                        end 
                        @login = AdminLogin.create(admin_id: @user.id, ip_address: request.remote_ip,user_agent: request.user_agent,device_id: @device_id, :operation_type => "sign_in")
                        AdminWatchdog.create(:admin_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now, :operation_type => "request_two_factor")
                        if @user.active_otp == 1
                          session[:token_code] = @token
                          @login.update(email_token_sent_at: DateTime.now)
                          UserMailer.confirmsignin(@user,@token).deliver_later
                        end
                        redirect_to two_factor_path
                      end                     
                    end               
                    #redirect_to google_auth_path
                    #redirect_back_or url_after_create
                else
                  @currentuser = Admin.where("email =?",params[:session][:email].to_s).first
                  if @currentuser != nil 
                    @currentuser.failedattempts += 1
                     if @currentuser.failedattempts >= 3
                       @currentuser.disabled = true
                       @currentuser.unlock_token = @token
                       UserMailer.unlockaccount(@currentuser,@token).deliver_later
                     end
                     @currentuser.save
                     AdminLogin.create(admin_id: @currentuser.id, ip_address: request.remote_ip, user_agent: request.user_agent, device_id: @device_id, :operation_type => "failed_sign_in")
                     AdminWatchdog.create(:admin_id => @currentuser.id, :ipaddress => request.env['REMOTE_ADDR'], :logintime => Time.now, :lastvisit => Time.now, :operation_type => "failed_sign_in"  )
                  end
                flash.now.notice = status.failure_message
                render template: "sessions/new", status: :unauthorized
                end
            end
       end
    end

    # when user signedout, he is logeed out from all sessions
  
    def destroy
      #if two sessions opened all of them will destroy but only this session will be deleted from log so must edit to delete all logs
      #@test = Login.where(user_id: current_user.id, device_id: cookies[:device_id]).first
      #if @test != nil
      #@test.destroy
      #end
      @user_sessions = AdminLogin.where(admin_id: current_user.id).all
      if (@user_sessions  != nil)
          @user_sessions.all.each do |user_sessions|
            user_sessions.destroy
          end
      end
      #cookies.delete(:auth_token)
      cookies.delete(:device_id)
      session[:refered_by] = nil
      session[:verify_code] = false
      session[:last_visit] = nil
      flash.now[:notice] = "Successfully signed out."
      sign_out
      redirect_to url_after_destroy
    end
  
    def new
      render template: "sessions/new"
    end
  
    private
  
    def redirect_signed_in_users
      if signed_in?
        redirect_to url_for_signed_in_users
      end
    end
  
    def url_after_create
      Clearance.configuration.redirect_url
    end
  
    def url_after_destroy
      sign_in_url
    end
  
    def url_for_signed_in_users
      url_after_create
    end
  end