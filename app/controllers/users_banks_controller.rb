class UsersBanksController < ApplicationController
  before_action :set_users_bank, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # GET /users_banks
  # GET /users_banks.json
  def index
    @users_banks = UsersBank.includes(:user, :bank).all
  end

  # GET /users_banks/1
  # GET /users_banks/1.json
  def show
  end

  # GET /users_banks/new
  def new
    @users_bank = UsersBank.new
  end

  # GET /users_banks/1/edit
  def edit
  end

  # POST /users_banks
  # POST /users_banks.json
  def create
    @users_bank = UsersBank.new(users_bank_params)

    respond_to do |format|
      if @users_bank.save
        format.html { redirect_to @users_bank, notice: 'Users bank was successfully created.' }
        format.json { render :show, status: :created, location: @users_bank }
      else
        format.html { render :new }
        format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_banks/1
  # PATCH/PUT /users_banks/1.json
  def update
    respond_to do |format|
      if @users_bank.update(users_bank_params)
        format.html { redirect_to @users_bank, notice: 'Users bank was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_bank }
      else
        format.html { render :edit }
        format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_banks/1
  # DELETE /users_banks/1.json
  def destroy
    @users_bank.destroy
    respond_to do |format|
      format.html { redirect_to users_banks_url, notice: 'Users bank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_bank
      @users_bank = UsersBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_bank_params
      params.require(:users_bank).permit(:bank_id, :user_id, :account_number, :active, :branch_name, :swift_code, :is_default)
    end
end
