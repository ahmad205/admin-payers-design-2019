class ApplicationController < ActionController::Base
  include Clearance::Controller
  include Response
  include ExceptionHandler
  include Pundit
  
  before_action :site_settings
  before_action :check2fa ,except: [:get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email]
  before_action :check_active_session ,except: [:get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email, :lock_page, :check_to_unlock] , :unless => :api_request?
  before_action :check_password_expiration ,except: [:reset_expired_password, :post_reset_expired_password, :get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email, :lock_page, :check_to_unlock], :unless => :api_request?
  before_action :set_locale
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  protect_from_forgery with: :exception

  layout :setlayout

       #attr_reader :current_user
   

  def check2fa
    if  session[:verify_code] == false
      if current_user
        redirect_to two_factor_path
      else 
        render template: "sessions/new"
      end
    end
  end



  def check_active_session
    if current_user
      @session_log = AdminLogin.where("admin_id = ? and device_id = ? ", current_user.id,cookies[:device_id]).first
      if @session_log and (@session_log.ip_address == request.remote_ip)
        if (session[:last_visit] == nil or session[:last_visit] <= 30.minutes.ago)
            # before_sign_out
            # sign_out
            redirect_to lock_page_path
        elsif (session[:last_visit] >= 15.minutes.ago)
            if (session[:last_visit] <= 5.minutes.ago)
              session[:last_visit] = Time.now
            end        
        else
            redirect_to lock_page_path 
        end
      else
        before_sign_out
        sign_out
        redirect_to sign_in_path
      end
    else
      redirect_to sign_in_path
    end
  end

  def check_password_expiration
    if current_user
      if (current_user.password_changed_at < 1.month.ago) # prevent admin from logging if his password expired
        flash.now.notice = 'Your Password has been expired, please reset your password.'
        redirect_to reset_expired_password_path
      end
    else
      redirect_to sign_in_path
    end

  end

  def set_locale
    if current_user
    locale = params[:locale].to_s.strip.to_sym
    I18n.locale = I18n.available_locales.include?(locale) ?
        locale :
        I18n.locale = current_user.language
    end
  end

  def before_sign_out
    @user_sessions = AdminLogin.where(admin_id: current_user.id).all
    if (@user_sessions  != nil)
        @user_sessions.all.each do |user_sessions|
            user_sessions.destroy
         end
    end
    cookies.delete(:auth_token)
    cookies.delete(:device_id)
    session[:refered_by] = nil
    session[:verify_code] = false
    session[:last_visit] = nil
  end

  def record_not_found
    if request.format.json?
      render json: "record_not_found"
    else
      redirect_to error_page_path , notice: "record not found" 
    end
  end

  def site_settings
    @max_items_per_page = 10
  end
  protected
  def authenticate_request!
    @current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
   # unless user_id_in_token?
      # render json: { errors: ['Not Authenticated'] }, status: :unauthorized
    #  return
    #end
  #  @current_user = User.find(auth_token[:user_id])
  #rescue JWT::VerificationError, JWT::DecodeError
   # render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  private
  def http_token
      @http_token ||= if request.headers['Authorization'].present?
        request.headers['Authorization'].split(' ').last
      end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end

  def setlayout
    if current_user 
        "admin_layout"
    end
  end

  def api_request?
    request.format.json?
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
 
    flash[:error] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end

  # def url_after_denied_access_when_signed_out
  #   '/'
  #end

  #def sign_in(user)
   # user.reset_remember_token! if user
   # super
  #end

  #users#url_after_create
  # def url_after_create
  #   '/'
  #end

  #sessions#url_for_signed_in_users
  # def url_for_signed_in_users
  #   '/'
  #end

  #sessions#url_after_create
  # def url_after_create
  #   '/'
  #end

  #passwords#url_after_update
  # def url_after_update
  #   '/'
  #end

end
